/** \file lexer.l
*** \brief
*** LEX (FLEX) lexer
***/

/** \file lexer.lex.cpp
*** \brief
*** Generated LEX (FLEX) lexer code file
***/

%{

	#include <stack>
	#include <iostream>
	#include <cstring>
    using namespace std;
    
    #include "parser/parserincludes.hpp"
    #include "parser/parser.yacc.hpp"
    
    #include "semantics/function.hpp"
    #include "semantics/procedure.hpp"
	using namespace Semantics;

	// Forward declaration of yyerror 
    void yyerror(char const *);
    
	/// \brief
	/// Identifier or Keyword processing
	///
	/// Function checks if given string is a keyword or identifier.
	///
	/// \param[in] word - string to check
	///
	/// \return
	/// Keyword or _IDENTIFIER token.
    int ident_or_keyword(const char* word);

	/// \brief
	/// String copy using low case
	///
	/// Function copies string from source to destination
	/// converting all characters to lower case.
	///
	/// \param[in] dest - destination buffer
	/// \param[in] src - source buffer
	///
	/// \return
	/// Returns destination buffer.
	char* strcpylower(char* dest, const char* src);

	/// Contents of string extracted from input
    string stringContents;

/*
stack<int> unputbuffer;
int lex_input();
void lex_unput(int c);
*/

#ifdef __cplusplus
#	define LEX_INPUT		yyinput
#else
#	define LEX_INPUT		input
#endif

#define LEX_UNPUT(c)	unput(c)

/*
#define YY_NO_INPUT
#define YY_NO_UNPUT

#define YYINPUT LEX_INPUT

#define YY_INPUT(buf,result,max_size) \
    { \
    int c = getchar(); \
    result = (c == EOF) ? YY_NULL : (buf[0] = c, 1); \
    }
*/

%}

%x COMMENT APOSTR

LETTER          [a-zA-Z]
DIGIT           [0-9]
WHITE           [ \t\n\r]

SCALECHAR       [eE]
SIGNCHAR        [-+]

%%

    /* ================================================== */
    /* ==================== COMMENTS ==================== */
    /* ================================================== */

"{"|"(*"                            {   BEGIN COMMENT;  }
<COMMENT>"}"|"*)"                   {   BEGIN INITIAL;  }
<COMMENT>.                          {                   }

    /* ================================================== */
    /* ==================== STRINGS ===================== */
    /* ================================================== */

\'                                  {   
										BEGIN APOSTR;
										stringContents.clear();   
									}
<APOSTR>\'                          {
                                        int c;
										c = LEX_INPUT();

                                        /* APOSTROPHE IMAGE */
                                        if(c == '\'')
                                        {
                                            stringContents += '\'';
                                        }
                                        else
                                        {
                                            char* tmp = new char[stringContents.length()+1];
                                            strcpy(tmp, stringContents.c_str());
                                            yylval.m_charString = REG(tmp);
                                            LEX_UNPUT(c);
                                            BEGIN INITIAL;
                                            
                                            return _STRING;
                                        }
                                    }
<APOSTR>[^\"]                       {
                                        stringContents += yytext[0];
                                    }

    /* ================================================== */
    /* ============== ALTERNATIVE VERSIONS ============== */
    /* ================================================== */

"(."                                {   return _SQUARE_ALT_OPEN;    }
".)"                                {   return _SQUARE_ALT_CLOSE;   }

    /* ================================================== */
    /* ============ IDENTIFIERS & KEYWORDS ============== */
    /* ================================================== */

{LETTER}({LETTER}|{DIGIT})*     {   return ident_or_keyword(yytext);    }

    /* ================================================== */
    /* =================== NUMBERS ====================== */
    /* ================================================== */

{DIGIT}+                        {   
                                    yylval.m_unsignedInteger = strtol(yytext, NULL, 10);
                                    return _UNSIGNED_INTEGER;
                                }

{DIGIT}+"."{DIGIT}+             {
                                    yylval.m_real = strtod(yytext, NULL);
                                    return _UNSIGNED_REAL;
                                }

{DIGIT}+"."{DIGIT}+{SCALECHAR}{SIGNCHAR}?{DIGIT}+   {
                                    yylval.m_real = strtod(yytext, NULL);
                                    return _UNSIGNED_REAL;
                                }

{DIGIT}+{SCALECHAR}{SIGNCHAR}?{DIGIT}+  {
                                    yylval.m_real = strtod(yytext, NULL);
                                    return _UNSIGNED_REAL;
                                }
                                                                
    /* ================================================== */
    /* ==================== OPERATORS =================== */
    /* ================================================== */

"<="                            {   return OP_LE;   }
">="                            {   return OP_GE;   }
"<>"                            {   return OP_NE;   }

":="                            {   return OP_ASSIGN;      }

".."                            {   return _DOUBLE_DOT;     }

    /* ================================================== */
    /* ======= WHITESPACES AND OTHER CHARACTERS ========= */
    /* ================================================== */

{WHITE}                         {   }

.                               {   return yytext[0];   }

%%

//--------------------------------------------------------------------

int ident_or_keyword(const char* word)
{
    typedef struct
    {
        const char* m_keyword;
        int m_token;
    } KeywordToken;

    static KeywordToken keytok[] =
    {
        {   "and",          _AND        },
        {   "array",        _ARRAY      },
        {   "begin",        _BEGIN      },
        {   "case",         _CASE       },
        {   "const",        _CONST      },
        {   "div",          _DIV        },
        {   "do",           _DO         },
        {   "downto",       _DOWNTO     },
        {   "else",         _ELSE       },
        {   "end",          _END        },
        {   "file",         _FILE       },
        {   "for",          _FOR        },
        {   "function",     _FUNCTION   },
        {   "goto",         _GOTO       },
        {   "if",           _IF         },
        {   "in",           _IN         },
        {   "label",        _LABEL      },
        {   "mod",          _MOD        },
        {   "nil",          _NIL        },
        {   "not",          _NOT        },
        {   "of",           _OF         },
        {   "or",           _OR         },
        {   "packed",       _PACKED     },
        {   "procedure",    _PROCEDURE  },
        {   "program",      _PROGRAM    },
        {   "record",       _RECORD     },
        {   "repeat",       _REPEAT     },
        {   "set",          _SET        },
        {   "then",         _THEN       },
        {   "to",           _TO         },
        {   "type",         _TYPE       },
        {   "until",        _UNTIL      },
        {   "var",          _VAR        },
        {   "while",        _WHILE      },
        {   "with",         _WITH       },
        {   NULL,           0           }
    };

	// copy given text
	char* tmp = new char[strlen(word) + 1];
	strcpylower(tmp, word);
	word = NULL;
	yylval.m_charString = REG(tmp);

    for(int i=0; keytok[i].m_keyword != NULL; ++i)
        if(strcmp(keytok[i].m_keyword, tmp) == 0)
            return keytok[i].m_token;

	// check if identifiers should be recognized as objects
	if(g_semgen.getIdentifiersRecognition())
	{
		// check if there is an object with this identifier
		Object* obj = g_semgen.findExistingObject(tmp);
		if(obj)
		{
			if(obj->isOfClass(clid_Procedure))
				return _PROCEDURE_IDENTIFIER;
			else if(obj->isOfClass(clid_Function))
				return _FUNCTION_IDENTIFIER;
			else if(obj->isOfClass(clid_Type))
				return _TYPE_IDENTIFIER;
			else if(obj->isOfClass(clid_Const))
				return _CONSTANT_IDENTIFIER;
			else
				return _IDENTIFIER;				
		}
		else
			return _IDENTIFIER;
	}
	else
		return _IDENTIFIER;
}

//--------------------------------------------------------------------

char* strcpylower(char* dest, const char* src)
{
	char* tmp = dest;

	while(*src)
		*tmp++ = tolower(*src++);
	*tmp = 0;

	return dest;
}

/*
int lex_input()
{
	int c;
	
	if(unputbuffer.size() > 0)
	{
		c = unputbuffer.top();
		unputbuffer.pop();
	}
	else
	{
		c = getc(yyin);
		if(c == EOF)
		{
			if(ferror(yyin))
				YY_FATAL_ERROR("Lexer input failed");
			else
				c = YY_NULL;
		}
	}
	
	return c;
}

void lex_unput(int c)
{
	unputbuffer.push(c);

	yyunput(c);
	input(c);
	yyinput(c);
}
*/

/// \brief
/// Default yywrap function implementation
///
int yywrap()
{
    return 1;
}
