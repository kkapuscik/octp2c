//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics generator class
//--------------------------------------------------------------------

#ifndef OCT_SEMGEN_SEMGEN_HPP
#define OCT_SEMGEN_SEMGEN_HPP

//--------------------------------------------------------------------

#include "semantics/global.hpp"
#include "semantics/namespace.hpp"
#include "semantics/simpletype.hpp"
#include "utility/garbagecollector.hpp"
#include "parser/typedefs.hpp"

#include <vector>

//--------------------------------------------------------------------

/// \brief
/// Semantic generator class
///
class SemGen
{
private:
    /// Semantics global namespace object
    Semantics::Global* m_global;
    /// Current scope namespace
    Semantics::Namespace* m_currentNamespace;
    /// Garbage collector object
    GarbageCollector* m_garbageCollector;
	/// Identifier types recognition flag
	bool m_recognizeIdentifiers;

public:
    /// \brief
    /// Constructor
    ///
    SemGen(void);

    /// \brief
    /// Destructor
    ///
    virtual ~SemGen(void);


    /// \brief
    /// Initialization
    ///
    /// \return
    /// True is returned if generator was successfully initialized or false otherwise.
    bool init(void);

    /// \brief
    /// Deinitialization
    ///
    void free(void);


	/// \brief
	/// Set identifiers recognition status
	///
	/// \param[in] recognize - new identifiers recognition flag value
	void setIdentifiersRecognition(bool recognize);

	/// \brief
	/// Get identifiers recognition status
	///
	/// \return
	/// Returns current identifiers recognition flag value.
	bool getIdentifiersRecognition() const;


    /// \brief
    /// Get global object
    ///
    /// \return
    /// Global object or NULL if it doesn't exist.
    const Semantics::Global* getGlobal(void) const;

    /// \brief
    /// Push namespace
    ///
    /// Set current namespace to this given as parameter. It is called pushing
    /// because the given namespace should have previously current namespace
    /// as a parent.
    ///
    /// \param[in] nspace - namespace object to push
    void pushNamespace(Semantics::Namespace* nspace);
    
    /// \brief
    /// Pop namespace
    ///
    /// Set current namespace to parent of current namespace.
    void popNamespace(void);

    /// \brief
    /// Get current namespace
    ///
    /// \return
    /// Pointer to current namespace object or NULL if there
	/// is no current namespace.
    Semantics::Namespace* getCurrentNamespace(void);

private:
	/// \brief
	/// Create standard PASCAL objects
	///
	/// This method creates all standard pascal objects like
	/// simple types, default procedures and function and other.
	///
	/// \return
	/// \c True is returned on success and \c false on error.
    bool createStandardObjects(void);


public:
    /// \brief
    /// Register pointer in garbage collector
    ///
    /// \param[in] pointer - pointer to register
    ///
    /// \return
    /// The same pointer given as parameter.
    template <class T>
#ifdef _DEBUG
    T* registerPointer(T* pointer, char* srcfile, int srcline)
#else /* _DEBUG */
    T* registerPointer(T* pointer)
#endif /* _DEBUG */
    {
        assert(m_garbageCollector);

#ifdef _DEBUG
        m_garbageCollector->store(pointer, srcfile, srcline);
#else /* _DEBUG */
        m_garbageCollector->store(pointer);
#endif /* _DEBUG */
        return pointer;
    }

    /// \brief
    /// Unregister pointer in garbage collector
    ///
    /// \param[in] pointer - pointer to unregister
    ///
    /// \return
    /// The same pointer given as parameter.
    template <class T>
#ifdef _DEBUG
    T* unregisterPointer(T* pointer, char* srcfile, int srcline)
#else /* _DEBUG */
    T* unregisterPointer(T* pointer)
#endif /* _DEBUG */	
    {
        assert(m_garbageCollector);

#ifdef _DEBUG
        m_garbageCollector->free(pointer, srcfile, srcline);
#else /* _DEBUG */
        m_garbageCollector->free(pointer);
#endif /* _DEBUG */

        return pointer;
    }

    /// \brief
    /// Free pointer stored in garbage collector
    ///
    /// This function unregisters pointer from garbage collector
    /// and calls delete for it.
    ///
    /// \param[in] pointer - pointer to free
    template <class T>
#ifdef _DEBUG
    void freePointer(T* pointer, char* srcfile, int srcline)
#else /* _DEBUG */
    void freePointer(T* pointer)
#endif /* _DEBUG */ 
    {
#ifdef _DEBUG
        delete unregisterPointer(pointer, srcfile, srcline);
#else /* _DEBUG */
        delete unregisterPointer(pointer);
#endif /* _DEBUG */
    }


public:
	/// \brief
	/// Get standard simple type object
	///
	/// \param[in] tid - SimpleType type identificator
	///
	/// \return
	/// Simple type object of specified type is returned.
	const Semantics::SimpleType* getSimpleType(Semantics::SimpleType::TypeId tid) const;

public:
	/// \brief
	/// Find type object
	///
	/// \param[in] identifier - type identifier (name)
	///
	/// \return
	/// Returns found type object or NULL if there is no type
	/// with given name.
	Semantics::Type* findType(char* identifier);

	/// \brief
	/// Find variable object
	///
	/// \param[in] identifier - variable identifier (name)
	///
	/// \return
	/// Returns found variable object or NULL if there is no variable
	/// with given name.
	Semantics::Variable* findVariable(char* identifier);

	/// \brief
	/// Find existing object
	///
	/// \param[in] id - object identifier (name)
	/// \param[in] recursive - recursive search in namespaces tree
	///
	/// \return
	/// Returns found object or NULL if there is no object
	/// with given name.
	Semantics::Object* findExistingObject(const String& id, bool recursive = true);

	/// \brief
	/// Check if access object points to record variable
	///
	/// \param[in] access - access object to check
	///
	/// \return
	/// \c True is returned if access points to record variable or
	/// \c false otherwise.
	bool isRecordVariable(Semantics::ObjectAccess* access);

	/// \brief
	/// Check if access object points to 'entire variable'
	///
	/// \param[in] access - access object to check
	///
	/// \return
	/// \c True is returned if access points to entire variable or
	/// \c false otherwise.
	bool isEntireVariable(Semantics::ObjectAccess* access);

	/// \brief
	/// Record program heading
	///
	/// \param[in] identifier - program identifier
	/// \param[in] parameters - program paramerers
	///
	/// \return
	/// \c True is returned if program was successfully recorded
	/// or \c false otherwise.
	bool recProgramHeading(char* identifier, IdentifierVector* parameters);
	
	/// \brief
	/// Record label declaration part
	///
	/// \param[in] labels - vector with declared labels
	///
	/// \return
	/// \c True is returned if labels was successfully recorded
	/// or \c false otherwise.
	bool recLabelDeclarationPart(LabelVector* labels);

	/// \brief
	/// Create unnamed const object from given integer value
	///
	/// \param[in] value - constans integer value
	///
	/// Created object is returned if constans object was succesfully created
	/// or \c NULL otherwise.
	Semantics::Const* recUnsignedNumber(unsigned int value);

	/// \brief
	/// Create unnamed const object from given real value
	///
	/// \param[in] value - constans real value
	///
	/// Created object is returned if constans object was succesfully created
	/// or \c NULL otherwise.
	Semantics::Const* recUnsignedNumber(double value);

	/// \brief
	/// Create unnamed const object from other constans
	/// and optional sign operator
	///
	/// \param[in] signop - optional sign operator
	/// \param[in] number - constant object with value
	///
	/// Created object is returned if constans object was succesfully created
	/// or \c NULL otherwise.
	Semantics::Const* recConstant(Operator signop, Semantics::Const* number);

	/// \brief
	/// Create unnamed const object from other constans
	/// specified by identifier and optional sign operator
	///
	/// \param[in] signop - optional sign operator
	/// \param[in] identifier - identifier of constant object with value
	///
	/// Created object is returned if constans object was succesfully created
	/// or \c NULL otherwise.
	Semantics::Const* recConstant(Operator signop, char* identifier);

	/// \brief
	/// Create unnamed const object from given string
	///
	/// \param[in] value - constant string value
	///
	/// Created object is returned if constans object was succesfully created
	/// or \c NULL otherwise.
	Semantics::Const* recConstant(char* value);

	/// \brief
	/// Record constans declaration
	///
	/// \param[in] name - declared constans name
	/// \param[in] value - constans value
	///
	/// \c True is returned if constans object was succesfully declared
	/// or \c false otherwise.
	bool recConstantDefinition(char* name, Semantics::Const* value);

	/// \brief
	/// Record variables declaration
	///
	/// \param[in] identifiers - variable identifiers list
	/// \param[in] type - variables type
	///
	/// \c True is returned if variables was successfully declared
	/// or \c false otherwise.
	bool recVariableDeclaration(IdentifierVector* identifiers, 
		Semantics::Type* type);

	/// \brief
	/// Record object access
	///
	/// \param[in] identifier - accessed object identifier
	///
	/// Created object is returned if object access was valid (successful)
	/// or \c NULL otherwise.
	Semantics::ObjectAccess* recObjectAccess(char* identifier);

	/// \brief
	/// Record assignment statement
	///
	/// \param[in] accessedvariable - assignment destination variable
	/// \param[in] valueexpression - assignmed value expression
	///
	/// Created object is returned if assignment was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Statement* recAssignmentStatement(
		Semantics::ObjectAccess* accessedvariable,
		Semantics::Expression* valueexpression);

	/// \brief
	/// Record expression
	///
	/// \param[in] value - expression value object
	///
	/// Created object is returned if expression was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Expression* recExpression(Semantics::Typed* value);

	/// \brief
	/// Record expression
	///
	/// \param[in] value1 - first expression value object
	/// \param[in] oper - expression operator
	/// \param[in] value2 - second expression value object
	///
	/// Created object is returned if expression was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Expression* recExpression(
		Semantics::Typed* value1, Operator oper, Semantics::Typed* value2);

	/// \brief
	/// Record simple expression
	///
	/// \param[in] unoper - unary operator (sign)
	/// \param[in] value - expression value object
	///
	/// Created object is returned if simple expression was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Typed* recSimpleExpression(
		Operator unoper, Semantics::Typed* value);

	/// \brief
	/// Record simple expression
	///
	/// \param[in] value1 - first expression value object
	/// \param[in] oper - binary operator
	/// \param[in] value2 - second expression value object
	///
	/// Created object is returned if simple expression was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Typed* recSimpleExpression(Semantics::Typed* value1,
		Operator oper, Semantics::Typed* value2);

	/// \brief
	/// Record term
	///
	/// \param[in] value1 - first expression value object
	/// \param[in] oper - binary operator
	/// \param[in] value2 - second expression value object
	///
	/// Created object is returned if term was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Typed* recTerm(
		Semantics::Typed* value1, Operator oper, Semantics::Typed* value2);

	/// \brief
	/// Record statement sequence
	///
	/// \param[in] statement - first sequence statement
	///
	/// Created object is returned if statement sequence was successfully recorded
	/// or \c NULL otherwise.
	Semantics::StatementSequence* recStatementSequence(
		Semantics::Statement* statement);

	/// \brief
	/// Record statement sequence
	///
	/// \param[in] sequence - existing statement sequence object
	/// \param[in] statement - next sequence statement
	///
	/// Created object is returned if statement sequence was successfully recorded
	/// or \c NULL otherwise.
	Semantics::StatementSequence* recStatementSequence(
		Semantics::StatementSequence* sequence,
		Semantics::Statement* statement);

	/// \brief
	/// Record empty statement
	///
	/// Created object is returned if empty statement was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Statement* recEmptyStatement();

	/// \brief
	/// Record general sequence
	///
	/// \param[in] label - optional statement label
	/// \param[in] statement - statement object to record
	///
	/// Created object is returned if statement was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Statement* recStatement(LabelType label, 
		Semantics::Statement* statement);

	/// \brief
	/// Record statement part of named block
	///
	/// \param[in] sequence - statement sequence (statement part) to record
	///
	/// \c True is returned if statement part was successfully recorded
	/// or \c false otherwise.
	bool recStatementPart(Semantics::StatementSequence* sequence);

	/// \brief
	/// Record NIL constans
	///
	/// Created object is returned if statement part was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Const* recNil();

	/// \brief
	/// Record not'ed factor
	///
	/// Created object is returned if factor part was successfully recorded
	/// or \c NULL otherwise.
	Semantics::Typed* recNotFactor(Operator oper, Semantics::Typed* factor);

	/// \brief
	/// Check if accessed object is valid for factor
	///
	/// \c True is returned if access is valid
	/// or \c false otherwise.
	bool checkFactorAccess(Semantics::Typed* typed);

	/// \brief
	/// Check if expression is a boolean expression
	///
	/// \param[in] expr - expression to check
	///
	/// \c Expression object is returned if it is a boolean expression
	/// or \c NULL otherwise.
	Semantics::Expression* checkBooleanExpression(Semantics::Expression* expr);

	/// \brief
	/// Check if object access is a variable access
	///
	/// \param[in] access - access object to check
	///
	/// \c Given access object is returned if it is a variable access
	/// or \c NULL otherwise.
	Semantics::ObjectAccess* checkVariableAccess(Semantics::ObjectAccess* access);

	/// \brief
	/// Check if object access is a variable or function return access 
	///
	/// \param[in] access - access object to check
	///
	/// \c Given access object is returned if it is a variable or function
	/// return access or \c NULL otherwise.
	Semantics::ObjectAccess* checkVariableOrFunctionAccess(
		Semantics::ObjectAccess* access);

	/// \brief
	/// Record IF statement
	///
	/// \param[in] condition - IF condition expression
	/// \param[in] ifstatement - IF body statement
	///
	/// \c Created statement object is returned on success
	/// or \c NULL otherwise.
	Semantics::Statement* recIfStatement(Semantics::Expression* condition,
		Semantics::Statement* ifstatement);

	/// \brief
	/// Record IF statement
	///
	/// \param[in] condition - IF condition expression
	/// \param[in] ifstatement - IF part body statement
	/// \param[in] elsestatement - ELSE part body statement
	///
	/// \c Created statement object is returned on success
	/// or \c NULL otherwise.
	Semantics::Statement* recIfStatement(Semantics::Expression* condition,
		Semantics::Statement* ifstatement, Semantics::Statement* elsestatement);

	/// \brief
	/// Record WHILE loopstatement
	///
	/// \param[in] condition - loop condition expression
	/// \param[in] loopstatement - loop body statement
	///
	/// \c Created statement object is returned on success
	/// or \c NULL otherwise.
	Semantics::Statement* recWhileStatement(Semantics::Expression* condition,
		Semantics::Statement* loopstatement);
	
	/// \brief
	/// Record REPEAT loop statement
	///
	/// \param[in] sequence - loop body statement sequence
	/// \param[in] condition - loop condition expression
	///
	/// \c Created statement object is returned on success
	/// or \c NULL otherwise.
	Semantics::Statement* recRepeatStatement(Semantics::StatementSequence* sequence,
		Semantics::Expression* condition);

	/// \brief
	/// Record FOR loopstatement
	///
	/// \param[in] dir - FOR loop direction
	/// \param[in] indexvariable - object access to variable used for indexing
	/// \param[in] initialvalue - initial index value expression
	/// \param[in] finalvalue - final index value expression
	/// \param[in] loopstatement - loop body statement
	///
	/// \c Created statement object is returned on success
	/// or \c NULL otherwise.
	Semantics::Statement* recForStatement(Semantics::ForStatement::Direction dir, 
		Semantics::ObjectAccess* indexvariable,
		Semantics::Expression* initialvalue, Semantics::Expression* finalvalue,
		Semantics::Statement* loopstatement);

	/// \brief
	/// Record directive
	///
	/// \param[in] identifier - directive text
	///
	/// \return
	/// Directive code is returned for given directive identifier. The
	/// result could be DIRECTIVE_UNKNOWN for an unknown / invalid directives.
	Directive recDirective(char* identifier);

	bool recProgramBlockEnd();
	bool recFunctionBlockBegin();
	bool recFunctionBlockEnd();
	bool recProcedureBlockBegin();
	bool recProcedureBlockEnd();

	Semantics::Procedure* recProcedureHeading(char* identifier,
		Semantics::Object* params);
	Semantics::Function* recFunctionHeading(char* identifier,
		Semantics::Object* params, Semantics::Type* resulttype);

	bool recProcedureDeclaration(Semantics::Procedure* procedure,
		Directive directive);

	bool recFunctionDeclaration(Semantics::Function* function, 
		Directive directive);

	Semantics::ActualParameter* recActualParameter(
		Semantics::ObjectAccess* variableaccess);

	Semantics::ActualParameter* recActualParameter(
		Semantics::Expression* expression);

	Semantics::ActualParameter* recActualParameterProc(char* identifier);
	Semantics::ActualParameter* recActualParameterFunc(char* identifier);

	Semantics::Statement* recProcedureStatement(char* identifier, 
		ActualParameterVector* parameters);
};

//--------------------------------------------------------------------

#endif /* OCT_SEMGEN_SEMGEN_HPP */
