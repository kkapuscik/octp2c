//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics generator class (implementation)
//--------------------------------------------------------------------

#include "semgen.hpp"

#include "semantics/simpletype.hpp"
#include "semantics/program.hpp"

#include "parser/parserincludes.hpp"

#include "semantics/const.hpp"
#include "semantics/simpleconst.hpp"
#include "semantics/simpletype.hpp"
#include "semantics/variable.hpp"
#include "semantics/objectaccess.hpp"
#include "semantics/assignmentstatement.hpp"
#include "semantics/expression.hpp"
#include "semantics/function.hpp"
#include "semantics/statementsequence.hpp"
#include "semantics/emptystatement.hpp"
#include "semantics/nilconst.hpp"
#include "semantics/ifstatement.hpp"
#include "semantics/repeatstatement.hpp"
#include "semantics/whilestatement.hpp"
#include "semantics/procedure.hpp"
#include "semantics/function.hpp"
#include "semantics/readprocedure.hpp"
#include "semantics/writeprocedure.hpp"
#include "semantics/actualparameter.hpp"
#include "semantics/procedurestatement.hpp"

using namespace Semantics;
using namespace std;

//--------------------------------------------------------------------

SemGen::SemGen(void)
    : m_global(NULL)
    , m_currentNamespace(NULL)
    , m_garbageCollector(NULL)
	, m_recognizeIdentifiers(true)
{
}

SemGen::~SemGen(void)
{
    free();
}

//--------------------------------------------------------------------

bool SemGen::init(void)
{
    free();
    
    m_global = new Global();
    m_currentNamespace = m_global;
    m_garbageCollector = new GarbageCollector("SemGen");
	m_recognizeIdentifiers = true;

    if(!createStandardObjects())
        return false;
    
    return true;
}

void SemGen::free(void)
{
    delete m_global;
    m_global = NULL;
    
    m_currentNamespace = NULL;
    
    delete m_garbageCollector;
    m_garbageCollector = NULL;
}

//--------------------------------------------------------------------

void SemGen::setIdentifiersRecognition(bool recognize)
{
	m_recognizeIdentifiers = recognize;
}

bool SemGen::getIdentifiersRecognition() const
{
	return m_recognizeIdentifiers;
}

//--------------------------------------------------------------------

bool SemGen::createStandardObjects(void)
{
	// create standard types
    m_global->add(new SimpleType(SimpleType::REAL));
    m_global->add(new SimpleType(SimpleType::INTEGER));
    m_global->add(new SimpleType(SimpleType::BOOLEAN));
    m_global->add(new SimpleType(SimpleType::CHAR));
    m_global->add(new SimpleType(SimpleType::STRING));
    m_global->add(new SimpleType(SimpleType::INVALID));
    
	// create standard procedures & functions
	m_global->add(new ReadProcedure(false, "read", m_global));
	m_global->add(new ReadProcedure(true, "readln", m_global));
	m_global->add(new WriteProcedure(false, "write", m_global));
	m_global->add(new WriteProcedure(true, "writeln", m_global));


    return true;
}

//--------------------------------------------------------------------

const Global* SemGen::getGlobal(void) const
{
    return m_global;
}

void SemGen::pushNamespace(Namespace* nspace)
{
    assert(nspace != NULL);
    assert(nspace->getOuter() == m_currentNamespace);
    
    m_currentNamespace = nspace;
}

void SemGen::popNamespace(void)
{
    assert(m_currentNamespace->getOuter() != NULL);
    
    m_currentNamespace = m_currentNamespace->getOuter();
}

Namespace* SemGen::getCurrentNamespace(void)
{
    return m_currentNamespace;
}

//--------------------------------------------------------------------

bool SemGen::recProgramHeading(char* identifier, IdentifierVector* parameters)
{
    Global* outer = cast<Global>(getCurrentNamespace());

    assert(outer != NULL);

    Program* program = new Program(identifier, outer);

	/// \todo Support for parameters
	if(parameters != NULL)
	{
		cerr << "Program parameters not supported" << endl;
		for(int i=0; i<(int)parameters->size(); ++i)
			FREE(parameters->at(i));
		FREE(parameters);
	}

	// free identifier memory
    FREE(identifier);

	// add program to current namespace (Global)
    getCurrentNamespace()->add(program);
	// push program as current namespace
    pushNamespace(program);

	return true;
}

//--------------------------------------------------------------------

bool SemGen::recLabelDeclarationPart(LabelVector* labels)
{
	// check for current namespace type
	assert(
		getCurrentNamespace()->isOfClass(clid_Program) ||
		getCurrentNamespace()->isOfClass(clid_Procedure) ||
		getCurrentNamespace()->isOfClass(clid_Function));

	// add labels to current NamedBlock
	NamedBlock* block = cast<NamedBlock>(getCurrentNamespace());
	assert(block);
	LabelVector::const_iterator iter;
	for(iter = labels->begin(); iter != labels->end(); ++iter)
	{
		// check for wrong label value
		if(*iter < 0 || *iter > 9999)
		{
			cerr << "Wrong label value" << endl;
			return false;
		}
		// check if add is successful (could fail if label already exists)
		if(block->addLabel(*iter) == false)
		{
			cerr << "Label already exists" << endl;
			return false;
		}
	}

	// free labels vector
	FREE(labels);

	// success
	return true;
}

//--------------------------------------------------------------------

Const* SemGen::recConstant(Operator signop, Const* number)
{
	SimpleConst* simple = cast<SimpleConst>(number);
	assert(simple != NULL);

	if(signop == O_MINUS)
	{
		const SimpleType* type = cast<SimpleType>(simple->getType());
		assert(type != NULL);

		if(type->getTypeId() == SimpleType::INTEGER)
		{
			int val;
			simple->getValue(val);
			simple->setValue(-val);
		}
		else if(type->getTypeId() == SimpleType::REAL)
		{
			double val;
			simple->getValue(val);
			simple->setValue(-val);
		}
		else
		{
			cerr << "Wrong constant value type" << endl;
			return NULL;
		}
	}

	return simple;
}

Const* SemGen::recConstant(Operator signop, char* identifier)
{
	const Object* obj = getCurrentNamespace()->find(identifier, true);
	if(obj != NULL)
	{
		const SimpleConst* simple = cast<SimpleConst>(obj);
		if(simple != NULL)
		{
			const SimpleType* type = simple->getSimpleType();
			if(type->getTypeId() == SimpleType::INTEGER)
			{
				int value;
				simple->getValue(value);
				if(signop == O_MINUS)
					value = -value;
				return REG(new SimpleConst(value));
			}
			else if(type->getTypeId() == SimpleType::REAL)
			{
				double value;
				simple->getValue(value);
				if(signop == O_MINUS)
					value = -value;
				return REG(new SimpleConst(value));
			}
			else if(signop == O_NOOP && type->getTypeId() == SimpleType::STRING)
			{
				String value;
				simple->getValue(value);
				return REG(new SimpleConst(value));
			}
			else
			{
				cerr << identifier << " is not an acceptable constant" << endl;
				return NULL;
			}
		}
		else
		{
			cerr << identifier << " is not an acceptable constant" << endl;
			return NULL;
		}
	}
	else
	{
		cerr << "Unknown identifier (" << identifier << ")" << endl;
		return NULL;
	}
}

Const* SemGen::recConstant(char* value)
{
	String tmpval = value;

	// free value memory
    FREE(value);

	// create object
	return REG(new SimpleConst(tmpval));
}

bool SemGen::recConstantDefinition(char* name, Semantics::Const* value)
{
	String id = name;

	// free identifier memory
    FREE(name);	

	// find object with given name
	Object* obj = getCurrentNamespace()->find(id, false);
	if(obj)
	{
		cerr << "Object with name " << id << " already exists" << endl;
		return false;
	}

	// set constant identifier
	value->setId(id);

	// insert into namespace
	if(getCurrentNamespace()->add(value) == true)
	{
		UNREG(value);
		return true;
	}
	else
	{
		cerr << "Error occured when adding to current namespace" << endl;
		return false;
	}
}

Semantics::Const* SemGen::recUnsignedNumber(unsigned int value)
{
	int tmpval = value;
	return REG(new SimpleConst(tmpval));
}

Semantics::Const* SemGen::recUnsignedNumber(double value)
{
	return REG(new SimpleConst(value));
}

//--------------------------------------------------------------------

const SimpleType* SemGen::getSimpleType(SimpleType::TypeId tid) const
{
	const Object* obj = getGlobal()->find(SimpleType::getTypeName(tid), false);
	assert(obj);
	const SimpleType* simple = cast<SimpleType>(obj);
	assert(simple);

	return simple;
}

//--------------------------------------------------------------------

bool SemGen::recVariableDeclaration(IdentifierVector* identifiers, Type* type)
{
	assert(identifiers);
	assert(type);

	// result
	bool success = true;

	// process all identifiers
	for(int i=0; success && i<(int)identifiers->size(); ++i)
	{
		// get ident
		char* ident = identifiers->at(i);

		// check if object with given name already exists
		Object* obj = getCurrentNamespace()->find(ident, false);
		if(obj != NULL)
		{
			cerr << "Object with name " << ident << " already exists" << endl;
			success = false;
		}
		else
		{
			// create variable
			Variable* var = new Variable(type, ident);

			// insert into namespace
			if(getCurrentNamespace()->add(var) == false)
			{
				delete var;
				cerr << "Error occured when adding to current namespace" << endl;
				success = false;
			}
		}
	}

	// free identifiers
	for(int i=0; i<(int)identifiers->size(); ++i)
		FREE(identifiers->at(i));

	// free labels vector
	FREE(identifiers);

	return success;
}

//--------------------------------------------------------------------

Semantics::Object* SemGen::findExistingObject(const String& id, bool recursive)
{
	// check if object with given name already exists
	Object* obj = getCurrentNamespace()->find(id, recursive);
	if(obj != NULL)
		return obj;
	else
		return NULL;
}

Type* SemGen::findType(char* identifier)
{
	String id = identifier;

	// Free identifier
	FREE(identifier);

	// check if object with given name already exists
	Object* obj = getCurrentNamespace()->find(id, true);
	if(obj != NULL)
	{
		if(obj->isOfClass(clid_Type))
			return cast<Type>(obj);
		else
		{
			cerr << "Identifier " << id << " is not a type name" << endl;
			return NULL;
		}
	}
	else
	{
		cerr << "There is no type " << id << endl;
		return NULL;
	}
}

Semantics::Variable* SemGen::findVariable(char* identifier)
{
	String id = identifier;

	// Free identifier
	FREE(identifier);

	// check if object with given name already exists
	Object* obj = getCurrentNamespace()->find(id, true);
	if(obj != NULL)
	{
		if(obj->isOfClass(clid_Variable))
			return cast<Variable>(obj);
		else
		{
			cerr << "Identifier " << id << " is not a variable name" << endl;
			return NULL;
		}
	}
	else
	{
		cerr << "There is no variable " << id << endl;
		return NULL;
	}
}

//--------------------------------------------------------------------

Semantics::ObjectAccess* SemGen::recObjectAccess(char* identifier)
{
	String id(identifier);

	FREE(identifier);
	identifier = NULL;

	/// \todo Checking for WITH field designators

	Object* obj = this->findExistingObject(id);
	if(obj)
	{
		// Function identifier
		if(obj->isOfClass(clid_Function))
		{
			if(obj == getCurrentNamespace())
			{
				return REG(new ObjectAccess(cast<Function>(obj)));
			}
			else
			{
				cerr << "Assigning value for function " << id << " that is not the current function" << endl;
			}
		}
		// Entire variable
		else if(obj->isOfClass(clid_Variable))
		{
			return REG(new ObjectAccess(cast<Variable>(obj)));
		}
		// Constans
		else if(obj->isOfClass(clid_Const))
		{
			return REG(new ObjectAccess(cast<Const>(obj)));
		}
		else
		{
			cerr << "Invalid type of accessed object " << id << endl;
		}
	}
	else
		cerr << "Accessing nonexisting object " << id << endl;

	return NULL;
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recAssignmentStatement(
	Semantics::ObjectAccess* accessedvariable,
	Semantics::Expression* valueexpression)
{
	assert(accessedvariable);
	assert(valueexpression);

	/// \todo Checking accessed element?

	// Checking types
	if(AssignmentStatement::checkTypes(accessedvariable, valueexpression) == false)
	{
		cerr << "Types not compatible - cannot assign value" << endl;
		return NULL;
	}

	// unregister access and expression
	UNREG(accessedvariable);
	UNREG(valueexpression);

	// create and register statement object
	return REG(new AssignmentStatement(accessedvariable, valueexpression));
}

//--------------------------------------------------------------------

bool SemGen::isRecordVariable(ObjectAccess* access)
{
	/// \todo Checking for record variable
	return false;
}

bool SemGen::isEntireVariable(ObjectAccess* access)
{
	return access->getAccessType() == ObjectAccess::ENTIRE_VARIABLE;
}

//--------------------------------------------------------------------

Semantics::Expression* SemGen::recExpression(Semantics::Typed* value)
{
	assert(value);

	if(value->isOfClass(clid_Expression))
		return cast<Expression>(value);
	else
	{
		UNREG(value);
		return REG(new Expression(value));
	}
}

Semantics::Expression* SemGen::recExpression(
	Semantics::Typed* value1, Operator oper, Semantics::Typed* value2)
{
	assert(value1);
	assert(value2);

	/// \todo Checking types

	UNREG(value1);
	UNREG(value2);

	return REG(new Expression(oper, value1, value2));
}

//--------------------------------------------------------------------

Semantics::Typed* SemGen::recSimpleExpression(
	Operator unoper, Semantics::Typed* value)
{
	assert(value);

	/// \todo Checking types

	if(unoper == O_NOOP)
		return value;
	else
	{
		UNREG(value);
		return REG(new Expression(unoper, value));
	}
}

Semantics::Typed* SemGen::recSimpleExpression(Semantics::Typed* value1,
	Operator oper, Semantics::Typed* value2)
{
	assert(value1);
	assert(value2);

	/// \todo Checking types

	UNREG(value1);
	UNREG(value2);
	return REG(new Expression(oper, value1, value2));
}

//--------------------------------------------------------------------

Semantics::Typed* SemGen::recTerm(
	Semantics::Typed* value1, Operator oper, Semantics::Typed* value2)
{
	assert(value1);
	assert(value2);

	/// \todo Checking types

	UNREG(value1);
	UNREG(value2);

	return REG(new Expression(oper, value1, value2));
}

//--------------------------------------------------------------------

Semantics::StatementSequence* SemGen::recStatementSequence(
	Semantics::Statement* statement)
{
	assert(statement);

	StatementSequence* seq = REG(new StatementSequence());

	return recStatementSequence(seq, statement);
}

Semantics::StatementSequence* SemGen::recStatementSequence(
	Semantics::StatementSequence* sequence,
	Semantics::Statement* statement)
{
	assert(sequence);
	assert(statement);

	UNREG(statement);
	sequence->addStatement(statement);

	return sequence;
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recEmptyStatement()
{
	return REG(new EmptyStatement());
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recStatement(LabelType label, Semantics::Statement* statement)
{
	if(label == INVALID_LABEL_VALUE)
		return statement;
	else
	{
		if(getCurrentNamespace()->isOfClass(clid_NamedBlock))
		{
			NamedBlock* block = cast<NamedBlock>(getCurrentNamespace());
			if(block->existLabel(label))
			{
				/// \todo - checking for multiple used label
				statement->setLabel(label);

				return statement;
			}
			else
			{
				cerr << "Unknown Label " << label << " used" << endl;
			}
		}
		else
		{
			cerr << "Label " << label << " used in not allowed place" << endl;
		}
	}
	return NULL;
}

//--------------------------------------------------------------------

bool SemGen::recStatementPart(Semantics::StatementSequence* sequence)
{
	assert(sequence);

	// check for current namespace type
	assert(
		getCurrentNamespace()->isOfClass(clid_Program) ||
		getCurrentNamespace()->isOfClass(clid_Procedure) ||
		getCurrentNamespace()->isOfClass(clid_Function));

	// get current NamedBlock
	NamedBlock* block = cast<NamedBlock>(getCurrentNamespace());
	assert(block);

	UNREG(sequence);
	block->setStatementSequence(sequence);

	return true;
}

//--------------------------------------------------------------------

Semantics::Const* SemGen::recNil()
{
	return REG(new NilConst());
}

//--------------------------------------------------------------------

Semantics::Typed* SemGen::recNotFactor(Operator oper, Semantics::Typed* factor)
{
	assert(factor);
	assert(oper == O_NOT);

	/// \todo Checking types

	UNREG(factor);
	return REG(new Expression(oper, factor));
}

//--------------------------------------------------------------------

bool SemGen::checkFactorAccess(Semantics::Typed* typed)
{
	assert(typed);

	if(typed->isOfClass(clid_ObjectAccess))
		return true;
	
	/// \todo Other checkings (bound itentifiers, functions, constant access etc.)
	
	return false;
}

//--------------------------------------------------------------------

Semantics::Expression* SemGen::checkBooleanExpression(Semantics::Expression* expr)
{
	assert(expr);

	const Type* type = expr->getType();

	/// \todo CHECK TYPE!!!
	if(type == NULL)
		return expr;

	if(type->isOfClass(clid_SimpleType))
	{
		const SimpleType* simple = cast<SimpleType>(type);
		if(simple->getTypeId() == SimpleType::BOOLEAN)
		{
			return expr;
		}
	}

	cerr << "Expression does not evaluate to boolean type" << endl;

	return NULL;
}

//--------------------------------------------------------------------

Semantics::ObjectAccess* SemGen::checkVariableAccess(
	Semantics::ObjectAccess* access)
{
	if(access->getAccessType() == ObjectAccess::ENTIRE_VARIABLE)
		return access;
	else
	{
		Object* obj = access->getAccessedObject();
		cerr << "Accessed object " << obj->getId() << " is not a variable" << endl;
		return NULL;
	}
}

Semantics::ObjectAccess* SemGen::checkVariableOrFunctionAccess(
	Semantics::ObjectAccess* access)
{
	if(checkVariableAccess(access))
		return access;
	else if(access->getAccessType() == ObjectAccess::FUNCTION_RETURN)
		return access;
	else
	{
		Object* obj = access->getAccessedObject();
		cerr << "Accessed object " << obj->getId() << " is not a variable or function return value" << endl;
		return NULL;
	}
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recIfStatement(Semantics::Expression* condition,
	Semantics::Statement* ifstatement)
{
	assert(condition);
	assert(ifstatement);

	UNREG(condition);
	UNREG(ifstatement);

	return REG(new IfStatement(condition, ifstatement));
}

Semantics::Statement* SemGen::recIfStatement(Semantics::Expression* condition,
	Semantics::Statement* ifstatement, Semantics::Statement* elsestatement)
{
	assert(condition);
	assert(ifstatement);
	assert(elsestatement);

	UNREG(condition);
	UNREG(ifstatement);
	UNREG(elsestatement);

	return REG(new IfStatement(condition, ifstatement, elsestatement));
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recWhileStatement(Semantics::Expression* condition,
	Semantics::Statement* loopstatement)
{
	assert(condition);
	assert(loopstatement);

	UNREG(condition);
	UNREG(loopstatement);

	return REG(new WhileStatement(condition, loopstatement));
}

Semantics::Statement* SemGen::recRepeatStatement(Semantics::StatementSequence* sequence,
	Semantics::Expression* condition)
{
	assert(condition);
	assert(sequence);

	UNREG(condition);
	UNREG(sequence);

	return REG(new RepeatStatement(condition, sequence));
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recForStatement(Semantics::ForStatement::Direction dir, 
	Semantics::ObjectAccess* indexvariable,
	Semantics::Expression* initialvalue, Semantics::Expression* finalvalue,
	Semantics::Statement* loopstatement)
{
	assert(indexvariable);
	assert(initialvalue);
	assert(finalvalue);
	assert(loopstatement);

	/// \todo Support for checking threatening index variable
	/// \todo Checking types

	UNREG(indexvariable);
	UNREG(initialvalue);
	UNREG(finalvalue);
	UNREG(loopstatement);

	return REG(new ForStatement(dir, indexvariable,
		initialvalue, finalvalue, loopstatement));
}

//--------------------------------------------------------------------

Directive SemGen::recDirective(char* identifier)
{
	Directive res;

	if(strcmp(identifier, "forward") == 0)
		res = DIRECTIVE_FORWARD;
	else
	{
		cerr << "Unknown directive " << identifier << endl;
		res = DIRECTIVE_UNKNOWN;
	}

	FREE(identifier);

	return res;
}

//--------------------------------------------------------------------
/*
Semantics::Function* SemGen::recFunctionIdentification(char* identifier)
{
	String id(identifier);

	FREE(identifier);

	Object* obj = findExistingObject(id);
	if(obj)
	{
		if(obj->isOfClass(clid_Function))
		{
			Function* func = cast<Function>(obj);
			if(func->getForwardStatus() == true)
			{
                pushNamespace(func);
				return func;
			}
			else
			{
				cerr << "Function " << id << " is already defined" << endl;
			}
		}
		else
		{
			cerr << id << " is not a function" << endl;
		}
	}
	else
	{
		cerr << "Cannot find function " << id << endl;
	}
	return NULL;
}

Semantics::Procedure* SemGen::recProcedureIdentification(char* identifier)
{
	String id(identifier);

	FREE(identifier);

	Object* obj = findExistingObject(id);
	if(obj)
	{
		if(obj->isOfClass(clid_Procedure))
		{
			Procedure* proc = cast<Procedure>(obj);
			if(proc->getForwardStatus() == true)
			{
                pushNamespace(proc);
				return proc;
			}
			else
			{
				cerr << "Procedure " << id << " is already defined" << endl;
			}
		}
		else
		{
			cerr << id << " is not a procedure" << endl;
		}
	}
	else
	{
		cerr << "Cannot find procedure " << id << endl;
	}
	return NULL;
}
*/

//--------------------------------------------------------------------

bool SemGen::recProgramBlockEnd()
{
	if(getCurrentNamespace()->isOfClass(clid_Program))
	{
		popNamespace();
		return true;
	}
	else
	{
		cerr << "FATAL: Program block end not in a program" << endl;
		return false;
	}
}

//--------------------------------------------------------------------

bool SemGen::recFunctionBlockBegin()
{
	if(getCurrentNamespace()->isOfClass(clid_Function))
	{
		Function* func = cast<Function>(getCurrentNamespace());

		// not defined || forwarded
		if((func->getDeclarationStatus() == NamedBlock::STATUS_INVALID) ||
			(func->getDeclarationStatus() == NamedBlock::STATUS_FORWARD))
		{
			func->setDeclarationStatus(NamedBlock::STATUS_DECLARED);
			return true;
		}
		else
		{
			cerr << "Function body already declared" << endl;
			return false;
		}
	}
	else
	{
		cerr << "FATAL: Function block begin not in a function" << endl;
		return false;
	}
}

bool SemGen::recFunctionBlockEnd()
{
	if(getCurrentNamespace()->isOfClass(clid_Function))
	{
		popNamespace();
		return true;
	}
	else
	{
		cerr << "FATAL: Function block end not in a function" << endl;
		return false;
	}
}

//--------------------------------------------------------------------

bool SemGen::recProcedureBlockBegin()
{
	if((getCurrentNamespace()->isOfClass(clid_Procedure) == true) &&
		(getCurrentNamespace()->isOfClass(clid_Function) == false))
	{
		Procedure* proc = cast<Procedure>(getCurrentNamespace());

		// not defined || forwarded
		if((proc->getDeclarationStatus() == NamedBlock::STATUS_INVALID) ||
			(proc->getDeclarationStatus() == NamedBlock::STATUS_FORWARD))
		{
			proc->setDeclarationStatus(NamedBlock::STATUS_DECLARED);
			return true;
		}
		else
		{
			cerr << "Procedure body already declared" << endl;
			return false;
		}
	}
	else
	{
		cerr << "FATAL: Procedure block begin not in a procedure" << endl;
		return false;
	}
}

bool SemGen::recProcedureBlockEnd()
{
	if((getCurrentNamespace()->isOfClass(clid_Procedure) == true)
		&& (getCurrentNamespace()->isOfClass(clid_Function) == false))
	{
		popNamespace();
		return true;
	}
	else
	{
		cerr << "FATAL: Procedure block end not in a procedure" << endl;
		return false;
	}
}

//--------------------------------------------------------------------

Semantics::Procedure* SemGen::recProcedureHeading(char* identifier,
	Semantics::Object* params)
{
	String id(identifier);

	FREE(identifier);

	Object* obj;
	// check current namespace level
	obj = getCurrentNamespace()->find(id, false);
	if(obj)
	{
//		if(obj->
	}
	else
	{
		// check global namespace
		obj = getCurrentNamespace()->find(id, true);
		if(obj)
		{
		}
		else
		{
		}
	}
	return NULL;
}

Semantics::Function* SemGen::recFunctionHeading(char* identifier,
	Semantics::Object* params, Semantics::Type* resulttype)
{
	return NULL;
}

//--------------------------------------------------------------------

bool SemGen::recProcedureDeclaration(Semantics::Procedure* procedure, 
									 Directive directive)
{
	if(directive == DIRECTIVE_FORWARD)
	{
		switch(procedure->getDeclarationStatus())
		{
		case NamedBlock::STATUS_INVALID:
			// function header happened only
			return true;

		case NamedBlock::STATUS_DECLARED:
			cerr << "Procedure already declared" << endl;
			return false;

		case NamedBlock::STATUS_FORWARD:
			cerr << "Procedure already forwarded" << endl;
			return false;

		default:
			assert(0);
			return false;
		}
	}
	else
	{
		cerr << "Invalid directive" << endl;
		return false;
	}
}

//--------------------------------------------------------------------

bool SemGen::recFunctionDeclaration(Semantics::Function* function, 
									Directive directive)
{
	if(directive == DIRECTIVE_FORWARD)
	{
		switch(function->getDeclarationStatus())
		{
		case NamedBlock::STATUS_INVALID:
			// function header happened only
			return true;

		case NamedBlock::STATUS_DECLARED:
			cerr << "Function already declared" << endl;
			return false;

		case NamedBlock::STATUS_FORWARD:
			cerr << "Function already forwarded" << endl;
			return false;

		default:
			assert(0);
			return false;
		}
	}
	else
	{
		cerr << "Invalid directive" << endl;
		return false;
	}
}

//--------------------------------------------------------------------

Semantics::ActualParameter* SemGen::recActualParameter(ObjectAccess* variableaccess)
{
	assert(variableaccess);

	/// \todo Checking if it is variable access
	/// \todo Checking types if needed

	UNREG(variableaccess);

	return REG(new ActualParameter(variableaccess));
}

Semantics::ActualParameter* SemGen::recActualParameter(
	Semantics::Expression* expression)
{
	assert(expression);

	/// \todo Checking if it is variable access
	/// \todo Checking types if needed

	UNREG(expression);

	return REG(new ActualParameter(expression));
}

Semantics::ActualParameter* SemGen::recActualParameterProc(char* identifier)
{
	String id(identifier);

	FREE(identifier);

	Object* obj = findExistingObject(id);
	assert(obj->isOfClass(clid_Procedure) && !obj->isOfClass(clid_Function));

	return REG(new ActualParameter(cast<Procedure>(obj)));	
}

Semantics::ActualParameter* SemGen::recActualParameterFunc(char* identifier)
{
	String id(identifier);

	FREE(identifier);

	Object* obj = findExistingObject(id);
	assert(obj->isOfClass(clid_Function));

	return REG(new ActualParameter(cast<Function>(obj)));	
}

//--------------------------------------------------------------------

Semantics::Statement* SemGen::recProcedureStatement(char* identifier, 
	ActualParameterVector* parameters)
{
	String id(identifier);

	FREE(identifier);

	Procedure* proc = cast<Procedure>(findExistingObject(id));

	assert(proc->isOfClass(clid_Procedure) && !proc->isOfClass(clid_Function));

	ProcedureStatement* stat = REG(new ProcedureStatement(proc));
	if(parameters)
	{
		int n = (int)parameters->size();
		for(int i=0; i<n; ++i)
		{
			ActualParameter* param = parameters->at(i);
			stat->addParameter(param);
			UNREG(param);
		}
		FREE(parameters);
	}

	return stat;
}

//--------------------------------------------------------------------
