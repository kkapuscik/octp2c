//--------------------------------------------------------------------
/// \file
/// \brief
/// String type and manipulation (implementation)
//--------------------------------------------------------------------

#include "string.hpp"

//--------------------------------------------------------------------

int Compare(const String& str1, const String& str2)
{
	return str1.compare(str2);
}

int CompareNC(const String& str1, const String& str2)
{
	String::const_iterator it1,it2;
	String::const_iterator ie1,ie2;

	// Get iterators for string 1
	it1 = str1.begin();
	ie1 = str1.end();

	// Get iterators for string 2
	it2 = str2.begin();
	ie2 = str2.end();

	// Compare until end of any string
	while((it1 != ie1) && (it2 != ie2))
	{
		int res = tolower(*it1) - tolower(*it2);
		if(res)
			return res;

		++it1;
		++it2;
	}

	// String1 still have chars
	if(it1 != ie1)
		return 1;
	// String2 still have chars
	if(it2 != ie2)
		return -1;
	// Both strings finished
	return 0;
}

//--------------------------------------------------------------------
