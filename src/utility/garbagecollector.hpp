//--------------------------------------------------------------------
/// \file
/// \brief
/// Garbage collector class
//--------------------------------------------------------------------

#ifndef OCT_UTILITY_GARBAGECOLLECTOR_HPP
#define OCT_UTILITY_GARBAGECOLLECTOR_HPP

//--------------------------------------------------------------------

#include "log/log.hpp"
#include "string.hpp"

//--------------------------------------------------------------------

/// \brief
/// Garbage collector class
///
/// Garbage collector manages a collection of pointers.
/// Pointers can be added (store) and removed (free) dynamically to
/// the collector.
/// Each pointer stored in collector will be deleted when garbage
/// collector is deleted or deleteAll method is called.
/// Logging capability is added so the collector activity could be
/// monitored.
class GarbageCollector
{
private:
    class Comparator;

	/// \brief
    /// Stored element base class
    ///
    /// Element base class provides an abstract callDelete
    /// method which is used by derived classes to provide
    /// pointer type dependent 'delete &lt;pointer&gt;' operation.
    class ElementBase
    {
        /// Comparator class need access to m_pointer field
        friend class Comparator;

    protected:
        /// Stored pointer
        void* m_pointer;
#ifdef _DEBUG
        /// Source file name
        String m_sourceFile;
        /// Source line number
        int m_sourceLine;
#endif /* _DEBUG */

    protected:
		/// \brief
        /// Constructor
        ///
        /// Initializes object with given pointer.
        ///
        /// \param[in] pointer - pointer to store
#ifdef _DEBUG
        ElementBase(void* pointer, String srcfile = String(), int srcline = -1)
            : m_pointer(pointer)
            , m_sourceFile(srcfile)
            , m_sourceLine(srcline)
        {
        }
#else /* _DEBUG */
        ElementBase(void* pointer)
            : m_pointer(pointer)
        {
        }
#endif /* _DEBUG */

    public:
        /// \brief
        /// Delete stored pointer
        ///
        virtual void callDelete(void) = 0;

        /// \brief
        /// Get stored pointer
        ///
        /// \return
        /// Stored pointer.
        const void* get(void) const
        {
            return m_pointer;
        }

#ifdef _DEBUG
        /// \brief
        /// Get source file name
        ///
        /// \return
        /// Source file name.
        const String& getSourceFile(void) const
        {
            return m_sourceFile;
        }

        /// \brief
        /// Get source file line
        ///
        /// \return
        /// Source file line.
        int getSourceLine(void) const
        {
            return m_sourceLine;
        }
#endif /* _DEBUG */
    };

    /// \brief
    /// Template stored element class
    ///
    /// This class extends ElementBase and provides the
    /// virtual 'callDelete' method which calls delete operator
    /// with proper type cast of stored pointer.
    template <class T>
    class Element : public ElementBase
    {
    public:
        /// \brief
        /// Constructor
        ///
        /// Initializes object with given pointer.
        ///
        /// \param[in] pointer - pointer to store
#ifdef _DEBUG
        Element(T* pointer, String srcfile = String(), int srcline = -1)
            : ElementBase(pointer, srcfile, srcline)
        {
        }
#else /* _DEBUG */
        Element(T* pointer)
            : ElementBase(pointer)
        {
        }
#endif /* _DEBUG */
        // ElementBase methods
        virtual void callDelete(void)
        {
            T* tmp = (T*)m_pointer;
            // It is safe because it was downcasted by contructor
            delete tmp;
        }
    };

	/// \brief
    /// Comparision functor
	///
    class Comparator
    {
    public:
        /// \brief
        /// Object comparision function call operator
        ///
        /// \param[in] eb1 - pointer to compare
        /// \param[in] eb2 - pointer to compare
        ///
        /// \return
        /// Method returns \c true if eb1 < eb2 or \c false otherwise.
        bool operator () (const ElementBase* eb1, const ElementBase* eb2) const
        {
            return eb1->m_pointer < eb2->m_pointer;
        }
    };

    /// Element set collection type
    typedef std::set<ElementBase*,Comparator> ElementSet;
    /// Element collection iterator type
    typedef ElementSet::iterator Iterator;

private:
    /// Elements collection
    ElementSet m_elements;
    /// Collector name
    String m_name;

public:
    /// \brief
    /// Constructor
    ///
    /// Creates collection with given name.
    ///
    /// \param[in] name - name of the collector
    GarbageCollector(const String& name)
        : m_name(name)
    {
    }

    /// \brief
    /// Destructor
    ///
    /// Calls deleteAll method.
    ~GarbageCollector(void)
    {
        deleteAll();
    }

    /// \brief
    /// Delete all stored elements
    ///
    void deleteAll(void)
    {
        // delete all stored data
        Iterator iter;
        for(iter = m_elements.begin(); iter != m_elements.end(); ++iter)
        {
#ifdef _DEBUG
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Delete: " << (*iter)->get()
                << " stored at (" << (*iter)->getSourceFile() << ", " << (*iter)->getSourceLine() << ')'
                );
#else /* _DEBUG */
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Delete: " << (*iter)->get()
                );
#endif /* _DEBUG */
            (*iter)->callDelete();
            delete *iter;
        }
        m_elements.clear();
    }

    /// \brief
    /// Store given pointer
    ///
    /// \param[in] pointer - pointer to store
    template <class T>
#ifdef _DEBUG
    void store(T* pointer, char* srcfile, int srcline)
#else /* _DEBUG */
    void store(T* pointer)
#endif /* _DEBUG */
    {
        // check if element is not already stored
        Iterator iter = find(pointer);
        if(iter == m_elements.end())
        {
#ifdef _DEBUG
            // remember given pointer
            m_elements.insert(new Element<T>(pointer, srcfile, srcline));

            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Store: " << (void*)(pointer)
                << " at (" << srcfile << ", " << srcline << ')'
                );
#else /* _DEBUG */
            // remember given pointer
            m_elements.insert(new Element<T>(pointer));

            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Store: " << (void*)(pointer)
                );
#endif /* _DEBUG */
        }
        else
        {
#ifdef _DEBUG
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Duplicated store: " << (*iter)->get()
                << " at (" << srcfile << ", " << srcline << ')'
                << " before at (" << (*iter)->getSourceFile() << ", " << (*iter)->getSourceLine() << ')'
                );
#else /* _DEBUG */
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Duplicated store: " << (void*)(pointer)
                );
#endif /* _DEBUG */
        }
    }

    /// \brief
    /// Release given pointer
    ///
    /// \param[in] pointer - pointer to release
    template <class T>
#ifdef _DEBUG
    void free(T* pointer, char* srcfile, int srcline)
#else /* _DEBUG */
    void free(T* pointer)
#endif /* _DEBUG */
    {
        // check if element is stored
        Iterator iter = find(pointer);
        if(iter != m_elements.end())
        {
#ifdef _DEBUG
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Free: " << (void*)(pointer)
                << " at (" << srcfile << ", " << srcline << ')'
                << " stored at (" << (*iter)->getSourceFile() << ", " << (*iter)->getSourceLine() << ')'
                );
#else /* _DEBUG */
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Free: " << (void*)(pointer)
                );
#endif /* _DEBUG */
            delete *iter;
            m_elements.erase(iter);
        }
        else
        {
#ifdef _DEBUG
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Invalid free: " << (void*)(pointer)
                << " at (" << srcfile << ", " << srcline << ')'
                );
#else /* _DEBUG */
            LOG(LOG_GARBAGECOLLECTOR,
                this << " GarbageCollector " << m_name
                << " --- Invalid free: " << (void*)(pointer)
                );
#endif /* _DEBUG */
        }
    }

private:
    /// \brief
    /// Find given pointer in collection
    ///
    /// \param[in] pointer - pointer to find
    ///
    /// \return
    /// Iterator for position of found pointer or collection.end()
    /// if pointer cannot be found.
    Iterator find(void* pointer)
    {
        // casted to char to omit the 'delete (void*) is undefined'
        // warning in callDelete
        // it could be also removed using template<> find but this
        // will produce extra code for every type so it makes no sense
        // to use it
        Element<char> tmp((char*)pointer);

        return m_elements.find(&tmp);
    }
};

//--------------------------------------------------------------------

#endif /* OCT_UTILITY_GARBAGECOLLECTOR_HPP */
