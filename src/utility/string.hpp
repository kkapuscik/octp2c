//--------------------------------------------------------------------
/// \file
/// \brief
/// String type and manipulation
//--------------------------------------------------------------------

#ifndef OCT_UTILITY_STRING_HPP
#define OCT_UTILITY_STRING_HPP

//--------------------------------------------------------------------

#include <string>
#include <cctype>

//--------------------------------------------------------------------

/// String type
typedef std::string String;

//--------------------------------------------------------------------

/// Case sensitive string comparision
int Compare(const String& str1, const String& str2);

/// Case insensitive string comparision (CompareNoCase)
int CompareNC(const String& str1, const String& str2);

//--------------------------------------------------------------------

#endif /* OCT_UTILITY_STRING_HPP */
