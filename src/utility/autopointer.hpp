//--------------------------------------------------------------------
/// \file
/// \brief
/// Auto pointer class
///
/// \author		Krzysztof Kapuscik
///
/// \version	0.1		(2004-10-13)
///		First version
/// \version	0.2		(2004-10-13)
///		- references count is now private in PointerInfo
///		- incrementing/decrementing references moved to PointerInfo
///		- incrementing/decrementing copy & free functions
///		- taking pointer 'out of sharing' method take
///		- autopointer comparision operators
//--------------------------------------------------------------------

#ifndef OCT_UTILITY_AUTOPOINTER_HPP
#define OCT_UTILITY_AUTOPOINTER_HPP

//--------------------------------------------------------------------

#include <cassert>

//--------------------------------------------------------------------

/// \brief
/// Auto pointer class
///
/// Auto pointer class is a class for automatically managing pointers.
/// It provides reference counting and automatic destruction of pointers
/// with no references.
///
/// \warning
/// Always copy AutoPointer object and not pointers they are holding!
/// If you will copy just a pointer it could be freed by AutoPointer
/// objects and this could provide to serious errors in application!
///
/// \warning
/// Using of m_info is not allowed because:
/// - It is public only because there is no workaround to make it private.
/// - Modyfying its contents could provide to serious errors.
///
/// \todo
/// Make m_info member private or protected if possible.
template <class Type>
class AutoPointer
{
private:
	/// \brief
	/// Pointer info class
	///
	/// Pointer info class holds a pointer and counts number of references to it.
	/// It also contains code for testing casting from other pointer types to
	/// the current type (castCheck function) - it casting is not possible a
	/// symantic error will be reported by compiler.
	class PointerInfo
	{
	protected:
		/// Number of references to pointer
		int m_references;

	public:
		/// Pointer holded
		Type* m_pointer;


	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] pointer - pointer value to be remembered
		PointerInfo(Type* pointer = NULL)
			: m_pointer(pointer)
			, m_references(1)
		{
		}

		/// \brief
		/// Cast checking code
		///
		/// This code checks casting from T1 pointer type to current
		/// pointer type and generates semantic errors it casting is
		/// not possible.
		///
		/// \param[in] pointer - any pointer
		///
		/// \return
		/// The same pointer given as parameter.
		template <class T1>
		static Type* castCheck(T1 pointer)
		{
			// This will generate an error for not allowed castings
			return pointer;
		}

		/// \brief
		/// Copy pointer (Increase pointer reference count)
		///
		/// Copy method increases pointer reference count and returns
		/// the this pointer.
		///
		/// \return
		/// Method returns \c this.
		PointerInfo* copy(void)
		{
			++m_references;

			return this;
		}

		/// \brief
		/// Free pointer (Decrease pointer reference count)
		///
		/// Free method decrements pointer reference count. If the reference
		/// count after decrementation is zero the pointer is no longer
		/// referenced and both: the pointer and this object will be
		/// deleted.
		///
		/// \return
		/// If number of references after the decrementation is not zero
		/// the returned value is \c this or it is \c NULL otherwise.
		///
		/// \warning
		/// This method could delete the object! Read the description
		/// carefully.
		PointerInfo* free(void)
		{
			if(--m_references == 0)
			{
				// delete holded pointer
				delete m_pointer;
				// delete itself
				delete this;

				return NULL;
			}
			else
				return this;
		}

		/// \brief
		/// Take pointer from sharing
		///
		/// \return
		/// Method returns pointer that is being 'taken' from sharing.
		Type* take(void)
		{
			Type* temp = m_pointer;

			// no pointer being shared
			m_pointer = NULL;

			return temp;
		}

	private:
		/// \brief
		/// Destructor
		///
		/// Destructor is private to prevent deletion when pointer
		/// still have a references - deletion is handled by the
		/// \c decrement function.
		~PointerInfo(void)
		{
		}
	};

public:
	/// Pointer to info structure holding pointer and counting references to it
	PointerInfo* m_info;

public:
	/// \brief
	/// Constructor
	///
	/// - Creates autopointer holding pointer given as parameter.
	///
	/// \param[in] pointer - pointer to "remember"
	AutoPointer(Type* pointer = NULL)
	{
		m_info = new PointerInfo(pointer);
	}

	/// \brief
	/// Constructor
	///
	/// - Creates object that shares pointer with autopointer given as parameter.
	///
	/// \param[in] other - autopointer object with which the pointer will be shared
	AutoPointer(const AutoPointer& other)
	{
		m_info = other.m_info->copy();
	}

	/// \brief
	/// Constructor
	///
	/// - Creates object that shares pointer with autopointer given as parameter.
	///   Constructor provided for autopointers for pointer or types related to
	///   this used.
	///
	/// \param[in] other - autopointer object with which the pointer will be shared
	template <class T1>
	AutoPointer(const AutoPointer<T1>& other)
	{
		// Casting checking code
		if(PointerInfo::castCheck((T1*)NULL) == NULL)
		{
			// Share info object with other AutoPointer
			m_info = (PointerInfo*)other.m_info->copy();
		}
		else
		{
			assert(0);
		}
	}

	/// \brief
	/// Destructor
	///
	/// Destructor automatically frees pointers with no references.
	~AutoPointer(void)
	{
		assert(m_info);

		m_info->free();
	}


	/// \brief
	/// Assignement operator for Pointers
	///
	/// \param[in] pointer - pointer to "remember"
	AutoPointer& operator = (Type* pointer)
	{
		// free currently hold pointer
		m_info->free();

		// remember new pointer
		m_info = new PointerInfo(pointer);

		return *this;
	}

	/// \brief
	/// Assignment operator for AutoPointers<CurrentType> support.
	///
	/// \param[in] other - autopointer object with which the pointer will be shared
	AutoPointer& operator = (const AutoPointer& other)
	{
		// "Free" currently holded pointer
		m_info->free();

		// Share info object with other AutoPointer
		m_info = other.m_info->copy();

		return *this;
	}

	/// \brief
	/// Assignment operator for AutoPointers<DerivedType> support.
	///
	/// \param[in] other - autopointer object with which the pointer will be shared
	template <class T1>
	AutoPointer& operator = (const AutoPointer<T1>& other)
	{
		// Casting checking code
		if(PointerInfo::castCheck((T1*)NULL) == NULL)
		{
			// "Free" currently holded pointer
			m_info->free();

			// Share info object with other AutoPointer
			m_info = (PointerInfo*)other.m_info->copy();

		}
		else
		{
			assert(0);
		}

		return *this;
	}


	/// \brief
	/// Arrow operator
	///
	Type* operator -> (void)
	{
		return m_info->m_pointer;
	}

	/// \brief
	/// Arrow operator (const)
	///
	const Type* operator -> (void) const
	{
		return m_info->m_pointer;
	}

	/// \brief
	/// Dereference operator
	///
	Type& operator * (void)
	{
		assert(m_info->m_pointer);

		return *m_info->m_pointer;
	}

	/// \brief
	/// Dereference operator (const)
	///
	const Type& operator * (void) const
	{
		assert(m_info->m_pointer);

		return *m_info->m_pointer;
	}

	/// \brief
	/// Get stored pointer
	///
	/// \return
	/// The stored pointer.
	Type* get(void)
	{
		return m_info->m_pointer;
	}

	/// \brief
	/// Take pointer from autopointers sharing
	///
	/// All autopointers sharing the pointer will be set to share 'NULL'
	/// pointer.
	///
	/// \return
	/// The stored pointer that has been taken.
	Type* take(void)
	{
		return m_info->take();
	}

	/// Equal operator for two auto pointers
	template <class T1, class T2>
	friend bool operator == (const AutoPointer<T1>& ap1, const AutoPointer<T2>& ap2);

	/// Not equal operator for two auto pointers
	template <class T1, class T2>
	friend bool operator != (const AutoPointer<T1>& ap1, const AutoPointer<T2>& ap2);
};

/// \brief
/// Equal operator for two auto pointers
///
template <class T1, class T2>
bool operator == (const AutoPointer<T1>& ap1, const AutoPointer<T2>& ap2)
{
	return ap1.m_info->m_pointer == ap2.m_info->m_pointer;
}

/// \brief
/// Not equal operator for two auto pointers
///
template <class T1, class T2>
bool operator != (const AutoPointer<T1>& ap1, const AutoPointer<T2>& ap2)
{
	return ap1.m_info->m_pointer != ap2.m_info->m_pointer;
}

//--------------------------------------------------------------------

template <class T>
AutoPointer<T> makeAutoPointer(T* pointer)
{
	return AutoPointer<T>(pointer);
}

//--------------------------------------------------------------------

#endif /* OCT_UTILITY_AUTOPOINTER_HPP */

