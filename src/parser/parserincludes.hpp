//--------------------------------------------------------------------
/// \file
/// \brief
/// Parser includes file
//--------------------------------------------------------------------

#ifndef OCT_PARSER_PARSERINCLUDES_HPP
#define OCT_PARSER_PARSERINCLUDES_HPP

//--------------------------------------------------------------------

#include "semantics/operators.hpp"
#include "semgen/semgen.hpp"

#include "main/globals.hpp"

#include "typedefs.hpp"

//--------------------------------------------------------------------

#ifdef _DEBUG

/// Register pointer in SemGen garbage collector
#   define REG(p) g_semgen.registerPointer(p,__FILE__,__LINE__)
/// Unregister pointer stored in SemGen garbage collector
#   define UNREG(p) g_semgen.unregisterPointer(p,__FILE__,__LINE__)
/// Unregister and free pointer stored in SemGen garbage collector
#   define FREE(p) g_semgen.freePointer(p,__FILE__,__LINE__) 

#else /* _DEBUG */

/// Register pointer in SemGen garbage collector
#   define REG(p) g_semgen.registerPointer(p)
/// Unregister pointer stored in SemGen garbage collector
#   define UNREG(p) g_semgen.unregisterPointer(p)
/// Unregister and free pointer stored in SemGen garbage collector
#   define FREE(p) g_semgen.freePointer(p) 

#endif /* _DEBUG */

//--------------------------------------------------------------------

#endif /* OCT_PARSER_PARSERINCLUDES_HPP */
