/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
#define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     _AND = 258,
     _ARRAY = 259,
     _BEGIN = 260,
     _CASE = 261,
     _CONST = 262,
     _DIV = 263,
     _DO = 264,
     _DOWNTO = 265,
     _ELSE = 266,
     _END = 267,
     _FILE = 268,
     _FOR = 269,
     _FUNCTION = 270,
     _GOTO = 271,
     _IF = 272,
     _IN = 273,
     _LABEL = 274,
     _MOD = 275,
     _NIL = 276,
     _NOT = 277,
     _OF = 278,
     _OR = 279,
     _PACKED = 280,
     _PROCEDURE = 281,
     _PROGRAM = 282,
     _RECORD = 283,
     _REPEAT = 284,
     _SET = 285,
     _THEN = 286,
     _TO = 287,
     _TYPE = 288,
     _UNTIL = 289,
     _VAR = 290,
     _WHILE = 291,
     _WITH = 292,
     OP_LE = 293,
     OP_GE = 294,
     OP_NE = 295,
     OP_ASSIGN = 296,
     _DOUBLE_DOT = 297,
     _UNSIGNED_INTEGER = 298,
     _UNSIGNED_REAL = 299,
     _STRING = 300,
     _SQUARE_ALT_OPEN = 301,
     _SQUARE_ALT_CLOSE = 302,
     _IDENTIFIER = 303,
     _FUNCTION_IDENTIFIER = 304,
     _PROCEDURE_IDENTIFIER = 305,
     _TYPE_IDENTIFIER = 306,
     _CONSTANT_IDENTIFIER = 307
   };
#endif
#define _AND 258
#define _ARRAY 259
#define _BEGIN 260
#define _CASE 261
#define _CONST 262
#define _DIV 263
#define _DO 264
#define _DOWNTO 265
#define _ELSE 266
#define _END 267
#define _FILE 268
#define _FOR 269
#define _FUNCTION 270
#define _GOTO 271
#define _IF 272
#define _IN 273
#define _LABEL 274
#define _MOD 275
#define _NIL 276
#define _NOT 277
#define _OF 278
#define _OR 279
#define _PACKED 280
#define _PROCEDURE 281
#define _PROGRAM 282
#define _RECORD 283
#define _REPEAT 284
#define _SET 285
#define _THEN 286
#define _TO 287
#define _TYPE 288
#define _UNTIL 289
#define _VAR 290
#define _WHILE 291
#define _WITH 292
#define OP_LE 293
#define OP_GE 294
#define OP_NE 295
#define OP_ASSIGN 296
#define _DOUBLE_DOT 297
#define _UNSIGNED_INTEGER 298
#define _UNSIGNED_REAL 299
#define _STRING 300
#define _SQUARE_ALT_OPEN 301
#define _SQUARE_ALT_CLOSE 302
#define _IDENTIFIER 303
#define _FUNCTION_IDENTIFIER 304
#define _PROCEDURE_IDENTIFIER 305
#define _TYPE_IDENTIFIER 306
#define _CONSTANT_IDENTIFIER 307




/* Copy the first part of user declarations.  */
#line 56 "parser.y"


/** \file parser.y
*** \brief
*** YACC (BISON) parser
***/

/** \file parser.yacc.cpp
*** \brief
*** Generated YACC (BISON) parser code file
***/

/** \file parser.yacc.hpp
*** \brief
*** Generated YACC (BISON) parser header file
***/

#include <iostream>
#include <set>
using namespace std;

#include "parserincludes.hpp"

// Forward declaration
extern int yylex(void);

/// \brief
/// Print YACC error message
///
/// \param[in] message - error message to print
void yyerror(char const* message);

/// Verbose YYERROR message
#define YYERROR_VERBOSE

/// Error checking macro - calls YYERROR if condition is false
#define ERRORCHECK(a) if((a) == false) YYERROR

/// Not implemented feature macro
#define NOT_IMPLEMENTED							\
	cerr << "Element not implemented " << __FILE__ << " " << __LINE__ << endl;	\
	YYERROR



/* Enabling traces.  */
#ifndef YYDEBUG
#define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
#undef YYERROR_VERBOSE
#define YYERROR_VERBOSE 1
#else
#define YYERROR_VERBOSE 0
#endif

#if !defined (YYSTYPE) && !defined (YYSTYPE_IS_DECLARED)
#line 105 "parser.y"
typedef union YYSTYPE {
    char* m_charString;
    Operator m_operator;
    double m_real;
    unsigned int m_unsignedInteger;
    int m_signedInteger;
	LabelVector* m_labelVector;
	Semantics::Const* m_const;
	IdentifierVector* m_identifierVector;
	Semantics::Type* m_type;
	Semantics::ForStatement::Direction m_forLoopDir;
	Semantics::Variable* m_variable;
	Semantics::ObjectAccess* m_objectAccess;
	Semantics::Statement* m_statement;
	Semantics::Expression* m_expression;
	Semantics::Object* m_object;
	Semantics::Typed* m_typed;
	StatementVector* m_statementVector;
	Semantics::StatementSequence* m_statementSequence;
	Directive m_directive;
	Semantics::Function* m_function;
	Semantics::Procedure* m_procedure;
	ActualParameterVector* m_actualParameterVector;
	Semantics::ActualParameter* m_actualParameter;
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 250 "parser.yacc.cpp"
#define yystype YYSTYPE /* obsolescent; will be withdrawn */
#define YYSTYPE_IS_DECLARED 1
#define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 262 "parser.yacc.cpp"

#if !defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

#if YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#ifndef YYSTACK_USE_ALLOCA
#if defined (alloca) || defined (_ALLOCA_H)
#define YYSTACK_ALLOC alloca
#else
#ifdef __GNUC__
#define YYSTACK_ALLOC __builtin_alloca
#endif
#endif
#endif
#endif

#ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#else
#if defined (__STDC__) || defined (__cplusplus)
#include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#define YYSIZE_T size_t
#endif
#define YYSTACK_ALLOC malloc
#define YYSTACK_FREE free
#endif
#endif /* !defined (yyoverflow) || YYERROR_VERBOSE */


#if (!defined (yyoverflow) \
     && (!defined (__cplusplus) \
	 || (YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
#define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
#define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
#ifndef YYCOPY
#if 1 < __GNUC__
#define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#else
#define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#endif
#endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
#define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   387

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  70
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  157
/* YYNRULES -- Number of rules. */
#define YYNRULES  262
/* YYNRULES -- Number of states. */
#define YYNSTATES  415

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   307

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      55,    56,    64,    59,    57,    60,    54,    65,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    61,    53,
      63,    58,    62,     2,    67,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    68,     2,    69,    66,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short yyprhs[] =
{
       0,     0,     3,     8,    12,    13,    15,    19,    21,    22,
      30,    31,    35,    37,    41,    43,    44,    47,    50,    54,
      58,    61,    64,    66,    68,    69,    71,    73,    75,    76,
      79,    82,    86,    90,    91,    93,    95,    98,   101,   104,
     108,   112,   116,   120,   123,   125,   126,   129,   133,   137,
     141,   147,   150,   152,   153,   156,   158,   160,   161,   163,
     167,   169,   173,   175,   177,   179,   181,   183,   187,   192,
     194,   196,   198,   200,   204,   209,   211,   213,   221,   228,
     230,   232,   234,   238,   244,   245,   248,   251,   255,   259,
     261,   263,   265,   267,   269,   271,   273,   275,   279,   283,
     286,   287,   289,   291,   293,   295,   297,   304,   306,   310,
     312,   314,   316,   318,   322,   323,   326,   328,   332,   334,
     336,   340,   344,   349,   353,   355,   357,   361,   367,   368,
     370,   372,   374,   378,   380,   384,   386,   389,   391,   393,
     397,   398,   399,   403,   405,   407,   409,   411,   413,   415,
     418,   421,   422,   425,   427,   429,   431,   433,   435,   437,
     439,   441,   442,   446,   448,   451,   455,   457,   461,   463,
     465,   467,   472,   477,   486,   488,   490,   492,   494,   496,
     501,   503,   507,   509,   511,   516,   523,   530,   532,   536,
     538,   542,   544,   548,   550,   553,   554,   556,   560,   562,
     566,   568,   570,   574,   580,   582,   584,   586,   588,   590,
     593,   597,   602,   604,   606,   608,   610,   614,   616,   618,
     620,   624,   627,   631,   633,   637,   639,   641,   643,   645,
     649,   652,   654,   656,   658,   662,   663,   665,   667,   671,
     673,   677,   679,   681,   683,   685,   687,   689,   691,   693,
     695,   697,   699,   701,   703,   705,   707,   709,   711,   713,
     715,   717,   719
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short yyrhs[] =
{
      71,     0,    -1,    72,    53,    75,    54,    -1,    27,   161,
      73,    -1,    -1,    74,    -1,    55,   160,    56,    -1,    76,
      -1,    -1,    77,    78,    81,   125,    88,    91,   168,    -1,
      -1,    19,    79,    53,    -1,    80,    -1,    79,    57,    80,
      -1,    43,    -1,    -1,     7,    82,    -1,    83,    53,    -1,
      82,    83,    53,    -1,   161,    58,    84,    -1,    86,   165,
      -1,    86,    85,    -1,   164,    -1,    52,    -1,    -1,    87,
      -1,    59,    -1,    60,    -1,    -1,    35,    89,    -1,    90,
      53,    -1,    89,    90,    53,    -1,   160,    61,   128,    -1,
      -1,    92,    -1,    93,    -1,    92,    93,    -1,    94,    53,
      -1,   100,    53,    -1,    95,    53,   106,    -1,    95,    53,
      98,    -1,    96,    53,    98,    -1,    26,   161,   108,    -1,
      26,    97,    -1,    50,    -1,    -1,    99,    76,    -1,   101,
      53,   106,    -1,   101,    53,   104,    -1,   102,    53,   104,
      -1,    15,   161,   108,    61,   107,    -1,    15,   103,    -1,
      49,    -1,    -1,   105,    76,    -1,    48,    -1,   129,    -1,
      -1,   109,    -1,    55,   110,    56,    -1,   111,    -1,   110,
      53,   111,    -1,   112,    -1,   113,    -1,   114,    -1,   115,
      -1,   116,    -1,   160,    61,   129,    -1,    35,   160,    61,
     129,    -1,    95,    -1,   101,    -1,   117,    -1,   118,    -1,
     160,    61,   119,    -1,    35,   160,    61,   119,    -1,   120,
      -1,   121,    -1,    25,     4,   225,   124,   226,    23,   129,
      -1,     4,   225,   123,   226,    23,   122,    -1,   129,    -1,
     119,    -1,   124,    -1,   123,    53,   124,    -1,   161,    42,
     161,    61,   141,    -1,    -1,    33,   126,    -1,   127,    53,
      -1,   126,   127,    53,    -1,   161,    58,   128,    -1,   129,
      -1,   130,    -1,    51,    -1,   131,    -1,   134,    -1,   158,
      -1,   132,    -1,   133,    -1,    55,   160,    56,    -1,    84,
      42,    84,    -1,   135,   136,    -1,    -1,    25,    -1,   137,
      -1,   142,    -1,   154,    -1,   156,    -1,     4,   225,   138,
     226,    23,   157,    -1,   139,    -1,   138,    57,   139,    -1,
     140,    -1,   131,    -1,   141,    -1,   129,    -1,    28,   143,
      12,    -1,    -1,   144,   151,    -1,   145,    -1,   145,    53,
     147,    -1,   147,    -1,   146,    -1,   145,    53,   146,    -1,
     160,    61,   128,    -1,     6,   148,    23,   149,    -1,   153,
      61,   152,    -1,   152,    -1,   150,    -1,   149,    53,   150,
      -1,   195,    61,    55,   143,    56,    -1,    -1,    53,    -1,
     141,    -1,   161,    -1,    30,    23,   155,    -1,   140,    -1,
      13,    23,   157,    -1,   128,    -1,   224,   159,    -1,   129,
      -1,   161,    -1,   160,    57,   161,    -1,    -1,    -1,   162,
      48,   163,    -1,    45,    -1,   166,    -1,   167,    -1,    43,
      -1,    44,    -1,   177,    -1,   170,   171,    -1,   170,   172,
      -1,    -1,    80,    61,    -1,   173,    -1,   197,    -1,   174,
      -1,   176,    -1,   177,    -1,   189,    -1,   179,    -1,   187,
      -1,    -1,   175,    41,   210,    -1,   203,    -1,    16,    80,
      -1,     5,   178,    12,    -1,   169,    -1,   178,    53,   169,
      -1,   180,    -1,   181,    -1,   182,    -1,    29,   178,    34,
     208,    -1,    36,   208,     9,   169,    -1,    14,   183,    41,
     185,   184,   186,     9,   169,    -1,   202,    -1,    32,    -1,
      10,    -1,   210,    -1,   210,    -1,    37,   188,     9,   169,
      -1,   204,    -1,   188,    57,   204,    -1,   190,    -1,   191,
      -1,    17,   208,    31,   169,    -1,    17,   208,    31,   169,
      11,   169,    -1,     6,   193,    23,   192,   151,    12,    -1,
     194,    -1,   192,    53,   194,    -1,   210,    -1,   195,    61,
     169,    -1,   196,    -1,   195,    57,   196,    -1,    84,    -1,
      97,   198,    -1,    -1,   199,    -1,    55,   200,    56,    -1,
     201,    -1,   200,    57,   201,    -1,   202,    -1,   210,    -1,
     210,    61,   210,    -1,   210,    61,   210,    61,   210,    -1,
      97,    -1,   103,    -1,   203,    -1,    52,    -1,   161,    -1,
     202,   224,    -1,   202,    54,   205,    -1,   202,   225,   207,
     226,    -1,   202,    -1,   206,    -1,   161,    -1,   209,    -1,
     207,    57,   209,    -1,   210,    -1,   210,    -1,   211,    -1,
     211,   220,   211,    -1,    86,   212,    -1,   211,   221,   212,
      -1,   214,    -1,   212,   222,   214,    -1,   203,    -1,   213,
      -1,   215,    -1,   216,    -1,    55,   210,    56,    -1,   223,
     214,    -1,   165,    -1,   164,    -1,    21,    -1,   225,   217,
     226,    -1,    -1,   218,    -1,   219,    -1,   218,    57,   219,
      -1,   210,    -1,   210,    42,   210,    -1,    58,    -1,    62,
      -1,    63,    -1,    40,    -1,    38,    -1,    39,    -1,    18,
      -1,    59,    -1,    60,    -1,    24,    -1,    64,    -1,    65,
      -1,     8,    -1,    20,    -1,     3,    -1,    22,    -1,    66,
      -1,    67,    -1,    68,    -1,    46,    -1,    69,    -1,    47,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short yyrline[] =
{
       0,   189,   189,   192,   199,   202,   208,   214,   220,   220,
     233,   234,   240,   245,   252,   257,   258,   261,   262,   265,
     271,   276,   281,   288,   294,   295,   298,   300,   306,   307,
     310,   311,   314,   322,   323,   326,   327,   330,   331,   335,
     336,   337,   340,   343,   349,   352,   352,   358,   359,   360,
     363,   366,   372,   375,   375,   381,   401,   411,   414,   420,
     423,   424,   428,   429,   430,   431,   433,   436,   439,   442,
     445,   448,   449,   452,   455,   458,   459,   462,   465,   468,
     469,   472,   473,   476,   480,   481,   487,   488,   491,   494,
     499,   505,   508,   509,   510,   513,   514,   517,   520,   523,
     526,   527,   530,   531,   532,   533,   536,   539,   540,   543,
     546,   547,   550,   553,   556,   557,   560,   561,   562,   565,
     566,   569,   572,   575,   576,   579,   580,   583,   586,   587,
     590,   593,   596,   599,   602,   605,   608,   611,   617,   622,
     629,   631,   629,   640,   645,   650,   657,   660,   665,   671,
     676,   684,   687,   693,   697,   701,   705,   711,   716,   721,
     726,   733,   746,   753,   760,   763,   770,   775,   782,   787,
     792,   799,   806,   813,   827,   834,   838,   844,   851,   858,
     861,   862,   865,   870,   877,   882,   889,   895,   896,   899,
     902,   905,   906,   909,   930,   938,   941,   948,   954,   959,
     976,   981,   986,   990,   994,   999,  1026,  1033,  1038,  1046,
    1050,  1054,  1060,  1067,  1070,  1073,  1074,  1077,  1084,  1090,
    1095,  1106,  1111,  1121,  1125,  1133,  1153,  1158,  1162,  1166,
    1172,  1192,  1193,  1198,  1208,  1211,  1212,  1215,  1216,  1219,
    1220,  1225,  1226,  1227,  1228,  1229,  1230,  1231,  1234,  1235,
    1236,  1239,  1240,  1241,  1242,  1243,  1246,  1251,  1252,  1255,
    1256,  1259,  1260
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "_AND", "_ARRAY", "_BEGIN", "_CASE", 
  "_CONST", "_DIV", "_DO", "_DOWNTO", "_ELSE", "_END", "_FILE", "_FOR", 
  "_FUNCTION", "_GOTO", "_IF", "_IN", "_LABEL", "_MOD", "_NIL", "_NOT", 
  "_OF", "_OR", "_PACKED", "_PROCEDURE", "_PROGRAM", "_RECORD", "_REPEAT", 
  "_SET", "_THEN", "_TO", "_TYPE", "_UNTIL", "_VAR", "_WHILE", "_WITH", 
  "OP_LE", "OP_GE", "OP_NE", "OP_ASSIGN", "_DOUBLE_DOT", 
  "_UNSIGNED_INTEGER", "_UNSIGNED_REAL", "_STRING", "_SQUARE_ALT_OPEN", 
  "_SQUARE_ALT_CLOSE", "_IDENTIFIER", "_FUNCTION_IDENTIFIER", 
  "_PROCEDURE_IDENTIFIER", "_TYPE_IDENTIFIER", "_CONSTANT_IDENTIFIER", 
  "';'", "'.'", "'('", "')'", "','", "'='", "'+'", "'-'", "':'", "'>'", 
  "'<'", "'*'", "'/'", "'^'", "'@'", "'['", "']'", "$accept", "program", 
  "program_heading", "program_parameter_list_opt", 
  "program_parameter_list", "program_block", "block", "@1", 
  "label_declaration_part", "label_list", "label", 
  "constant_definition_part", "constant_definition_list", 
  "constant_definition", "constant", "constant_identifier", "opt_sign", 
  "sign", "variable_declaration_part", "variable_declaration_list", 
  "variable_declaration", "procedure_and_function_declaration_part", 
  "procedure_and_function_declaration_list", 
  "procedure_or_function_declaration", "procedure_declaration", 
  "procedure_heading", "procedure_identification", "procedure_identifier", 
  "procedure_block", "@2", "function_declaration", "function_heading", 
  "function_identification", "function_identifier", "function_block", 
  "@3", "directive", "result_type", "opt_formal_parameter_list", 
  "formal_parameter_list", "formal_parameter_list_list", 
  "formal_parameter_section_list", "value_parameter_specification", 
  "variable_parameter_specification", 
  "procedural_parameter_specification", 
  "functional_parameter_specification", 
  "conformant_array_parameter_specification", 
  "value_conformant_parameter_specification", 
  "variable_conformant_parameter_specification", 
  "conformant_array_schema", "packed_conformant_array_schema", 
  "unpacked_conformant_array_schema", 
  "unpacked_conformant_array_schema_type", 
  "index_type_specification_list", "index_type_specification", 
  "type_definition_part", "type_definition_list", "type_definition", 
  "type_denoter", "type_identifier", "new_type", "new_ordinal_type", 
  "enumerated_type", "subrange_type", "new_structured_type", "opt_packed", 
  "unpacked_structured_type", "array_type", "index_type_list", 
  "index_type", "ordinal_type", "ordinal_type_identifier", "record_type", 
  "field_list", "field_list_data", "fixed_part", "record_section", 
  "variant_part", "variant_selector", "variant_list", "variant", 
  "opt_semicolon", "tag_type", "tag_field", "set_type", "base_type", 
  "file_type", "component_type", "new_pointer_type", "domain_type", 
  "identifier_list", "identifier", "@4", "@5", "character_string", 
  "unsigned_number", "unsigned_integer", "unsigned_real", 
  "statement_part", "statement", "opt_statement_label", 
  "simple_statement", "structured_statement", "empty_statement", 
  "assignment_statement", "variable_or_function_access", "goto_statement", 
  "compound_statement", "statement_sequence", "repetitive_statement", 
  "repeat_statement", "while_statement", "for_statement", 
  "control_variable", "for_statement_direction", "initial_value", 
  "final_value", "with_statement", "record_variable_list", 
  "conditional_statement", "if_statement", "case_statement", 
  "case_list_element_list", "case_index", "case_list_element", 
  "case_constant_list", "case_constant", "procedure_statement", 
  "opt_actual_parameter_list", "actual_parameter_list", 
  "actual_parameter_list_list", "actual_parameter", "variable_access", 
  "object_access", "record_variable", "field_specifier", 
  "field_identifier", "index_expression_list", "boolean_expression", 
  "index_expression", "expression", "simple_expression", "term", 
  "factor_object_use", "factor", "unsigned_constant", "set_constructor", 
  "opt_member_designator_list", "member_designator_list", 
  "member_designator", "relational_operator", "adding_operator", 
  "multyplying_operator", "not_operator", "arrow_char", "square_open", 
  "square_close", 0
};
#endif

#ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,    59,    46,    40,    41,    44,    61,    43,
      45,    58,    62,    60,    42,    47,    94,    64,    91,    93
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    70,    71,    72,    73,    73,    74,    75,    77,    76,
      78,    78,    79,    79,    80,    81,    81,    82,    82,    83,
      84,    84,    84,    85,    86,    86,    87,    87,    88,    88,
      89,    89,    90,    91,    91,    92,    92,    93,    93,    94,
      94,    94,    95,    96,    97,    99,    98,   100,   100,   100,
     101,   102,   103,   105,   104,   106,   107,   108,   108,   109,
     110,   110,   111,   111,   111,   111,   111,   112,   113,   114,
     115,   116,   116,   117,   118,   119,   119,   120,   121,   122,
     122,   123,   123,   124,   125,   125,   126,   126,   127,   128,
     128,   129,   130,   130,   130,   131,   131,   132,   133,   134,
     135,   135,   136,   136,   136,   136,   137,   138,   138,   139,
     140,   140,   141,   142,   143,   143,   144,   144,   144,   145,
     145,   146,   147,   148,   148,   149,   149,   150,   151,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   160,
     162,   163,   161,   164,   165,   165,   166,   167,   168,   169,
     169,   170,   170,   171,   171,   171,   171,   172,   172,   172,
     172,   173,   174,   175,   176,   177,   178,   178,   179,   179,
     179,   180,   181,   182,   183,   184,   184,   185,   186,   187,
     188,   188,   189,   189,   190,   190,   191,   192,   192,   193,
     194,   195,   195,   196,   197,   198,   198,   199,   200,   200,
     201,   201,   201,   201,   201,   201,   202,   203,   203,   203,
     203,   203,   204,   205,   206,   207,   207,   208,   209,   210,
     210,   211,   211,   212,   212,   213,   214,   214,   214,   214,
     214,   215,   215,   215,   216,   217,   217,   218,   218,   219,
     219,   220,   220,   220,   220,   220,   220,   220,   221,   221,
     221,   222,   222,   222,   222,   222,   223,   224,   224,   225,
     225,   226,   226
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     4,     3,     0,     1,     3,     1,     0,     7,
       0,     3,     1,     3,     1,     0,     2,     2,     3,     3,
       2,     2,     1,     1,     0,     1,     1,     1,     0,     2,
       2,     3,     3,     0,     1,     1,     2,     2,     2,     3,
       3,     3,     3,     2,     1,     0,     2,     3,     3,     3,
       5,     2,     1,     0,     2,     1,     1,     0,     1,     3,
       1,     3,     1,     1,     1,     1,     1,     3,     4,     1,
       1,     1,     1,     3,     4,     1,     1,     7,     6,     1,
       1,     1,     3,     5,     0,     2,     2,     3,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     3,     2,
       0,     1,     1,     1,     1,     1,     6,     1,     3,     1,
       1,     1,     1,     3,     0,     2,     1,     3,     1,     1,
       3,     3,     4,     3,     1,     1,     3,     5,     0,     1,
       1,     1,     3,     1,     3,     1,     2,     1,     1,     3,
       0,     0,     3,     1,     1,     1,     1,     1,     1,     2,
       2,     0,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     3,     1,     2,     3,     1,     3,     1,     1,
       1,     4,     4,     8,     1,     1,     1,     1,     1,     4,
       1,     3,     1,     1,     4,     6,     6,     1,     3,     1,
       3,     1,     3,     1,     2,     0,     1,     3,     1,     3,
       1,     1,     3,     5,     1,     1,     1,     1,     1,     2,
       3,     4,     1,     1,     1,     1,     3,     1,     1,     1,
       3,     2,     3,     1,     3,     1,     1,     1,     1,     3,
       2,     1,     1,     1,     3,     0,     1,     1,     3,     1,
       3,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short yydefact[] =
{
       0,   140,     0,     0,     4,     0,     1,     8,   140,     3,
       5,   141,     0,     7,    10,     0,   138,   142,     2,     0,
      15,     6,   140,    14,     0,    12,   140,    84,   139,    11,
       0,    16,     0,     0,   140,    28,    13,     0,    17,    24,
      85,     0,     0,   140,    33,    18,   143,    26,    27,    19,
       0,    25,    22,     0,    86,   100,    29,     0,     0,   140,
     140,     0,    34,    35,     0,     0,     0,     0,     0,     0,
     146,   147,    23,    21,    20,   144,   145,    87,   101,    91,
     140,   257,   258,     0,    88,    89,    90,    92,    95,    96,
      93,     0,    94,     0,     0,    30,   100,    52,    51,    57,
      44,    43,    57,   151,     9,   148,    36,    37,    45,    45,
      38,    53,    53,     0,    24,     0,     0,   114,     0,    99,
     102,   103,   104,   105,   137,   136,    31,    32,   140,     0,
      58,    42,     0,   166,   161,     0,    55,    40,     8,    39,
      41,    48,     8,    47,    49,    97,    98,   260,   259,    24,
     100,   140,     0,   128,   116,   119,   118,     0,    24,   140,
     140,   140,    69,    70,     0,    60,    62,    63,    64,    65,
      66,    71,    72,     0,     0,   152,    24,   140,     0,    24,
     151,    24,   140,   207,   195,   208,   149,   150,   153,   155,
       0,   156,   157,   159,   168,   169,   170,   160,   158,   182,
     183,   154,     0,   206,   165,   151,    46,    54,   112,   110,
       0,   107,   109,   111,   135,   134,   130,     0,   124,     0,
     131,   113,   129,   115,   140,   100,   133,   132,     0,   140,
      59,     0,    50,    56,   140,     0,   189,   219,     0,   174,
     206,   164,     0,   217,     0,     0,     0,   212,   180,    24,
     194,   196,    24,   140,   209,    24,   167,   262,    24,   261,
       0,    24,     0,   120,   117,   121,     0,    61,     0,     0,
      73,    75,    76,    67,   233,   256,    24,   232,   231,   225,
     221,   226,   223,   227,   228,   140,    24,    24,   247,   250,
     245,   246,   244,   241,   248,   249,   242,   243,    24,   140,
      24,   151,    24,   151,   151,   140,   204,   205,     0,   198,
     200,   201,   162,   214,   210,   213,     0,   215,   218,   108,
     100,   193,   122,   125,     0,   191,   123,    74,    68,   140,
       0,     0,   255,   253,   254,   251,   252,   140,   230,   239,
       0,   236,   237,   128,   187,     0,   220,   222,     0,   177,
     184,   171,   172,   179,   181,   197,    24,    24,    24,   211,
     106,    24,    24,     0,     0,    81,     0,   140,   229,   224,
      24,   234,    24,    24,     0,   151,   176,   175,    24,   151,
     199,   202,   216,   126,   192,   114,   140,     0,   140,     0,
     240,   238,   188,   186,   190,     0,   178,   185,    24,     0,
      82,     0,     0,     0,   151,   203,   127,    80,    78,    79,
       0,     0,   173,    83,    77
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short yydefgoto[] =
{
      -1,     2,     3,     9,    10,    12,    13,    14,    20,    24,
     132,    27,    31,    32,    83,    73,   234,    51,    44,    56,
      57,    61,    62,    63,    64,    65,    66,   306,   137,   138,
      67,    68,    69,   307,   141,   142,   139,   232,   129,   130,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   270,
     271,   272,   408,   364,   365,    35,    40,    41,   214,   208,
      86,    87,    88,    89,    90,    91,   119,   120,   210,   211,
     212,   213,   121,   152,   153,   154,   155,   156,   217,   322,
     323,   223,   218,   219,   122,   227,   123,   215,    92,   125,
     157,    16,     5,    17,    52,   278,    75,    76,   104,   133,
     134,   186,   187,   188,   189,   190,   191,   105,   135,   193,
     194,   195,   196,   238,   378,   348,   395,   197,   246,   198,
     199,   200,   343,   235,   344,   324,   325,   201,   250,   251,
     308,   309,   202,   240,   248,   314,   315,   316,   242,   317,
     243,   237,   280,   281,   282,   283,   284,   340,   341,   342,
     298,   299,   337,   285,    93,   255,   260
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -286
static const short yypact[] =
{
       1,  -286,    51,    63,    87,    99,  -286,  -286,  -286,  -286,
    -286,  -286,    95,  -286,   133,   -21,  -286,  -286,  -286,   130,
     163,  -286,  -286,  -286,     9,  -286,  -286,   142,  -286,  -286,
     130,   141,   150,   187,  -286,   212,  -286,   165,  -286,    -7,
     141,   203,   205,  -286,    41,  -286,  -286,  -286,  -286,  -286,
     102,  -286,  -286,   211,  -286,   250,   141,   215,   127,   216,
     219,   265,    41,  -286,   218,   223,   224,   228,   233,   238,
    -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,
    -286,  -286,  -286,   230,  -286,  -286,  -286,  -286,  -286,  -286,
    -286,   106,  -286,   239,   243,  -286,   250,  -286,  -286,   242,
    -286,  -286,   242,   130,  -286,  -286,  -286,  -286,   244,  -286,
    -286,   244,  -286,    45,    -7,    39,   276,    42,   277,  -286,
    -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,    77,   245,
    -286,  -286,   246,  -286,   237,    20,  -286,  -286,  -286,  -286,
    -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,   160,
     250,   239,   291,   255,   258,  -286,  -286,   149,   160,  -286,
    -286,  -286,  -286,  -286,    55,  -286,  -286,  -286,  -286,  -286,
    -286,  -286,  -286,   151,   239,  -286,   157,   260,   130,   157,
     130,   157,   260,  -286,   259,  -286,  -286,  -286,  -286,  -286,
     272,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,
    -286,  -286,    94,   274,  -286,   130,  -286,  -286,  -286,  -286,
      84,  -286,  -286,  -286,  -286,  -286,  -286,   295,  -286,   261,
    -286,  -286,  -286,  -286,   313,   250,  -286,  -286,   173,    77,
    -286,    40,  -286,  -286,    25,   297,  -286,   220,   280,    94,
    -286,  -286,   294,  -286,    75,   317,    15,    94,  -286,   172,
    -286,  -286,   157,  -286,  -286,   157,  -286,  -286,   160,  -286,
     304,    -7,   239,  -286,  -286,  -286,    40,  -286,    39,   327,
    -286,  -286,  -286,  -286,  -286,  -286,   157,  -286,  -286,    97,
      34,  -286,  -286,  -286,  -286,    25,   135,    -7,  -286,  -286,
    -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,   157,    25,
     157,   130,   157,   130,   130,   260,  -286,  -286,   183,  -286,
      94,   271,  -286,  -286,  -286,  -286,   122,  -286,  -286,  -286,
     250,  -286,   281,  -286,   180,  -286,  -286,  -286,  -286,  -286,
      39,   282,  -286,  -286,  -286,  -286,  -286,    25,  -286,   298,
     108,   285,  -286,   284,  -286,   189,    36,    34,   170,  -286,
     322,  -286,  -286,  -286,  -286,  -286,   172,   157,   157,  -286,
    -286,    -7,    -7,   288,   145,  -286,   302,  -286,  -286,  -286,
     157,  -286,   157,    29,   333,   130,  -286,  -286,   157,   130,
    -286,   278,  -286,  -286,  -286,    42,  -286,   323,  -286,   108,
    -286,  -286,  -286,  -286,  -286,   338,  -286,  -286,   157,   293,
    -286,    40,   289,   328,   130,  -286,  -286,  -286,  -286,  -286,
     239,   239,  -286,  -286,  -286
};

/* YYPGOTO[NTERM-NUM].  */
static const short yypgoto[] =
{
    -286,  -286,  -286,  -286,  -286,  -286,   119,  -286,  -286,  -286,
     -11,  -286,  -286,   321,   -38,  -286,   -32,  -286,  -286,  -286,
     300,  -286,  -286,   292,  -286,  -106,  -286,   -40,   248,  -286,
    -286,   -99,  -286,   301,   241,  -286,   251,  -286,   257,  -286,
    -286,   132,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -257,
    -286,  -286,  -286,  -286,  -230,  -286,  -286,   324,   -51,   -53,
    -286,  -131,  -286,  -286,  -286,  -286,  -286,  -286,  -286,   105,
     207,  -148,  -286,   -18,  -286,  -286,   144,   146,  -286,  -286,
       8,    28,   110,  -286,  -286,  -286,  -286,    53,  -286,  -286,
       7,    -1,  -286,  -286,  -224,   325,  -286,  -286,  -286,  -179,
    -286,  -286,  -286,  -286,  -286,  -286,  -286,   240,   196,  -286,
    -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,  -286,
    -286,  -286,  -286,  -286,     4,  -273,    16,  -286,  -286,  -286,
    -286,    23,  -166,  -128,    76,  -286,  -286,  -286,  -164,    22,
    -171,    85,    83,  -286,  -251,  -286,  -286,  -286,  -286,    12,
    -286,  -286,  -286,  -286,  -190,  -102,  -285
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -236
static const short yytable[] =
{
       4,    49,    85,   216,    84,   236,   203,    50,    25,   327,
     277,   239,   254,   149,   345,    15,   247,   245,   209,    36,
     101,    28,   162,    50,   304,    33,   256,   209,     1,   163,
      33,   359,   204,    42,   338,    21,    22,   332,    46,    42,
     124,  -129,   333,    85,   268,   127,   274,   275,   151,   254,
      58,     6,    47,    48,   334,   371,    59,   254,    99,   102,
     289,   277,    29,    58,    50,   269,    30,    60,    70,    71,
      46,   147,   305,   205,    46,   277,   146,   183,   311,   387,
     276,   312,    50,   310,   318,   147,   369,   113,    47,    48,
    -140,    79,   159,   148,   184,   294,   295,    85,   335,   336,
     345,   145,    22,   160,   403,   331,   279,   148,   229,   302,
     115,   230,   161,   277,   216,   339,     7,    50,    50,   116,
     254,   233,   350,   162,   352,   353,    50,   209,   205,   349,
     163,   257,   286,   185,   117,   173,   118,   389,   351,   247,
     147,   258,     8,  -206,   407,    70,    71,    11,   253,    18,
     220,  -206,    19,   259,    72,   257,   400,   279,    99,   102,
      81,    82,   148,  -206,  -206,  -206,   329,   241,   228,   257,
      26,   279,    85,    23,   265,    34,   185,   259,   273,   358,
     376,   185,  -235,   286,    22,   311,   381,   318,    96,  -140,
     310,   259,   257,    50,    47,    48,   394,   286,   386,   390,
     397,   339,   377,    38,  -235,    46,    22,   396,    22,   279,
     225,    79,   231,   328,   259,    80,    47,    48,    45,    47,
      48,    97,   100,   321,   183,   412,    50,   405,   367,    50,
      22,    47,    48,   185,   266,   286,   173,   362,   288,   355,
     356,   363,   103,   176,   289,    39,   362,    43,   185,   321,
     375,   177,   313,   178,   179,    50,    54,   206,   290,   291,
     292,   207,   413,    55,    77,    97,   180,    85,    95,   100,
     103,   107,   114,   181,   182,    78,   108,   109,   293,   294,
     295,   110,   296,   297,   185,  -140,   111,   100,    50,   183,
      79,   112,   136,   -24,   -24,    46,   126,   128,   185,   150,
     158,    79,   -24,   221,   185,    80,   174,   175,   222,    47,
      48,   224,   183,   252,   249,  -163,    81,    82,   261,   151,
     287,   300,   262,   321,   321,   301,   303,   320,   366,    50,
      50,   330,   357,   379,   361,   321,   185,   373,   368,   398,
     370,    50,   372,   385,   388,   393,   401,   404,   409,   406,
     410,   411,    37,   144,   106,   185,    94,   140,   414,   131,
      98,   267,   143,   319,    53,   226,   366,   399,   263,   383,
     264,   374,   326,   360,   192,    74,   244,   392,   384,   380,
     382,   354,   347,   346,   391,   366,     0,   402
};

static const short yycheck[] =
{
       1,    39,    55,   151,    55,   176,   134,    39,    19,   266,
     234,   177,   202,   115,   287,     8,   182,   181,   149,    30,
      60,    22,   128,    55,     9,    26,   205,   158,    27,   128,
      31,   316,    12,    34,   285,    56,    57,     3,    45,    40,
      93,    12,     8,    96,     4,    96,    21,    22,     6,   239,
      43,     0,    59,    60,    20,   340,    15,   247,    59,    60,
      24,   285,    53,    56,    96,    25,    57,    26,    43,    44,
      45,    46,    57,    53,    45,   299,   114,    52,   249,   364,
      55,   252,   114,   249,   255,    46,   337,    80,    59,    60,
      48,    51,    15,    68,   134,    59,    60,   150,    64,    65,
     373,    56,    57,    26,   389,   276,   234,    68,    53,    34,
       4,    56,    35,   337,   262,   286,    53,   149,   150,    13,
     310,   174,   301,   229,   303,   304,   158,   258,    53,   300,
     229,    47,   234,   134,    28,   128,    30,   367,   302,   305,
      46,    57,    55,    46,   401,    43,    44,    48,    54,    54,
     151,    54,    19,    69,    52,    47,   386,   285,   159,   160,
      66,    67,    68,    66,    67,    68,   268,   178,   161,    47,
       7,   299,   225,    43,   225,    33,   177,    69,   231,    57,
      10,   182,    47,   285,    57,   356,   357,   358,    61,    48,
     356,    69,    47,   225,    59,    60,   375,   299,    53,   370,
     379,   372,    32,    53,    69,    45,    57,   378,    57,   337,
      61,    51,    61,   266,    69,    55,    59,    60,    53,    59,
      60,    49,    50,   261,    52,   404,   258,   398,   330,   261,
      57,    59,    60,   234,    61,   337,   229,    57,    18,    56,
      57,    61,     5,     6,    24,    58,    57,    35,   249,   287,
      61,    14,   253,    16,    17,   287,    53,   138,    38,    39,
      40,   142,   410,    58,    53,    49,    29,   320,    53,    50,
       5,    53,    42,    36,    37,    25,    53,    53,    58,    59,
      60,    53,    62,    63,   285,    48,    53,    50,   320,    52,
      51,    53,    48,    43,    44,    45,    53,    55,   299,    23,
      23,    51,    52,    12,   305,    55,    61,    61,    53,    59,
      60,    53,    52,    41,    55,    41,    66,    67,    23,     6,
      23,    41,    61,   361,   362,    31,     9,    23,   329,   361,
     362,     4,    61,    11,    53,   373,   337,    53,    56,    61,
      42,   373,    57,    55,    42,    12,    23,     9,   401,    56,
      61,    23,    31,   112,    62,   356,    56,   109,   411,   102,
      59,   229,   111,   258,    40,   158,   367,   385,   224,   361,
     224,   343,   262,   320,   134,    50,   180,   373,   362,   356,
     358,   305,   299,   298,   372,   386,    -1,   388
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,    27,    71,    72,   161,   162,     0,    53,    55,    73,
      74,    48,    75,    76,    77,   160,   161,   163,    54,    19,
      78,    56,    57,    43,    79,    80,     7,    81,   161,    53,
      57,    82,    83,   161,    33,   125,    80,    83,    53,    58,
     126,   127,   161,    35,    88,    53,    45,    59,    60,    84,
      86,    87,   164,   127,    53,    58,    89,    90,   160,    15,
      26,    91,    92,    93,    94,    95,    96,   100,   101,   102,
      43,    44,    52,    85,   165,   166,   167,    53,    25,    51,
      55,    66,    67,    84,   128,   129,   130,   131,   132,   133,
     134,   135,   158,   224,    90,    53,    61,    49,   103,   161,
      50,    97,   161,     5,   168,   177,    93,    53,    53,    53,
      53,    53,    53,   160,    42,     4,    13,    28,    30,   136,
     137,   142,   154,   156,   129,   159,    53,   128,    55,   108,
     109,   108,    80,   169,   170,   178,    48,    98,    99,   106,
      98,   104,   105,   106,   104,    56,    84,    46,    68,   225,
      23,     6,   143,   144,   145,   146,   147,   160,    23,    15,
      26,    35,    95,   101,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   160,    61,    61,     6,    14,    16,    17,
      29,    36,    37,    52,    97,   161,   171,   172,   173,   174,
     175,   176,   177,   179,   180,   181,   182,   187,   189,   190,
     191,   197,   202,   203,    12,    53,    76,    76,   129,   131,
     138,   139,   140,   141,   128,   157,   141,   148,   152,   153,
     161,    12,    53,   151,    53,    61,   140,   155,   160,    53,
      56,    61,   107,   129,    86,   193,   210,   211,   183,   202,
     203,    80,   208,   210,   178,   208,   188,   202,   204,    55,
     198,   199,    41,    54,   224,   225,   169,    47,    57,    69,
     226,    23,    61,   146,   147,   128,    61,   111,     4,    25,
     119,   120,   121,   129,    21,    22,    55,   164,   165,   203,
     212,   213,   214,   215,   216,   223,   225,    23,    18,    24,
      38,    39,    40,    58,    59,    60,    62,    63,   220,   221,
      41,    31,    34,     9,     9,    57,    97,   103,   200,   201,
     202,   210,   210,   161,   205,   206,   207,   209,   210,   139,
      23,    84,   149,   150,   195,   196,   152,   119,   129,   225,
       4,   210,     3,     8,    20,    64,    65,   222,   214,   210,
     217,   218,   219,   192,   194,   195,   211,   212,   185,   210,
     169,   208,   169,   169,   204,    56,    57,    61,    57,   226,
     157,    53,    57,    61,   123,   124,   161,   225,    56,   214,
      42,   226,    57,    53,   151,    61,    10,    32,   184,    11,
     201,   210,   209,   150,   196,    55,    53,   226,    42,   124,
     210,   219,   194,    12,   169,   186,   210,   169,    61,   143,
     124,    23,   161,   226,     9,   210,    56,   119,   122,   129,
      61,    23,   169,   141,   129
};

#if !defined (YYSIZE_T) && defined (__SIZE_TYPE__)
#define YYSIZE_T __SIZE_TYPE__
#endif
#if !defined (YYSIZE_T) && defined (size_t)
#define YYSIZE_T size_t
#endif
#if !defined (YYSIZE_T)
#if defined (__STDC__) || defined (__cplusplus)
#include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#define YYSIZE_T size_t
#endif
#endif
#if !defined (YYSIZE_T)
#define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrlab1

/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
#define YYLLOC_DEFAULT(Current, Rhs, N)         \
  Current.first_line   = Rhs[1].first_line;      \
  Current.first_column = Rhs[1].first_column;    \
  Current.last_line    = Rhs[N].last_line;       \
  Current.last_column  = Rhs[N].last_column;
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
#define YYLEX yylex (YYLEX_PARAM)
#else
#define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

#ifndef YYFPRINTF
#include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#define YYFPRINTF fprintf
#endif

#define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

#define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

#define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (cinluded).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short *bottom, short *top)
#else
static void
yy_stack_print (bottom, top)
    short *bottom;
    short *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

#define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylineno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylineno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

#define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
#define YYDPRINTF(Args)
#define YYDSYMPRINT(Args)
#define YYDSYMPRINTF(Title, Token, Value, Location)
#define YY_STACK_PRINT(Bottom, Top)
#define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

#ifndef yystrlen
#if defined (__GLIBC__) && defined (_STRING_H)
#define yystrlen strlen
#else
/* Return the length of YYSTR.  */
static YYSIZE_T
#if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#else
yystrlen (yystr)
     const char *yystr;
#endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#endif
#endif

#ifndef yystpcpy
#if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#define yystpcpy stpcpy
#else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#endif
#endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
#ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
#endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
#else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
#ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
#else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
#endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 193 "parser.y"
    {
                        ERRORCHECK(g_semgen.recProgramHeading(yyvsp[-1].m_charString, yyvsp[0].m_identifierVector));
                    ;}
    break;

  case 4:
#line 199 "parser.y"
    {
									yyval.m_identifierVector = NULL;
								;}
    break;

  case 5:
#line 203 "parser.y"
    {
									yyval.m_identifierVector = yyvsp[0].m_identifierVector;
								;}
    break;

  case 6:
#line 209 "parser.y"
    {
								yyval.m_identifierVector = yyvsp[-1].m_identifierVector;
							;}
    break;

  case 7:
#line 215 "parser.y"
    {
						ERRORCHECK(g_semgen.recProgramBlockEnd());
					;}
    break;

  case 8:
#line 220 "parser.y"
    {	
				// ERRORCHECK(g_semgen.checkDeclarationStatus());	
			;}
    break;

  case 11:
#line 235 "parser.y"
    {
								ERRORCHECK(g_semgen.recLabelDeclarationPart(yyvsp[-1].m_labelVector));
							;}
    break;

  case 12:
#line 241 "parser.y"
    {
					yyval.m_labelVector = REG(new LabelVector());
					yyval.m_labelVector->push_back(yyvsp[0].m_unsignedInteger);	
				;}
    break;

  case 13:
#line 246 "parser.y"
    {
					yyval.m_labelVector = yyvsp[-2].m_labelVector;
					yyval.m_labelVector->push_back(yyvsp[0].m_unsignedInteger);
				;}
    break;

  case 19:
#line 266 "parser.y"
    {
							ERRORCHECK(g_semgen.recConstantDefinition(yyvsp[-2].m_charString, yyvsp[0].m_const));
						;}
    break;

  case 20:
#line 272 "parser.y"
    {
					yyval.m_const = g_semgen.recConstant(yyvsp[-1].m_operator, yyvsp[0].m_const);
					ERRORCHECK(yyval.m_const != NULL);
				;}
    break;

  case 21:
#line 277 "parser.y"
    {
					yyval.m_const = g_semgen.recConstant(yyvsp[-1].m_operator, yyvsp[0].m_charString);
					ERRORCHECK(yyval.m_const != NULL);
				;}
    break;

  case 22:
#line 282 "parser.y"
    {
					yyval.m_const = g_semgen.recConstant(yyvsp[0].m_charString);
					ERRORCHECK(yyval.m_const != NULL);
				;}
    break;

  case 24:
#line 294 "parser.y"
    {   yyval.m_operator = O_NOOP;    ;}
    break;

  case 26:
#line 299 "parser.y"
    {   yyval.m_operator = O_PLUS;    ;}
    break;

  case 27:
#line 301 "parser.y"
    {   yyval.m_operator = O_MINUS;   ;}
    break;

  case 32:
#line 315 "parser.y"
    {
								ERRORCHECK(g_semgen.recVariableDeclaration(yyvsp[-2].m_identifierVector, yyvsp[0].m_type));
							;}
    break;

  case 43:
#line 344 "parser.y"
    {
									NOT_IMPLEMENTED;
								;}
    break;

  case 45:
#line 352 "parser.y"
    {	ERRORCHECK(g_semgen.recProcedureBlockBegin());	;}
    break;

  case 46:
#line 354 "parser.y"
    {	ERRORCHECK(g_semgen.recProcedureBlockEnd());	;}
    break;

  case 51:
#line 367 "parser.y"
    {
								NOT_IMPLEMENTED;
							;}
    break;

  case 53:
#line 375 "parser.y"
    {	ERRORCHECK(g_semgen.recFunctionBlockBegin());	;}
    break;

  case 54:
#line 377 "parser.y"
    {	ERRORCHECK(g_semgen.recFunctionBlockEnd());	;}
    break;

  case 55:
#line 382 "parser.y"
    {
					yyval.m_directive = g_semgen.recDirective(yyvsp[0].m_charString);
					ERRORCHECK(yyval.m_directive != DIRECTIVE_UNKNOWN);
				;}
    break;

  case 56:
#line 402 "parser.y"
    {
					yyval.m_type = g_semgen.findType(yyvsp[0].m_charString);
					ERRORCHECK(yyval.m_type != NULL);
				;}
    break;

  case 57:
#line 411 "parser.y"
    {
									yyval.m_object = NULL;
								;}
    break;

  case 58:
#line 415 "parser.y"
    {
									NOT_IMPLEMENTED;
								;}
    break;

  case 85:
#line 482 "parser.y"
    {
								NOT_IMPLEMENTED;
							;}
    break;

  case 89:
#line 495 "parser.y"
    {
						yyval.m_type = g_semgen.findType(yyvsp[0].m_charString);
						ERRORCHECK(yyval.m_type != NULL);
					;}
    break;

  case 90:
#line 500 "parser.y"
    {
						NOT_IMPLEMENTED;
					;}
    break;

  case 138:
#line 618 "parser.y"
    {
						yyval.m_identifierVector = REG(new IdentifierVector());
						yyval.m_identifierVector->push_back(yyvsp[0].m_charString);	
					;}
    break;

  case 139:
#line 623 "parser.y"
    {
						yyval.m_identifierVector = yyvsp[-2].m_identifierVector;
						yyval.m_identifierVector->push_back(yyvsp[0].m_charString);
					;}
    break;

  case 140:
#line 629 "parser.y"
    {	g_semgen.setIdentifiersRecognition(false);	;}
    break;

  case 141:
#line 631 "parser.y"
    {	g_semgen.setIdentifiersRecognition(true);	;}
    break;

  case 142:
#line 632 "parser.y"
    {
					yyval.m_charString = yyvsp[-1].m_charString;
				;}
    break;

  case 144:
#line 646 "parser.y"
    {
						yyval.m_const = g_semgen.recUnsignedNumber(yyvsp[0].m_unsignedInteger);
						ERRORCHECK(yyval.m_const != NULL);
					;}
    break;

  case 145:
#line 651 "parser.y"
    {
						yyval.m_const = g_semgen.recUnsignedNumber(yyvsp[0].m_real);
						ERRORCHECK(yyval.m_const != NULL);
					;}
    break;

  case 148:
#line 666 "parser.y"
    {
						ERRORCHECK(g_semgen.recStatementPart(yyvsp[0].m_statementSequence));
					;}
    break;

  case 149:
#line 672 "parser.y"
    {
					yyval.m_statement = g_semgen.recStatement(yyvsp[-1].m_unsignedInteger, yyvsp[0].m_statement);
					ERRORCHECK(yyval.m_statement != NULL);
				;}
    break;

  case 150:
#line 677 "parser.y"
    {
					yyval.m_statement = g_semgen.recStatement(yyvsp[-1].m_unsignedInteger, yyvsp[0].m_statement);
					ERRORCHECK(yyval.m_statement != NULL);
				;}
    break;

  case 151:
#line 684 "parser.y"
    {	
							yyval.m_unsignedInteger = INVALID_LABEL_VALUE;	
						;}
    break;

  case 152:
#line 688 "parser.y"
    {
							yyval.m_unsignedInteger = yyvsp[-1].m_unsignedInteger;
						;}
    break;

  case 153:
#line 694 "parser.y"
    {
							yyval.m_statement = yyvsp[0].m_statement;
						;}
    break;

  case 154:
#line 698 "parser.y"
    {
							yyval.m_statement = yyvsp[0].m_statement;
						;}
    break;

  case 155:
#line 702 "parser.y"
    {
							yyval.m_statement = yyvsp[0].m_statement;
						;}
    break;

  case 156:
#line 706 "parser.y"
    {
							NOT_IMPLEMENTED;
						;}
    break;

  case 157:
#line 712 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statementSequence;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 158:
#line 717 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 159:
#line 722 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 160:
#line 727 "parser.y"
    {
								NOT_IMPLEMENTED;
							;}
    break;

  case 161:
#line 733 "parser.y"
    {
						yyval.m_statement = g_semgen.recEmptyStatement();
						ERRORCHECK(yyval.m_statement != NULL);
					;}
    break;

  case 162:
#line 747 "parser.y"
    {
								yyval.m_statement = g_semgen.recAssignmentStatement(yyvsp[-2].m_objectAccess, yyvsp[0].m_expression);
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 163:
#line 754 "parser.y"
    {
									yyval.m_objectAccess = g_semgen.checkVariableOrFunctionAccess(yyvsp[0].m_objectAccess);
									ERRORCHECK(yyval.m_objectAccess != NULL);
								;}
    break;

  case 165:
#line 764 "parser.y"
    {
							yyval.m_statementSequence = yyvsp[-1].m_statementSequence;
							ERRORCHECK(yyval.m_statementSequence != NULL);
						;}
    break;

  case 166:
#line 771 "parser.y"
    {
							yyval.m_statementSequence = g_semgen.recStatementSequence(yyvsp[0].m_statement);
							ERRORCHECK(yyval.m_statementSequence != NULL);
						;}
    break;

  case 167:
#line 776 "parser.y"
    {
							yyval.m_statementSequence = g_semgen.recStatementSequence(yyvsp[-2].m_statementSequence, yyvsp[0].m_statement);
							ERRORCHECK(yyval.m_statementSequence != NULL);
						;}
    break;

  case 168:
#line 783 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 169:
#line 788 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 170:
#line 793 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 171:
#line 800 "parser.y"
    {
							yyval.m_statement = g_semgen.recRepeatStatement(yyvsp[-2].m_statementSequence, yyvsp[0].m_expression);
							ERRORCHECK(yyval.m_statement != NULL);
						;}
    break;

  case 172:
#line 807 "parser.y"
    {
						yyval.m_statement = g_semgen.recWhileStatement(yyvsp[-2].m_expression, yyvsp[0].m_statement);
						ERRORCHECK(yyval.m_statement != NULL);
					;}
    break;

  case 173:
#line 815 "parser.y"
    {
						yyval.m_statement = g_semgen.recForStatement(yyvsp[-3].m_forLoopDir, yyvsp[-6].m_objectAccess, yyvsp[-4].m_expression, yyvsp[-2].m_expression, yyvsp[0].m_statement);
						ERRORCHECK(yyval.m_statement != NULL);
					;}
    break;

  case 174:
#line 828 "parser.y"
    {
							ERRORCHECK(g_semgen.isEntireVariable(yyvsp[0].m_objectAccess));
							yyval.m_objectAccess = yyvsp[0].m_objectAccess;
						;}
    break;

  case 175:
#line 835 "parser.y"
    {	
								yyval.m_forLoopDir = Semantics::ForStatement::DIRECTION_UPTO;	
							;}
    break;

  case 176:
#line 839 "parser.y"
    {	
								yyval.m_forLoopDir = Semantics::ForStatement::DIRECTION_DOWNTO;	
							;}
    break;

  case 177:
#line 845 "parser.y"
    {
						yyval.m_expression = yyvsp[0].m_expression;
						ERRORCHECK(yyval.m_expression != NULL);
					;}
    break;

  case 178:
#line 852 "parser.y"
    {
					yyval.m_expression = yyvsp[0].m_expression;
					ERRORCHECK(yyval.m_expression != NULL);
				;}
    break;

  case 182:
#line 866 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 183:
#line 871 "parser.y"
    {
								yyval.m_statement = yyvsp[0].m_statement;
								ERRORCHECK(yyval.m_statement != NULL);
							;}
    break;

  case 184:
#line 878 "parser.y"
    {
						yyval.m_statement = g_semgen.recIfStatement(yyvsp[-2].m_expression, yyvsp[0].m_statement);
						ERRORCHECK(yyval.m_statement != NULL);
					;}
    break;

  case 185:
#line 883 "parser.y"
    {
						yyval.m_statement = g_semgen.recIfStatement(yyvsp[-4].m_expression, yyvsp[-2].m_statement, yyvsp[0].m_statement);
						ERRORCHECK(yyval.m_statement != NULL);
					;}
    break;

  case 186:
#line 890 "parser.y"
    {
						NOT_IMPLEMENTED;
					;}
    break;

  case 194:
#line 931 "parser.y"
    {
							yyval.m_statement = g_semgen.recProcedureStatement(yyvsp[-1].m_charString, yyvsp[0].m_actualParameterVector);
							ERRORCHECK(yyval.m_statement != NULL);
						;}
    break;

  case 195:
#line 938 "parser.y"
    {
									yyval.m_actualParameterVector = NULL;
								;}
    break;

  case 196:
#line 942 "parser.y"
    {
									yyval.m_actualParameterVector = yyvsp[0].m_actualParameterVector;
									ERRORCHECK(yyval.m_actualParameterVector != NULL);
								;}
    break;

  case 197:
#line 949 "parser.y"
    {
								yyval.m_actualParameterVector = yyvsp[-1].m_actualParameterVector;
							;}
    break;

  case 198:
#line 955 "parser.y"
    {
									yyval.m_actualParameterVector = REG(new ActualParameterVector());
									yyval.m_actualParameterVector->push_back(yyvsp[0].m_actualParameter);	
								;}
    break;

  case 199:
#line 960 "parser.y"
    {
									ERRORCHECK(yyvsp[-2].m_actualParameterVector != NULL);
									ERRORCHECK(yyvsp[0].m_actualParameter != NULL);
									yyval.m_actualParameterVector = yyvsp[-2].m_actualParameterVector;
									yyval.m_actualParameterVector->push_back(yyvsp[0].m_actualParameter);
								;}
    break;

  case 200:
#line 977 "parser.y"
    {
							yyval.m_actualParameter = g_semgen.recActualParameter(yyvsp[0].m_objectAccess);
							ERRORCHECK(yyval.m_actualParameter != NULL);
						;}
    break;

  case 201:
#line 982 "parser.y"
    {
							yyval.m_actualParameter = g_semgen.recActualParameter(yyvsp[0].m_expression);
							ERRORCHECK(yyval.m_actualParameter != NULL);
						;}
    break;

  case 202:
#line 987 "parser.y"
    {
							NOT_IMPLEMENTED;
						;}
    break;

  case 203:
#line 991 "parser.y"
    {
							NOT_IMPLEMENTED;
						;}
    break;

  case 204:
#line 995 "parser.y"
    {
							yyval.m_actualParameter = g_semgen.recActualParameterProc(yyvsp[0].m_charString);
							ERRORCHECK(yyval.m_actualParameter != NULL);
						;}
    break;

  case 205:
#line 1000 "parser.y"
    {
							yyval.m_actualParameter = g_semgen.recActualParameterFunc(yyvsp[0].m_charString);
							ERRORCHECK(yyval.m_actualParameter != NULL);
						;}
    break;

  case 206:
#line 1027 "parser.y"
    {
							yyval.m_objectAccess = g_semgen.checkVariableAccess(yyvsp[0].m_objectAccess);
							ERRORCHECK(yyval.m_objectAccess != NULL);
						;}
    break;

  case 207:
#line 1034 "parser.y"
    {
						yyval.m_objectAccess = g_semgen.recObjectAccess(yyvsp[0].m_charString);
						ERRORCHECK(yyval.m_objectAccess != NULL);
					;}
    break;

  case 208:
#line 1039 "parser.y"
    {
						// TODO field designator (field name in WITH)
						// entire_variable
						// function_identifier
						yyval.m_objectAccess = g_semgen.recObjectAccess(yyvsp[0].m_charString);
						ERRORCHECK(yyval.m_objectAccess != NULL);
					;}
    break;

  case 209:
#line 1047 "parser.y"
    {
						NOT_IMPLEMENTED;
					;}
    break;

  case 210:
#line 1051 "parser.y"
    {
						NOT_IMPLEMENTED;
					;}
    break;

  case 211:
#line 1055 "parser.y"
    {
						NOT_IMPLEMENTED;
					;}
    break;

  case 212:
#line 1061 "parser.y"
    {
						ERRORCHECK(g_semgen.isRecordVariable(yyvsp[0].m_objectAccess));
						yyval.m_objectAccess = yyvsp[0].m_objectAccess;
					;}
    break;

  case 217:
#line 1078 "parser.y"
    {
							yyval.m_expression = g_semgen.checkBooleanExpression(yyvsp[0].m_expression);
							ERRORCHECK(yyval.m_expression != NULL);
						;}
    break;

  case 218:
#line 1085 "parser.y"
    {
							NOT_IMPLEMENTED;
						;}
    break;

  case 219:
#line 1091 "parser.y"
    {
					yyval.m_expression = g_semgen.recExpression(yyvsp[0].m_typed);
					ERRORCHECK(yyval.m_expression != NULL);
				;}
    break;

  case 220:
#line 1096 "parser.y"
    {
					yyval.m_expression = g_semgen.recExpression(yyvsp[-2].m_typed, yyvsp[-1].m_operator, yyvsp[0].m_typed);
					ERRORCHECK(yyval.m_expression != NULL);
				;}
    break;

  case 221:
#line 1107 "parser.y"
    {
							yyval.m_typed = g_semgen.recSimpleExpression(yyvsp[-1].m_operator, yyvsp[0].m_typed);
							ERRORCHECK(yyval.m_typed != NULL);
						;}
    break;

  case 222:
#line 1112 "parser.y"
    {
							yyval.m_typed = g_semgen.recSimpleExpression(yyvsp[-2].m_typed, yyvsp[-1].m_operator, yyvsp[0].m_typed);
							ERRORCHECK(yyval.m_typed != NULL);
						;}
    break;

  case 223:
#line 1122 "parser.y"
    {
				yyval.m_typed = yyvsp[0].m_typed;
			;}
    break;

  case 224:
#line 1126 "parser.y"
    {
				yyval.m_typed = g_semgen.recTerm(yyvsp[-2].m_typed, yyvsp[-1].m_operator, yyvsp[0].m_typed);
				ERRORCHECK(yyval.m_typed != NULL);
			;}
    break;

  case 225:
#line 1134 "parser.y"
    {
							ERRORCHECK(g_semgen.checkFactorAccess(yyvsp[0].m_objectAccess));
						;}
    break;

  case 226:
#line 1154 "parser.y"
    {
				yyval.m_typed = yyvsp[0].m_typed;
				ERRORCHECK(yyval.m_typed != NULL);
			;}
    break;

  case 227:
#line 1159 "parser.y"
    {
				yyval.m_typed = yyvsp[0].m_const;
			;}
    break;

  case 228:
#line 1163 "parser.y"
    {
				NOT_IMPLEMENTED;
			;}
    break;

  case 229:
#line 1167 "parser.y"
    {
				// TODO: Check it
				yyval.m_typed = yyvsp[-1].m_expression;
				ERRORCHECK(yyval.m_typed != NULL);
			;}
    break;

  case 230:
#line 1173 "parser.y"
    {
				yyval.m_typed = g_semgen.recNotFactor(yyvsp[-1].m_operator, yyvsp[0].m_typed);
				ERRORCHECK(yyval.m_typed != NULL);
			;}
    break;

  case 232:
#line 1194 "parser.y"
    {
							yyval.m_const = g_semgen.recConstant(yyvsp[0].m_charString);
							ERRORCHECK(yyval.m_const != NULL);
						;}
    break;

  case 233:
#line 1199 "parser.y"
    {
							yyval.m_const = g_semgen.recNil();
							ERRORCHECK(yyval.m_const != NULL);
						;}
    break;

  case 241:
#line 1225 "parser.y"
    {	yyval.m_operator = O_EQ;	;}
    break;

  case 242:
#line 1226 "parser.y"
    {	yyval.m_operator = O_GT;	;}
    break;

  case 243:
#line 1227 "parser.y"
    {	yyval.m_operator = O_LT;	;}
    break;

  case 244:
#line 1228 "parser.y"
    {	yyval.m_operator = O_NE;	;}
    break;

  case 245:
#line 1229 "parser.y"
    {	yyval.m_operator = O_LE;	;}
    break;

  case 246:
#line 1230 "parser.y"
    {	yyval.m_operator = O_GE;	;}
    break;

  case 247:
#line 1231 "parser.y"
    {	yyval.m_operator = O_IN;	;}
    break;

  case 248:
#line 1234 "parser.y"
    {	yyval.m_operator = O_PLUS;	;}
    break;

  case 249:
#line 1235 "parser.y"
    {	yyval.m_operator = O_MINUS;	;}
    break;

  case 250:
#line 1236 "parser.y"
    {	yyval.m_operator = O_OR;		;}
    break;

  case 251:
#line 1239 "parser.y"
    {	yyval.m_operator = O_MUL;		;}
    break;

  case 252:
#line 1240 "parser.y"
    {	yyval.m_operator = O_REALDIV;	;}
    break;

  case 253:
#line 1241 "parser.y"
    {	yyval.m_operator = O_DIV;		;}
    break;

  case 254:
#line 1242 "parser.y"
    {	yyval.m_operator = O_MOD;		;}
    break;

  case 255:
#line 1243 "parser.y"
    {	yyval.m_operator = O_AND;		;}
    break;

  case 256:
#line 1246 "parser.y"
    {	yyval.m_operator = O_NOT;	;}
    break;


    }

/* Line 991 of yacc.c.  */
#line 2479 "parser.yacc.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("syntax error, unexpected ") + 1;
	  yysize += yystrlen (yytname[yytype]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* Return failure if at end of input.  */
      if (yychar == YYEOF)
        {
	  /* Pop the error token.  */
          YYPOPSTACK;
	  /* Pop the rest of the stack.  */
	  while (yyss < yyssp)
	    {
	      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
	      yydestruct (yystos[*yyssp], yyvsp);
	      YYPOPSTACK;
	    }
	  YYABORT;
        }

      YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
      yydestruct (yytoken, &yylval);
      yychar = YYEMPTY;

    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab2;


/*----------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action.  |
`----------------------------------------------------*/
yyerrlab1:

  /* Suppress GCC warning that yyerrlab1 is unused when no action
     invokes YYERROR.  */
#if defined (__GNUC_MINOR__) && 2093 <= (__GNUC__ * 1000 + __GNUC_MINOR__)
  __attribute__ ((__unused__))
#endif


  goto yyerrlab2;


/*---------------------------------------------------------------.
| yyerrlab2 -- pop states until the error token can be shifted.  |
`---------------------------------------------------------------*/
yyerrlab2:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      yyvsp--;
      yystate = *--yyssp;

      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 1263 "parser.y"


/*#################################################################################*/

void yyerror (const char* s)
{
    cerr << s << endl;
}

