/*#################################################################################*/
/* OPTIONS */
/*#################################################################################*/

%debug

/*#################################################################################*/
/* TOKENS */
/*#################################################################################*/

/*
** KEYWORDS
*/
%token _AND _ARRAY _BEGIN _CASE _CONST _DIV _DO _DOWNTO _ELSE _END _FILE _FOR
%token _FUNCTION _GOTO _IF _IN _LABEL _MOD _NIL _NOT _OF _OR _PACKED _PROCEDURE
%token _PROGRAM _RECORD _REPEAT _SET _THEN _TO _TYPE _UNTIL _VAR _WHILE _WITH

/*
** OPERATORS
*/
%token OP_LE
%token OP_GE
%token OP_NE
                        
%token OP_ASSIGN

/*
** ADDITIONAL ELEMENTS
*/
%token _DOUBLE_DOT

/*
** BASIC
*/
%token <m_unsignedInteger> _UNSIGNED_INTEGER
%token <m_real> _UNSIGNED_REAL
%token <m_charString> _STRING

/*
** ADDITIONAL SYMBOLS
*/
%token _SQUARE_ALT_OPEN
%token _SQUARE_ALT_CLOSE

/*
** IDENTIFIERS
*/
%token <m_charString> _IDENTIFIER
%token <m_charString> _FUNCTION_IDENTIFIER _PROCEDURE_IDENTIFIER
%token <m_charString> _TYPE_IDENTIFIER _CONSTANT_IDENTIFIER


/*#################################################################################*/
/* HEADER CODE */
/*#################################################################################*/
%{

/** \file parser.y
*** \brief
*** YACC (BISON) parser
***/

/** \file parser.yacc.cpp
*** \brief
*** Generated YACC (BISON) parser code file
***/

/** \file parser.yacc.hpp
*** \brief
*** Generated YACC (BISON) parser header file
***/

#include <iostream>
#include <set>
using namespace std;

#include "parserincludes.hpp"

// Forward declaration
extern int yylex(void);

/// \brief
/// Print YACC error message
///
/// \param[in] message - error message to print
void yyerror(char const* message);

/// Verbose YYERROR message
#define YYERROR_VERBOSE

/// Error checking macro - calls YYERROR if condition is false
#define ERRORCHECK(a) if((a) == false) YYERROR

/// Not implemented feature macro
#define NOT_IMPLEMENTED							\
	cerr << "Element not implemented " << __FILE__ << " " << __LINE__ << endl;	\
	YYERROR

%}

/*#################################################################################*/
/* UNION */
/*#################################################################################*/
%union
{
    char* m_charString;
    Operator m_operator;
    double m_real;
    unsigned int m_unsignedInteger;
    int m_signedInteger;
	LabelVector* m_labelVector;
	Semantics::Const* m_const;
	IdentifierVector* m_identifierVector;
	Semantics::Type* m_type;
	Semantics::ForStatement::Direction m_forLoopDir;
	Semantics::Variable* m_variable;
	Semantics::ObjectAccess* m_objectAccess;
	Semantics::Statement* m_statement;
	Semantics::Expression* m_expression;
	Semantics::Object* m_object;
	Semantics::Typed* m_typed;
	StatementVector* m_statementVector;
	Semantics::StatementSequence* m_statementSequence;
	Directive m_directive;
	Semantics::Function* m_function;
	Semantics::Procedure* m_procedure;
	ActualParameterVector* m_actualParameterVector;
	Semantics::ActualParameter* m_actualParameter;
}

/*#################################################################################*/
/* TYPES */
/*#################################################################################*/
%type <m_operator> relational_operator adding_operator multyplying_operator 
%type <m_operator> not_operator

%type <m_charString> identifier constant_identifier type_identifier 
%type <m_charString> function_identifier procedure_identifier

%type <m_charString> character_string

%type <m_unsignedInteger> unsigned_integer
%type <m_real> unsigned_real
%type <m_operator> sign opt_sign

%type <m_unsignedInteger> label opt_statement_label
%type <m_labelVector> label_list 
%type <m_const> constant unsigned_number

%type <m_identifierVector> identifier_list program_parameter_list_opt program_parameter_list
%type <m_type> type_denoter

%type <m_forLoopDir> for_statement_direction

%type <m_objectAccess> object_access variable_access variable_or_function_access
%type <m_objectAccess> control_variable record_variable

%type <m_expression> expression boolean_expression initial_value final_value
%type <m_typed> simple_expression term factor factor_object_use

%type <m_const> unsigned_constant

%type <m_statement> statement assignment_statement empty_statement
%type <m_statement> simple_statement structured_statement conditional_statement
%type <m_statement> if_statement repetitive_statement repeat_statement
%type <m_statement> while_statement for_statement case_statement procedure_statement

%type <m_statementSequence> compound_statement statement_sequence statement_part

%type <m_directive> directive

%type <m_procedure> procedure_heading
%type <m_function> function_heading

%type <m_type> result_type

/** \todo Change this later */
%type <m_object> opt_formal_parameter_list

%type <m_actualParameter> actual_parameter
%type <m_actualParameterVector> opt_actual_parameter_list actual_parameter_list 
%type <m_actualParameterVector> actual_parameter_list_list


%%

/*#################################################################################*/

program :   program_heading ';' program_block '.'
        ;

program_heading :   _PROGRAM identifier program_parameter_list_opt
                    {
                        ERRORCHECK(g_semgen.recProgramHeading($2, $3));
                    }
                ;

program_parameter_list_opt	:
								{
									$$ = NULL;
								}
                            |	program_parameter_list
								{
									$$ = $1;
								}
                            ;

program_parameter_list	:	'(' identifier_list ')'
							{
								$$ = $2;
							}
                        ;

program_block	:	block
					{
						ERRORCHECK(g_semgen.recProgramBlockEnd());
					}
                ;
                
block	:	{	
				// ERRORCHECK(g_semgen.checkDeclarationStatus());	
			}
			label_declaration_part
            constant_definition_part
            type_definition_part
            variable_declaration_part
            procedure_and_function_declaration_part
            statement_part
            ;

/*#################################################################################*/

label_declaration_part	:
                        |	_LABEL label_list ';'
							{
								ERRORCHECK(g_semgen.recLabelDeclarationPart($2));
							}
                        ;

label_list	:	label
				{
					$$ = REG(new LabelVector());
					$$->push_back($1);	
				}
            |	label_list ',' label
				{
					$$ = $1;
					$$->push_back($3);
				}
            ;

label   :   _UNSIGNED_INTEGER
        ;

/*#################################################################################*/

constant_definition_part    :
                            |   _CONST constant_definition_list
                            ;

constant_definition_list    :   constant_definition ';'
                            |   constant_definition_list constant_definition ';'
                            ;

constant_definition :   identifier '=' constant
						{
							ERRORCHECK(g_semgen.recConstantDefinition($1, $3));
						}
                    ;

constant    :   opt_sign unsigned_number
				{
					$$ = g_semgen.recConstant($1, $2);
					ERRORCHECK($$ != NULL);
				}
            |   opt_sign constant_identifier
				{
					$$ = g_semgen.recConstant($1, $2);
					ERRORCHECK($$ != NULL);
				}
            |   character_string
				{
					$$ = g_semgen.recConstant($1);
					ERRORCHECK($$ != NULL);
				}
            ;
            
constant_identifier :   _CONSTANT_IDENTIFIER
                    ;

/*#################################################################################*/

opt_sign    :
                {   $$ = O_NOOP;    }
            |   sign
            ;

sign    :   '+'
            {   $$ = O_PLUS;    }
        |   '-'
            {   $$ = O_MINUS;   }
        ;

/*#################################################################################*/

variable_declaration_part	:
                            |	_VAR variable_declaration_list
                            ;
                            
variable_declaration_list	:	variable_declaration ';'
                            |	variable_declaration_list variable_declaration ';'
                            ;
                            
variable_declaration	:	identifier_list ':' type_denoter
							{
								ERRORCHECK(g_semgen.recVariableDeclaration($1, $3));
							}
                        ;

/*#################################################################################*/

procedure_and_function_declaration_part	:
                                        |	procedure_and_function_declaration_list
                                        ;

procedure_and_function_declaration_list	:	procedure_or_function_declaration
                                        |	procedure_and_function_declaration_list procedure_or_function_declaration
                                        ;

procedure_or_function_declaration	:	procedure_declaration ';'
                                    |	function_declaration ';'
                                    ;


procedure_declaration	:	procedure_heading ';' directive
						|	procedure_heading ';' procedure_block
						|	procedure_identification ';' procedure_block
						;

procedure_heading	:	_PROCEDURE identifier opt_formal_parameter_list
					;

procedure_identification	:	_PROCEDURE procedure_identifier
								{
									NOT_IMPLEMENTED;
								}
							;

procedure_identifier	:	_PROCEDURE_IDENTIFIER
						;

procedure_block	:	{	ERRORCHECK(g_semgen.recProcedureBlockBegin());	}
					block
					{	ERRORCHECK(g_semgen.recProcedureBlockEnd());	}
                ;
                

function_declaration	:	function_heading ';' directive
						|	function_heading ';' function_block
						|	function_identification ';' function_block
						;

function_heading	:	_FUNCTION identifier opt_formal_parameter_list ':' result_type
					;

function_identification	:	_FUNCTION function_identifier
							{
								NOT_IMPLEMENTED;
							}
						;

function_identifier	:	_FUNCTION_IDENTIFIER
					;

function_block	:	{	ERRORCHECK(g_semgen.recFunctionBlockBegin());	}
					block
					{	ERRORCHECK(g_semgen.recFunctionBlockEnd());	}
                ;


directive	:	_IDENTIFIER
				{
					$$ = g_semgen.recDirective($1);
					ERRORCHECK($$ != DIRECTIVE_UNKNOWN);
				}
            ;

/*
result_type	:	simple_type_identifier
            |	pointer_type_identifier
            ;
simple_type_identifier	:	type_identifier
                        ;
                        
pointer_type_identifier	:	type_identifier
                        ;

- Because of conflicts simple-type-identifier and pointer-type-identifier
  both moved here as type-identifier
*/
result_type	:	type_identifier
				{
					$$ = g_semgen.findType($1);
					ERRORCHECK($$ != NULL);
				}
            ;



opt_formal_parameter_list	:
								{
									$$ = NULL;
								}
                            |	formal_parameter_list
								{
									NOT_IMPLEMENTED;
								}
                            ;

formal_parameter_list	:	'(' formal_parameter_list_list ')'
                        ;
                        
formal_parameter_list_list	:	formal_parameter_section_list
                            |	formal_parameter_list_list ';' formal_parameter_section_list
                            ;

/* ############### CONSULT WITH DOCUMENTATION - ISO-7185 - 6.6.3.1 ############ */
formal_parameter_section_list	:	value_parameter_specification
                                |	variable_parameter_specification
                                |	procedural_parameter_specification
                                |	functional_parameter_specification
/* ############### CONSULT WITH DOCUMENTATION - ISO-7185 - 6.6.3.7.1 ############ */
                                |	conformant_array_parameter_specification
                                ;

value_parameter_specification	:	identifier_list ':' type_identifier
                                ;

variable_parameter_specification	:	_VAR identifier_list ':' type_identifier
                                    ;

procedural_parameter_specification	:	procedure_heading
                                    ;

functional_parameter_specification	:	function_heading
                                    ;

conformant_array_parameter_specification	:	value_conformant_parameter_specification
                                            |	variable_conformant_parameter_specification
                                            ;

value_conformant_parameter_specification	:	identifier_list ':' conformant_array_schema
                                            ;

variable_conformant_parameter_specification	:	_VAR identifier_list ':' conformant_array_schema
                                            ;

conformant_array_schema	:	packed_conformant_array_schema
                        |	unpacked_conformant_array_schema
                        ;

packed_conformant_array_schema	:	_PACKED _ARRAY square_open index_type_specification square_close _OF type_identifier
                                ;

unpacked_conformant_array_schema	:	_ARRAY square_open index_type_specification_list square_close _OF unpacked_conformant_array_schema_type
                                    ;
                                    
unpacked_conformant_array_schema_type	:	type_identifier
                                        |	conformant_array_schema
                                        ;

index_type_specification_list	:	index_type_specification
                                |	index_type_specification_list ';' index_type_specification
                                ;

index_type_specification	:	identifier _DOUBLE_DOT identifier ':' ordinal_type_identifier
                            ;


type_definition_part	:
                        |	_TYPE type_definition_list
							{
								NOT_IMPLEMENTED;
							}
                        ;

type_definition_list	:	type_definition ';'
                        |	type_definition_list type_definition ';'
                        ;

type_definition	:	identifier '=' type_denoter
                ;

type_denoter	:	type_identifier
					{
						$$ = g_semgen.findType($1);
						ERRORCHECK($$ != NULL);
					}					
                |	new_type
					{
						NOT_IMPLEMENTED;
					}
                ;

type_identifier	:	_TYPE_IDENTIFIER
				;
                
new_type	:	new_ordinal_type
            |	new_structured_type
            |	new_pointer_type
            ;

new_ordinal_type	:	enumerated_type
                    |	subrange_type
                    ;

enumerated_type	:	'(' identifier_list ')'
                ;

subrange_type	:	constant _DOUBLE_DOT constant
                ;

new_structured_type	:	opt_packed unpacked_structured_type
                    ;

opt_packed	:
            |	_PACKED
            ;
            
unpacked_structured_type	:	array_type
                            |	record_type
                            |	set_type
                            |	file_type
                            ;

array_type	:	_ARRAY square_open index_type_list square_close _OF component_type
            ;

index_type_list	:	index_type
                |	index_type_list ',' index_type
                ;
                
index_type	:	ordinal_type
            ;

ordinal_type	:	new_ordinal_type
                |	ordinal_type_identifier
                ;

ordinal_type_identifier	:	type_identifier
                        ;

record_type	:	_RECORD field_list _END
            ;
            
field_list	:
            |	field_list_data opt_semicolon
            ;

field_list_data	:	fixed_part
                |	fixed_part ';' variant_part
                |	variant_part
                ;

fixed_part	:	record_section
            |	fixed_part ';' record_section
            ;

record_section	:	identifier_list ':' type_denoter
                ;

variant_part	:	_CASE variant_selector _OF variant_list
                ;
                
variant_selector	:	tag_field ':' tag_type
                    |	tag_type
                    ;
                    
variant_list	:	variant
                |	variant_list ';' variant
                ;
                
variant	:	case_constant_list ':' '(' field_list ')'
        ;
        
opt_semicolon	:
                |	';'
                ;

tag_type	:	ordinal_type_identifier
            ;

tag_field	:	identifier
            ;

set_type	:	_SET _OF base_type
            ;

base_type	:	ordinal_type
            ;

file_type	:	_FILE _OF component_type
            ;

component_type	:	type_denoter
                ;

new_pointer_type	:	arrow_char domain_type
                    ;

domain_type	:	type_identifier
            ;


/*#################################################################################*/

identifier_list	:	identifier
					{
						$$ = REG(new IdentifierVector());
						$$->push_back($1);	
					}
                |	identifier_list ',' identifier
					{
						$$ = $1;
						$$->push_back($3);
					}
                ;

identifier  :   {	g_semgen.setIdentifiersRecognition(false);	}
				_IDENTIFIER
				{	g_semgen.setIdentifiersRecognition(true);	}
				{
					$$ = $2;
				}
            ;


/*#################################################################################*/

character_string    :  _STRING
                    ;

/*#################################################################################*/

unsigned_number :   unsigned_integer
					{
						$$ = g_semgen.recUnsignedNumber($1);
						ERRORCHECK($$ != NULL);
					}
                |   unsigned_real
					{
						$$ = g_semgen.recUnsignedNumber($1);
						ERRORCHECK($$ != NULL);
					}
                ;

unsigned_integer    :   _UNSIGNED_INTEGER
                    ;

unsigned_real   :   _UNSIGNED_REAL
                ;

/*#################################################################################*/

statement_part	:	compound_statement
					{
						ERRORCHECK(g_semgen.recStatementPart($1));
					}
                ;

statement	:	opt_statement_label simple_statement
				{
					$$ = g_semgen.recStatement($1, $2);
					ERRORCHECK($$ != NULL);
				}
            |	opt_statement_label structured_statement
				{
					$$ = g_semgen.recStatement($1, $2);
					ERRORCHECK($$ != NULL);
				}
            ;

opt_statement_label	:
						{	
							$$ = INVALID_LABEL_VALUE;	
						}
                    |	label ':'
						{
							$$ = $1;
						}
                    ;

simple_statement	:	empty_statement
						{
							$$ = $1;
						}
                    |	procedure_statement
						{
							$$ = $1;
						}
                    |	assignment_statement
						{
							$$ = $1;
						}
                    |	goto_statement
						{
							NOT_IMPLEMENTED;
						}
                    ;

structured_statement	:	compound_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        |	conditional_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        |	repetitive_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        |	with_statement
							{
								NOT_IMPLEMENTED;
							}
                        ;

empty_statement	:
					{
						$$ = g_semgen.recEmptyStatement();
						ERRORCHECK($$ != NULL);
					}
                ;

/*-----
assignment_statement	:	variable_access OP_ASSIGN expression
                        |	function_identifier OP_ASSIGN expression
                        ;

function_identifier moved to object access due to reduce/reduce conflict.
-----*/
assignment_statement	:	variable_or_function_access OP_ASSIGN expression
							{
								$$ = g_semgen.recAssignmentStatement($1, $3);
								ERRORCHECK($$ != NULL);
							}
                        ;

variable_or_function_access	:	object_access
								{
									$$ = g_semgen.checkVariableOrFunctionAccess($1);
									ERRORCHECK($$ != NULL);
								}
							;

goto_statement	:	_GOTO label
                ;

compound_statement	:	_BEGIN statement_sequence _END
						{
							$$ = $2;
							ERRORCHECK($$ != NULL);
						}
                    ;

statement_sequence	:	statement
						{
							$$ = g_semgen.recStatementSequence($1);
							ERRORCHECK($$ != NULL);
						}
                    |	statement_sequence ';' statement
						{
							$$ = g_semgen.recStatementSequence($1, $3);
							ERRORCHECK($$ != NULL);
						}
                    ;

repetitive_statement	:	repeat_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        |	while_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        |	for_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        ;

repeat_statement	:	_REPEAT statement_sequence _UNTIL boolean_expression
						{
							$$ = g_semgen.recRepeatStatement($2, $4);
							ERRORCHECK($$ != NULL);
						}
                    ;

while_statement	:	_WHILE boolean_expression _DO statement
					{
						$$ = g_semgen.recWhileStatement($2, $4);
						ERRORCHECK($$ != NULL);
					}
                ;

for_statement	:	_FOR control_variable OP_ASSIGN initial_value
					for_statement_direction final_value _DO statement
					{
						$$ = g_semgen.recForStatement($5, $2, $4, $6, $8);
						ERRORCHECK($$ != NULL);
					}
                ;

/*-----
control_variable	:	entire_variable
                    ;

Due to conflicts it is changed to variable access.
-----*/
control_variable	:	variable_access
						{
							ERRORCHECK(g_semgen.isEntireVariable($1));
							$$ = $1;
						}
                    ;

for_statement_direction	:	_TO
							{	
								$$ = Semantics::ForStatement::DIRECTION_UPTO;	
							}
                        |	_DOWNTO
							{	
								$$ = Semantics::ForStatement::DIRECTION_DOWNTO;	
							}
                        ;

initial_value	:	expression
					{
						$$ = $1;
						ERRORCHECK($$ != NULL);
					}
                ;

final_value	:	expression
				{
					$$ = $1;
					ERRORCHECK($$ != NULL);
				}
            ;

with_statement	:	_WITH record_variable_list _DO statement
                ;

record_variable_list	:	record_variable
                        |	record_variable_list ',' record_variable
                        ;

conditional_statement	:	if_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        |	case_statement
							{
								$$ = $1;
								ERRORCHECK($$ != NULL);
							}
                        ;

if_statement	:	_IF boolean_expression _THEN statement
					{
						$$ = g_semgen.recIfStatement($2, $4);
						ERRORCHECK($$ != NULL);
					}
                |	_IF boolean_expression _THEN statement _ELSE statement
					{
						$$ = g_semgen.recIfStatement($2, $4, $6);
						ERRORCHECK($$ != NULL);
					}
                ;

case_statement	:	_CASE case_index _OF case_list_element_list opt_semicolon _END
					{
						NOT_IMPLEMENTED;
					}
                ;

case_list_element_list	:	case_list_element
                        |	case_list_element_list ';' case_list_element
                        ;

case_index	:	expression
            ;

case_list_element	:	case_constant_list ':' statement
                    ;

case_constant_list	:	case_constant
                    |	case_constant_list ',' case_constant
                    ;

case_constant	:	constant
                ;

/*-----
procedure_statement	:	procedure_identifier opt_actual_parameter_list
                    |	procedure_identifier read_parameter_list
                    |	procedure_identifier readln_parameter_list
                    |	procedure_identifier write_parameter_list
                    |	procedure_identifier writeln_parameter_list
                    ;

6.7.3 [actual-parameter-list] = '(' actual-parameter { ',' actual-parameter } ')' .
6.9.1 read-parameter-list     = '(' [ file-variable ',' ] variable-access { ',' variable-access } ')' .
6.9.2 readln-parameter-list   = [ '(' ( file-variable | variable-access ) { ',' variable-access } ')' ] . 
6.9.3 write-parameter-list    = '(' [ file-variable ',' ] write-parameter { ',' write-parameter } ')' .
6.9.4 writeln-parameter-list  = [ '(' ( file-variable | write-parameter ) { ',' write-parameter } ')' ] .
- Because of reduce/reduce conflicts between actual-parameter, file-variable
  and variable-access (all are variables, actual parameter even more than)
  the recognition must be shifted to semantics.
-----*/

procedure_statement	:	procedure_identifier opt_actual_parameter_list
						{
							$$ = g_semgen.recProcedureStatement($1, $2);
							ERRORCHECK($$ != NULL);
						}
                    ;

opt_actual_parameter_list	:
								{
									$$ = NULL;
								}
                            |	actual_parameter_list
								{
									$$ = $1;
									ERRORCHECK($$ != NULL);
								}
                            ;

actual_parameter_list	:	'(' actual_parameter_list_list ')'
							{
								$$ = $2;
							}
                        ;

actual_parameter_list_list	:	actual_parameter
								{
									$$ = REG(new ActualParameterVector());
									$$->push_back($1);	
								}
                            |	actual_parameter_list_list ',' actual_parameter
								{
									ERRORCHECK($1 != NULL);
									ERRORCHECK($3 != NULL);
									$$ = $1;
									$$->push_back($3);
								}
                            ;

/*-----
- Because of linking all procedure call things together some reduce/reduce conflicts
  happened (actual-parameter, write-parameter etc.).
- Conflict: file-variable is a variable-access so it would be linked together to
  variable-access.
- Conflict: write-parameter actual-parameter => expression. Everything from
  write-parameter moved to actual-parameter.
-----*/
actual_parameter	:	variable_access
						{
							$$ = g_semgen.recActualParameter($1);
							ERRORCHECK($$ != NULL);
						}
                    |	expression
						{
							$$ = g_semgen.recActualParameter($1);
							ERRORCHECK($$ != NULL);
						}
                    |	expression ':' expression
						{
							NOT_IMPLEMENTED;
						}
                    |	expression ':' expression ':' expression
						{
							NOT_IMPLEMENTED;
						}
                    |	procedure_identifier
						{
							$$ = g_semgen.recActualParameterProc($1);
							ERRORCHECK($$ != NULL);
						}
                    |	function_identifier
						{
							$$ = g_semgen.recActualParameterFunc($1);
							ERRORCHECK($$ != NULL);
						}
                    ;

/*-----
variable_access	:	...
                |	identified_variable
                |	buffer_variable
                ;
buffer_variable	:	file_variable arrow_char
                ;
file_variable	:	variable_access
                ;
identified_variable	:	pointer_variable arrow_char
                    ;
pointer_variable	:	variable_access
                    ;

- Because of conflicts file-variable moved here as variable-access
- Because of conflicts pointer-variable moved here as variable-access
- Because of conflicts buffer-variable moved here as variable_access arrow_char
- Because of conflicts identified-variable moved here as variable_access arrow_char
-----*/

variable_access	:	object_access
						{
							$$ = g_semgen.checkVariableAccess($1);
							ERRORCHECK($$ != NULL);
						}
					;

object_access	:	_CONSTANT_IDENTIFIER
					{
						$$ = g_semgen.recObjectAccess($1);
						ERRORCHECK($$ != NULL);
					}
				|	identifier
					{
						// TODO field designator (field name in WITH)
						// entire_variable
						// function_identifier
						$$ = g_semgen.recObjectAccess($1);
						ERRORCHECK($$ != NULL);
					}
				|	variable_access arrow_char
					{
						NOT_IMPLEMENTED;
					}
				|	variable_access '.' field_specifier
					{
						NOT_IMPLEMENTED;
					}
				|	variable_access square_open index_expression_list square_close
					{
						NOT_IMPLEMENTED;
					}
				;

record_variable	:	variable_access
					{
						ERRORCHECK(g_semgen.isRecordVariable($1));
						$$ = $1;
					}
				;

field_specifier	:	field_identifier
				;

field_identifier	:	identifier
					;

index_expression_list	:	index_expression
                        |	index_expression_list ',' index_expression
                        ;

boolean_expression	:	expression
						{
							$$ = g_semgen.checkBooleanExpression($1);
							ERRORCHECK($$ != NULL);
						}
                    ;

index_expression	:	expression
						{
							NOT_IMPLEMENTED;
						}
                    ;

expression	:	simple_expression
				{
					$$ = g_semgen.recExpression($1);
					ERRORCHECK($$ != NULL);
				}
            |	simple_expression relational_operator simple_expression
				{
					$$ = g_semgen.recExpression($1, $2, $3);
					ERRORCHECK($$ != NULL);
				}
            ;


/*-----
simple_expression	=	opt_sign term { adding_operator term }
-----*/
simple_expression	:	opt_sign term
						{
							$$ = g_semgen.recSimpleExpression($1, $2);
							ERRORCHECK($$ != NULL);
						}
                    |	simple_expression adding_operator term
						{
							$$ = g_semgen.recSimpleExpression($1, $2, $3);
							ERRORCHECK($$ != NULL);
						}
                    ;

/*-----
term	=	factor { multyplying_operator factor }
-----*/
term	:	factor
			{
				$$ = $1;
			}
        |	term multyplying_operator factor
			{
				$$ = g_semgen.recTerm($1, $2, $3);
				ERRORCHECK($$ != NULL);
			}
        ;


factor_object_use	:	object_access
						{
							ERRORCHECK(g_semgen.checkFactorAccess($1));
						}
/*
					|	function_designator
						{
							NOT_IMPLEMENTED;
						}
*/
/* ############### CONSULT WITH DOCUMENTATION - ISO-7185 - 6.6.3.7.1 ############ */
/*
					|	bound_identifier
						{
							NOT_IMPLEMENTED;
						}
*/
		;
					;
/* ############### CONSULT WITH DOCUMENTATION - ISO-7185 - 6.7.1 ############ */
factor	:	factor_object_use
			{
				$$ = $1;
				ERRORCHECK($$ != NULL);
			}
		|	unsigned_constant
			{
				$$ = $1;
			}
        |	set_constructor
			{
				NOT_IMPLEMENTED;
			}
        |	'(' expression ')'
			{
				// TODO: Check it
				$$ = $2;
				ERRORCHECK($$ != NULL);
			}
        |	not_operator factor
			{
				$$ = g_semgen.recNotFactor($1, $2);
				ERRORCHECK($$ != NULL);
			}
        ;

bound_identifier	:	identifier
						{
							NOT_IMPLEMENTED;
						}
                    ;

/*-----
unsigned_constant	:	...
					|	constant_identifier
					
- constant_identifier moved to 'factor' because of conflict with variable_access
  (it could be moved forward)
-----*/
unsigned_constant	:	unsigned_number
                    |	character_string
						{
							$$ = g_semgen.recConstant($1);
							ERRORCHECK($$ != NULL);
						}
                    |	_NIL
						{
							$$ = g_semgen.recNil();
							ERRORCHECK($$ != NULL);
						}
                    ;

function_designator	:	function_identifier opt_actual_parameter_list
                    ;

set_constructor	:	square_open opt_member_designator_list square_close
                ;

opt_member_designator_list	:
                            |	member_designator_list
                            ;

member_designator_list	:	member_designator
                        |	member_designator_list ',' member_designator
                        ;

member_designator	:	expression
                    |	expression _DOUBLE_DOT expression
                    ;

/*#################################################################################*/

relational_operator	:	'='		{	$$ = O_EQ;	}
                    |	'>'		{	$$ = O_GT;	}
                    |	'<'		{	$$ = O_LT;	}
                    |	OP_NE	{	$$ = O_NE;	}
                    |	OP_LE	{	$$ = O_LE;	}
                    |	OP_GE	{	$$ = O_GE;	}
                    |	_IN		{	$$ = O_IN;	}
                    ;

adding_operator	:	'+'		{	$$ = O_PLUS;	}
                |	'-'		{	$$ = O_MINUS;	}
                |	_OR		{	$$ = O_OR;		}
                ;

multyplying_operator	:	'*'		{	$$ = O_MUL;		}
                        |	'/'		{	$$ = O_REALDIV;	}
                        |	_DIV	{	$$ = O_DIV;		}
                        |	_MOD	{	$$ = O_MOD;		}
                        |	_AND	{	$$ = O_AND;		}
                        ;

not_operator	:	_NOT	{	$$ = O_NOT;	}
				;

/*#################################################################################*/

arrow_char	:	'^'
            |	'@'
            ;

square_open	:	'['
            |	_SQUARE_ALT_OPEN
            ;
            
square_close	:	']'
                |	_SQUARE_ALT_CLOSE
                ;

%%

/*#################################################################################*/

void yyerror (const char* s)
{
    cerr << s << endl;
}
