//--------------------------------------------------------------------
/// \file
/// \brief
/// Parser type definitions
//--------------------------------------------------------------------

#ifndef OCT_PARSER_TYPEDEFS_HPP
#define OCT_PARSER_TYPEDEFS_HPP

//--------------------------------------------------------------------

#include <vector>

#include "semantics/operators.hpp"
#include "semantics/forstatement.hpp"
#include "semantics/expression.hpp"
#include "semantics/const.hpp"
#include "semantics/labeltype.hpp"
#include "semantics/objectaccess.hpp"
#include "semantics/statementsequence.hpp"
#include "semantics/directives.hpp"

//--------------------------------------------------------------------

// Forward declarations
namespace Semantics
{
	class Object;

	class Const;
	class SimpleConst;
	class Type;
	class SimpleType;

	class Variable;
	class ObjectAccess;
	class ActualParameter;

	class Typed;
	class Expression;

	class Statement;
	class StatementSequence;
	class AssignmentStatement;

	class Procedure;
	class Function;
}

/// Labels vector type
typedef std::vector<LabelType> LabelVector;

/// Identifier type
typedef char* IdentifierType;
/// Identifiers vector type
typedef std::vector<IdentifierType> IdentifierVector;

/// Statements vector type
typedef std::vector<Semantics::Statement*> StatementVector; 

/// Actual parameters vector type
typedef std::vector<Semantics::ActualParameter*> ActualParameterVector;

//--------------------------------------------------------------------

#endif /* OCT_PARSER_TYPEDEFS_HPP */
