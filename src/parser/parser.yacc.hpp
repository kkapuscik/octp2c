/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
#define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     _AND = 258,
     _ARRAY = 259,
     _BEGIN = 260,
     _CASE = 261,
     _CONST = 262,
     _DIV = 263,
     _DO = 264,
     _DOWNTO = 265,
     _ELSE = 266,
     _END = 267,
     _FILE = 268,
     _FOR = 269,
     _FUNCTION = 270,
     _GOTO = 271,
     _IF = 272,
     _IN = 273,
     _LABEL = 274,
     _MOD = 275,
     _NIL = 276,
     _NOT = 277,
     _OF = 278,
     _OR = 279,
     _PACKED = 280,
     _PROCEDURE = 281,
     _PROGRAM = 282,
     _RECORD = 283,
     _REPEAT = 284,
     _SET = 285,
     _THEN = 286,
     _TO = 287,
     _TYPE = 288,
     _UNTIL = 289,
     _VAR = 290,
     _WHILE = 291,
     _WITH = 292,
     OP_LE = 293,
     OP_GE = 294,
     OP_NE = 295,
     OP_ASSIGN = 296,
     _DOUBLE_DOT = 297,
     _UNSIGNED_INTEGER = 298,
     _UNSIGNED_REAL = 299,
     _STRING = 300,
     _SQUARE_ALT_OPEN = 301,
     _SQUARE_ALT_CLOSE = 302,
     _IDENTIFIER = 303,
     _FUNCTION_IDENTIFIER = 304,
     _PROCEDURE_IDENTIFIER = 305,
     _TYPE_IDENTIFIER = 306,
     _CONSTANT_IDENTIFIER = 307
   };
#endif
#define _AND 258
#define _ARRAY 259
#define _BEGIN 260
#define _CASE 261
#define _CONST 262
#define _DIV 263
#define _DO 264
#define _DOWNTO 265
#define _ELSE 266
#define _END 267
#define _FILE 268
#define _FOR 269
#define _FUNCTION 270
#define _GOTO 271
#define _IF 272
#define _IN 273
#define _LABEL 274
#define _MOD 275
#define _NIL 276
#define _NOT 277
#define _OF 278
#define _OR 279
#define _PACKED 280
#define _PROCEDURE 281
#define _PROGRAM 282
#define _RECORD 283
#define _REPEAT 284
#define _SET 285
#define _THEN 286
#define _TO 287
#define _TYPE 288
#define _UNTIL 289
#define _VAR 290
#define _WHILE 291
#define _WITH 292
#define OP_LE 293
#define OP_GE 294
#define OP_NE 295
#define OP_ASSIGN 296
#define _DOUBLE_DOT 297
#define _UNSIGNED_INTEGER 298
#define _UNSIGNED_REAL 299
#define _STRING 300
#define _SQUARE_ALT_OPEN 301
#define _SQUARE_ALT_CLOSE 302
#define _IDENTIFIER 303
#define _FUNCTION_IDENTIFIER 304
#define _PROCEDURE_IDENTIFIER 305
#define _TYPE_IDENTIFIER 306
#define _CONSTANT_IDENTIFIER 307




#if !defined (YYSTYPE) && !defined (YYSTYPE_IS_DECLARED)
#line 105 "parser.y"
typedef union YYSTYPE {
    char* m_charString;
    Operator m_operator;
    double m_real;
    unsigned int m_unsignedInteger;
    int m_signedInteger;
	LabelVector* m_labelVector;
	Semantics::Const* m_const;
	IdentifierVector* m_identifierVector;
	Semantics::Type* m_type;
	Semantics::ForStatement::Direction m_forLoopDir;
	Semantics::Variable* m_variable;
	Semantics::ObjectAccess* m_objectAccess;
	Semantics::Statement* m_statement;
	Semantics::Expression* m_expression;
	Semantics::Object* m_object;
	Semantics::Typed* m_typed;
	StatementVector* m_statementVector;
	Semantics::StatementSequence* m_statementSequence;
	Directive m_directive;
	Semantics::Function* m_function;
	Semantics::Procedure* m_procedure;
	ActualParameterVector* m_actualParameterVector;
	Semantics::ActualParameter* m_actualParameter;
} YYSTYPE;
/* Line 1248 of yacc.c.  */
#line 166 "parser.yacc.hpp"
#define yystype YYSTYPE /* obsolescent; will be withdrawn */
#define YYSTYPE_IS_DECLARED 1
#define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



