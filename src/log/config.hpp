//--------------------------------------------------------------------
/// \file
/// \brief
/// Logging configuration
//--------------------------------------------------------------------

#ifndef OCT_LOG_CONFIG_HPP
#define OCT_LOG_CONFIG_HPP

//--------------------------------------------------------------------
// All these direcives are used to enable/disable logging.
//
// If the directive value is set to zero logging of that element
// is disabled.
//--------------------------------------------------------------------

/// General loging switch
#define LOG_ENABLED             1

//--------------------------------------------------------------------

/// Garbage collector logging
#define LOG_GARBAGECOLLECTOR    1

/// Semantics destructors logging
#define LOG_SEMDESTRUCTOR		1

//--------------------------------------------------------------------

#endif /* OCT_LOG_CONFIG_HPP */
