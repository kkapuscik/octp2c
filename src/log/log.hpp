//--------------------------------------------------------------------
/// \file
/// \brief
/// Logging support
//--------------------------------------------------------------------

#ifndef OCT_LOG_LOG_HPP
#define OCT_LOG_LOG_HPP

//--------------------------------------------------------------------

#include "config.hpp"

//--------------------------------------------------------------------

#if LOG_ENABLED

#   include <fstream>
	/// Log file definition
    extern std::ofstream logfile;

	/// Logging macro - part is a 'logging enabled' switch and data
	/// is a data to be logged
#   define LOG(part,data) if(part) { logfile << data << std::endl; }

#else /* LOG_ENABLED */

	/// Logging macro - do nothing
#   define LOG(part, data)

#endif  /* LOG_ENABLED */

//--------------------------------------------------------------------

#endif /* OCT_LOG_LOG_HPP */
