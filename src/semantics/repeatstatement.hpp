//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics REPEAT loop statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_REPEATSTATEMENT_HPP
#define OCT_SEMANTICS_REPEATSTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

namespace Semantics
{
	class Expression;
	class StatementSequence;
};

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// REPEAT loop statement class
	///
	class RepeatStatement : public Statement
	{
		DEFINE_RTTI(RepeatStatement,Statement)

	private:
		/// Loop condition expression
		const Expression* m_conditionEpression;
		/// Loop body statement sequence
		const StatementSequence* m_loopSequence;

	public:
		/// \brief
		/// Constuctor
		///
		/// Creates initialized statement object.
		///
		/// \param[in] condition - loop condition expression
		/// \param[in] sequence - loop body statement sequence 
		RepeatStatement(const Expression* condition,
			const StatementSequence* sequence);

		/// \brief
		/// Destructor
		///
		virtual ~RepeatStatement();

		/// \brief
		/// Get loop condition expression
		///
		/// \return
		/// Returns loop condition expression object.
		const Expression* getCondition() const;

		/// \brief
		/// Get loop body statement sequence
		///
		/// \return
		/// Returns loop body statement sequence object.
		const StatementSequence* getSequence() const;
	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_REPEATSTATEMENT_HPP */

