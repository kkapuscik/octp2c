//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics namespace class (implementation)
//--------------------------------------------------------------------

#include "namespace.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

//--------------------------------------------------------------------

Namespace::Namespace(const String& id, Namespace* outer)
    : Object(id)
    , m_outer(outer)
{
}

Namespace::~Namespace(void)
{
    /// delete all stored objects
    ObjectCollection::iterator iter;
    for(iter = m_objects.begin(); iter != m_objects.end(); ++iter)
        delete *iter;
}

//--------------------------------------------------------------------

bool Namespace::add(AutoPointer<Object>& object)
{
    assert(object.get());
    assert(object.get()->getId().length() > 0);

    if(add(object.get()))
    {
        object.take();
        return true;
    }
    else
        return false;
}

bool Namespace::add(Object* object)
{
    assert(object);

    // checking for repeated name
    if(m_objects.find(object) != m_objects.end())
        return false;

    // storing the object
    m_objects.insert(object);
	m_sortedObjects.push_back(object);

    return true;
}

//--------------------------------------------------------------------

Object* Namespace::find(const String& id, bool recursive)
{
    assert(id.length() > 0);

    // create temporary object (needed for IdComparator)
    Object tmp(id);

    // use temporary object to find
    return find(&tmp, recursive);
}

const Object* Namespace::find(const String& id, bool recursive) const
{
    assert(id.length() > 0);

    // create temporary object (needed for IdComparator)
    Object tmp(id);

    // use temporary object to find
    return find(&tmp, recursive);
}

Object* Namespace::find(Object* sem, bool recursive) const
{
    assert(sem);

    // search local objects collection
    ObjectCollection::const_iterator iter;
    iter = m_objects.find(sem);
    if(iter != m_objects.end())
        return *iter;

    // recursive search if specified
    if(recursive && m_outer)
        return m_outer->find(sem, recursive);
    else
        return NULL;
}

//--------------------------------------------------------------------

bool Namespace::writeContentsSES(SESStream& stream, int indent) const
{
    if(!Object::writeContentsSES(stream, indent))
        return false;

	// write objects in namespace
	writeIndentSES(stream, indent);
	stream << "<OBJECTS>" << endl;
    ObjectCollection::const_iterator iter;
    for(iter = m_objects.begin(); iter != m_objects.end(); ++iter)
        if(!((*iter)->writeSES(stream, indent+1)))
            return false;
	writeIndentSES(stream, indent);
	stream << "</OBJECTS>" << endl;

    return true;
}

//--------------------------------------------------------------------

Namespace* Namespace::getOuter(void)
{
    return m_outer;
}

//--------------------------------------------------------------------

const Namespace::SortedObjectCollection& Namespace::getSortedObjects() const
{
	return m_sortedObjects;
}

//--------------------------------------------------------------------

}
