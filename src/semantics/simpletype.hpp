//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics simple type class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_SIMPLETYPE_HPP
#define OCT_SEMANTICS_SIMPLETYPE_HPP

//--------------------------------------------------------------------

#include "type.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Simple type class
	///
	/// Simple type class describes all simple (general) types.
	class SimpleType : public Type
	{
		DEFINE_RTTI(SimpleType,Type)

	public:
		/// Simple types enumeration
		enum TypeId
		{
			/// Invalid type
			INVALID,
			/// 'Real' type
			REAL,
			/// 'Integer' type
			INTEGER,
			/// 'Boolean' type
			BOOLEAN,
			/// 'Char' type
			CHAR,
			/// 'String' type
			STRING
		};

	private:
		/// Type identifier
		TypeId m_typeId;

	public:
		/// \brief
		/// Constructor
		///
		/// Creates simple type basing on given type identifier.
		///
		/// \param[in] tid - SimpleType type identifier
		SimpleType(TypeId tid);
		
		/// \brief
		/// Destructor
		///
		virtual ~SimpleType(void);

		/// \brief
		/// Get type identifier
		///
		/// \return
		/// Returns object type identifier.
		TypeId getTypeId(void) const;

		/// \brief
		/// Get type name string
		///
		/// \return
		/// Returns string describing object simpletype type.
		const String& getTypeName(void) const;

	public:
		// SES support
		virtual bool writeSES(SESStream& stream, int indent) const;


	public:
		/// \brief
		/// Get type name string
		///
		/// \param[in] tid - type identificator
		///
		/// \return
		/// Returns string describing given type.
		static const String& getTypeName(TypeId tid);
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_SIMPLETYPE_HPP */
