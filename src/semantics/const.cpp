//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics constans objects base class (implementation)
//--------------------------------------------------------------------

#include "const.hpp"

namespace Semantics
{

	//--------------------------------------------------------------------

	Const::Const(const Type* type, const String& id)
		: Typed(type, id)
	{
	}

	Const::~Const()
	{
	}

	//--------------------------------------------------------------------

}
