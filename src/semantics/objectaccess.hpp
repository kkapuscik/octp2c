//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics object access class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_OBJECTACCESS_HPP
#define OCT_SEMANTICS_OBJECTACCESS_HPP

//--------------------------------------------------------------------

#include "typed.hpp"

namespace Semantics
{
	class Variable;
	class Function;
	class Const;
}

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Object access class
	///
	/// Object access class is used to describe access to some
	/// other object (ex. variable, constant) which could be simple
	/// (ex. entire variable) or more complicated (ex. record variable
	/// field access).
	class ObjectAccess : public Typed
	{
		DEFINE_RTTI(ObjectAccess,Typed)

	public:
		/// Access type
		enum Type
		{
			/// Access to constant
			CONSTANS,
			/// Access to entire variable
			ENTIRE_VARIABLE,
			/// Access to function return value
			FUNCTION_RETURN
		};

	private:
		/// Access type
		Type m_accessType;

		/// Accessed variable
		Variable* m_variable;
		/// Function for which value is returned
		Function* m_function;
		/// Accessed constant
		Const* m_const;

	public:
		/// \brief
		/// Constructor
		///
		/// This constructor creates 'entire variable' access object.
		///
		/// \param[in] variable - accessed variable
		ObjectAccess(Variable* variable);

		/// \brief
		/// Constructor
		///
		/// This constructor creates 'function return' access object.
		///
		/// \param[in] function - accessed variable
		ObjectAccess(Function* function);

		/// \brief
		/// Constructor
		///
		/// This constructor creates 'constans' access object
		///
		/// \param[in] constans - accessed constans object
		ObjectAccess(Const* constans);

		/// \brief
		/// Destructor
		///
		virtual ~ObjectAccess();

		/// \brief
		/// Get access type
		///
		Type getAccessType() const;

		/// \brief
		/// Get accessed object
		///
		Object* getAccessedObject() const;

		/// \brief
		/// Get accessed constans
		///
		Const* getConstans() const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_OBJECTACCESS_HPP */
