//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantic objects base class (implementation)
//--------------------------------------------------------------------

#include "object.hpp"

#include "log/log.hpp"


//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	Object::Object(const String& id)
		: m_id(id)
	{
	}

	Object::~Object(void)
	{
		LOG(LOG_SEMDESTRUCTOR, this << " ~Object");
	}

	//--------------------------------------------------------------------

	const String& Object::getId(void) const
	{
		return m_id;
	}

	void Object::setId(const String& id)
	{
		m_id = id;
	}

	//--------------------------------------------------------------------

	bool Object::writeSES(SESStream& stream, int indent) const
	{
		stream << getClassName() << "::writeSES not implemented" << endl;

		return true;
	}

	bool Object::writeContentsSES(SESStream& stream, int indent) const
	{
		if(m_id.empty() == false)
		{
			writeIndentSES(stream, indent);
			stream << "<ID>" << m_id << "</ID>" << endl;
		}
		return true;
	}

	bool Object::writeIndentSES(SESStream& stream, int indent) const
	{
		while(indent-- > 0)
			stream << "   ";

		return true;
	}

	//--------------------------------------------------------------------

}
