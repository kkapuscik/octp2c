//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics statement base class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_STATEMENT_HPP
#define OCT_SEMANTICS_STATEMENT_HPP

//--------------------------------------------------------------------

#include "object.hpp"
#include "labeltype.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Statement base class
	///
	class Statement : public Object
	{
		DEFINE_RTTI(Statement,Object)

	private:
		/// Label assigned to statement
		LabelType m_label;

	protected:
		/// \brief
		/// Constructor
		///
		/// Creates statement with no label assigned.
		Statement();

	public:
		/// \brief
		/// Destructor
		///
		virtual ~Statement();

		/// \brief
		/// Get label assigned to statement
		///
		/// \return
		/// Returns label assigned to statement or INVALID_LABEL_VALUE
		/// if there is no label assigned.
		LabelType getLabel() const;

		/// \brief
		/// Set label assigned to statement
		///
		/// \param[in] label - label to assign
		///
		/// \remarks
		/// Label could be assigned only once.
		void setLabel(LabelType label);
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_STATEMENT_HPP */

