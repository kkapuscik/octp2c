//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics simple types const class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_SIMPLE_CONST_HPP
#define OCT_SEMANTICS_SIMPLE_CONST_HPP

//--------------------------------------------------------------------

#include "const.hpp"

namespace Semantics
{
	class SimpleType;
}

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Simple type constans class
	///
	/// Objects of this class are used to create constans of simple
	/// type.
	class SimpleConst : public Const
	{
		DEFINE_RTTI(SimpleConst,Const)

	private:
		/// STRING value
		String m_valueString;
		/// REAL value
		double m_valueReal;
		/// INTEGER value
		int m_valueInteger;
		/// BOOLEAN value
		bool m_valueBoolean;
		/// CHAR value
		char m_valueChar;

	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] value - constant value (STRING)
		/// \param[in] id - object identificator (default: empty)
		SimpleConst(const String& value, const String& id = String());

		/// \brief
		/// Constructor
		///
		/// \param[in] value - constant value (REAL)
		/// \param[in] id - object identificator (default: empty)
		SimpleConst(double value, const String& id = String());

		/// \brief
		/// Constructor
		///
		/// \param[in] value - constant value (INTEGER)
		/// \param[in] id - object identificator (default: empty)
		SimpleConst(int value, const String& id = String());

		/// \brief
		/// Constructor
		///
		/// \param[in] value - constant value (BOOLEAN)
		/// \param[in] id - object identificator (default: empty)
		SimpleConst(bool value, const String& id = String());

		/// \brief
		/// Constructor
		///
		/// \param[in] value - constant value (CHAR)
		/// \param[in] id - object identificator (default: empty)
		SimpleConst(char value, const String& id = String());

		/// \brief
		/// Destructor
		///
		virtual ~SimpleConst();


		/// \brief
		/// Get constant value (STRING)
		///
		/// \param[out] result - constant value holded by this object
		void getValue(String& result) const;

		/// \brief
		/// Get constant value (INTEGER)
		///
		/// \param[out] result - constant value holded by this object
		void getValue(int& result) const;

		/// \brief
		/// Get constant value (REAL)
		///
		/// \param[out] result - constant value holded by this object
		void getValue(double& result) const;

		/// \brief
		/// Get constant value (BOOLEAN)
		///
		/// \param[out] result - constant value holded by this object
		void getValue(bool& result) const;

		/// \brief
		/// Get constant value (CHAR)
		///
		/// \param[out] result - constant value holded by this object
		void getValue(char& result) const;


		/// \brief
		/// Set constant value (STRING)
		///
		/// \param[in] value - new constant value
		///
		/// \remarks
		/// Use this function carefully. The objects that are managed by
		/// other objects (ex. namespaces) should not be modified outside
		/// manager in ANY way, so the value should not be changed too.
		void setValue(const String& value);

		/// \brief
		/// Set constant value (REAL)
		///
		/// \param[in] value - new constant value
		///
		/// \remarks
		/// Use this function carefully. The objects that are managed by
		/// other objects (ex. namespaces) should not be modified outside
		/// manager in ANY way, so the value should not be changed too.
		void setValue(double value);

		/// \brief
		/// Set constant value (INTEGER)
		///
		/// \param[in] value - new constant value
		///
		/// \remarks
		/// Use this function carefully. The objects that are managed by
		/// other objects (ex. namespaces) should not be modified outside
		/// manager in ANY way, so the value should not be changed too.
		void setValue(int value);

		/// \brief
		/// Set constant value (BOOLEAN)
		///
		/// \param[in] value - new constant value
		///
		/// \remarks
		/// Use this function carefully. The objects that are managed by
		/// other objects (ex. namespaces) should not be modified outside
		/// manager in ANY way, so the value should not be changed too.
		void setValue(bool value);

		/// \brief
		/// Set constant value (CHAR)
		///
		/// \param[in] value - new constant value
		///
		/// \remarks
		/// Use this function carefully. The objects that are managed by
		/// other objects (ex. namespaces) should not be modified outside
		/// manager in ANY way, so the value should not be changed too.
		void setValue(char value);

		/// \brief
		/// Get simple type
		///
		/// \return
		/// Get type object safe-casted to simple type.
		const SimpleType* getSimpleType() const;

	public:
		// SES support
		bool writeSES(SESStream& stream, int indent) const;
		bool writeContentsSES(SESStream& stream, int indent) const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_SIMPLE_CONST_HPP */
