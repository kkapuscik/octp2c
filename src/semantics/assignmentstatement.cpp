//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics assignment statement class (implementation)
//--------------------------------------------------------------------

#include "assignmentstatement.hpp"

#include "typed.hpp"
#include "simpletype.hpp"
#include "expression.hpp"
#include "objectaccess.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

//--------------------------------------------------------------------

AssignmentStatement::AssignmentStatement(const ObjectAccess* resultvariable,
										 const Expression* valueexpression)
	: m_resultVariable(resultvariable)
	, m_valueExpression(valueexpression)
{
}

AssignmentStatement::~AssignmentStatement()
{
	delete m_resultVariable;
	delete m_valueExpression;
}

//--------------------------------------------------------------------

const ObjectAccess* AssignmentStatement::getResultVariable() const
{
	return m_resultVariable;
}

const Expression* AssignmentStatement::getValueExpression() const
{
	return m_valueExpression;
}

//--------------------------------------------------------------------

bool AssignmentStatement::checkTypes(const Typed* result, const Typed* value)
{
	/// \todo Fix in future
	return true;

	if(result->getType()->isOfClass(clid_SimpleType) &&
		value->getType()->isOfClass(clid_SimpleType))
	{
		const SimpleType* resulttype = cast<SimpleType>(result->getType());
		const SimpleType* valuetype = cast<SimpleType>(value->getType());

		return resulttype->getTypeId() == valuetype->getTypeId();
	}
	else
		return false;
}

//--------------------------------------------------------------------

}
