//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics procedure class (implementation)
//--------------------------------------------------------------------

#include "procedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	Procedure::Procedure(const String& id, Namespace* outer)
		: NamedBlock(id, outer)
	{
	}

	Procedure::~Procedure()
	{
	}

}

