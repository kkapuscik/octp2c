//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics write / writeln procedure class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_WRITEPROCEDURE_HPP
#define OCT_SEMANTICS_WRITEPROCEDURE_HPP

//--------------------------------------------------------------------

#include "procedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Procedure class
	///
    class WriteProcedure : public Procedure
    {
        DEFINE_RTTI(WriteProcedure, Procedure)

	private:
		/// Write / Writeln procedure version flag
		bool m_writeline;

    public:
        /// \brief
        /// Constructor
        ///
        /// \param id - program identifier
        /// \param outer - outer (higher level) namespace
        ///
        /// \todo Should be 'outer' Program* ?
        WriteProcedure(bool writeline, const String& id, Namespace* outer);

        /// \brief
        /// Destructor
        ///
        virtual ~WriteProcedure(void);

		/// \brief
		/// Get procedure version
		///
		/// Check if procedure is a Writeln (true) or Write (false) version.
		///
		/// \return
		/// 'Line' version flag value is returned.
		bool getLineFlag() const;
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_WRITEPROCEDURE_HPP */
