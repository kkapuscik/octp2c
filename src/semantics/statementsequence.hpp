//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics statement sequence class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_STATEMENTSEQUENCE_HPP
#define OCT_SEMANTICS_STATEMENTSEQUENCE_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

#include <vector>

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Statement sequence class
	///
	/// Statement sequence class represent ordered sequence of 
	/// statements - a list of instructions to execute.
	class StatementSequence : public Statement
	{
		DEFINE_RTTI(StatementSequence,Statement)

	public:
		/// Sequence collection type
		typedef std::vector<const Statement*> SequenceCollection;

	private:
		/// Statements collection
		SequenceCollection m_sequence;

	public:
		/// \brief
		/// Constructor
		///
		/// Creates empty statement sequence.
		StatementSequence();

		/// \brief
		/// Destructor
		///
		virtual ~StatementSequence();

		/// \brief
		/// Add statement to sequence
		///
		/// \param[in] statement - statement to add
		void addStatement(Statement* statement);

		/// \brief
		/// Get sequence of statements
		///
		/// \return
		/// Returns statements currently in sequence.
		const SequenceCollection& getSequence() const;
	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_STATEMENTSEQUENCE_HPP */

