//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics WHILE loop statement class (implementation)
//--------------------------------------------------------------------

#include "whilestatement.hpp"

#include "expression.hpp"

//--------------------------------------------------------------------

namespace Semantics
{
	
	WhileStatement::WhileStatement(const Expression* condition, const Statement* loopstatement)
		: Statement()
		, m_conditionEpression(condition)
		, m_loopStatement(loopstatement)
	{
	}
	
	WhileStatement::~WhileStatement()
	{
		delete m_conditionEpression;
		delete m_loopStatement;
	}

	//--------------------------------------------------------------------

	const Expression* WhileStatement::getCondition() const
	{
		return m_conditionEpression;
	}

	const Statement* WhileStatement::getLoopStatement() const
	{
		return m_loopStatement;
	}

}
