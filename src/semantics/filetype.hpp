//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics file type class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_FILETYPE_HPP
#define OCT_SEMANTICS_FILETYPE_HPP

//--------------------------------------------------------------------

#include "type.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// File type class
	///
	class FileType : public Type
	{
		DEFINE_RTTI(FileType,Type)
	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] id - type name (identifier)
		FileType(const String& id);
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_FILETYPE_HPP */
