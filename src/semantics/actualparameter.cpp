#include "actualparameter.hpp"

#include "objectaccess.hpp"
#include "expression.hpp"
#include "function.hpp"
#include "procedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	ActualParameter::ActualParameter(ObjectAccess* variableaccess)
		: Typed(m_variableAccess->getType())
		, m_parameterType(VARIABLE_ACCESS)
		, m_variableAccess(variableaccess)
		, m_procedure(NULL)
		, m_function(NULL)
		, m_expression(NULL)
	{
	}

	ActualParameter::ActualParameter(Procedure* procedure)
		: Typed(NULL)
		, m_parameterType(PROCEDURE)
		, m_variableAccess(NULL)
		, m_procedure(procedure)
		, m_function(NULL)
		, m_expression(NULL)
	{
		/// \todo Set type
	}

	ActualParameter::ActualParameter(Function* function)
		: Typed(NULL)
		, m_parameterType(FUNCTION)
		, m_variableAccess(NULL)
		, m_procedure(NULL)
		, m_function(function)
		, m_expression(NULL)
	{
		/// \todo Set type
	}

	ActualParameter::ActualParameter(Expression* expr)
		: Typed(expr->getType())
		, m_parameterType(EXPRESSION)
		, m_variableAccess(NULL)
		, m_procedure(NULL)
		, m_function(NULL)
		, m_expression(expr)
	{
	}

	ActualParameter::~ActualParameter()
	{
//		delete m_procedure;
//		delete m_function;
		delete m_variableAccess;
		delete m_expression;
	}

	//--------------------------------------------------------------------

	ActualParameter::Type ActualParameter::getParameterType() const
	{
		return m_parameterType; 
	}

	Expression* ActualParameter::getExpression() const
	{
		assert(m_parameterType == EXPRESSION);

		return m_expression;
	}


}

//--------------------------------------------------------------------
