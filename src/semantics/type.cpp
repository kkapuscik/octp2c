//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics type base class (implementation)
//--------------------------------------------------------------------

#include "type.hpp"

namespace Semantics
{

Type::Type(const String& id)
    : Object(id)
{
}

Type::~Type(void)
{
}

}
