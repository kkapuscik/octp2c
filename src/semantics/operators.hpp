//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics operators definitions
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_OPERATORS_HPP
#define OCT_SEMANTICS_OPERATORS_HPP

//--------------------------------------------------------------------

/// Operators enumeration
enum Operator
{
    /// No-operation operator
    O_NOOP,

    /// Lower-than comparision operator
    O_LT,
    /// Lower-or-equal comparision operator
    O_LE,
    /// Greater-or-equal comparision operator
    O_GE,
    /// Greater-than comparision operator
    O_GT,
    /// Not-equal comparision operator
    O_NE,
    /// Equal comparision operator
    O_EQ,
    
    /// Minus arithmetic operator
    O_MINUS,
    /// Plus arithmetic operator
    O_PLUS,
    /// Multiplication arithmetic operator
    O_MUL,
    /// Division arithmetic operator
    O_DIV,
    /// "Real type result" division arithmetic operator
    O_REALDIV,
    /// Modulo arithmetic operator
    O_MOD,

    /// IN relation operator
    O_IN,

    /// OR boolean operator (also disjunction)
    O_OR,
    /// AND boolean operator (also conjunction)
    O_AND,
    /// NOT boolean operator (also negation)
    O_NOT
};

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_OPERATORS_HPP */
