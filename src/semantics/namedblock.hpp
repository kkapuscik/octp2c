//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics named block class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_NAMEDBLOCK_HPP
#define OCT_SEMANTICS_NAMEDBLOCK_HPP

//--------------------------------------------------------------------

#include "namespace.hpp"
#include "parser/typedefs.hpp"

#include <set>

namespace Semantics
{
	class StatementSequence;
}

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Named block class
	///
    class NamedBlock : public Namespace
    {
        DEFINE_RTTI(NamedBlock,Namespace)

	public:
		/// Declaration status enumerated type
		enum DeclarationStatus
		{
			/// Invalid status
			STATUS_INVALID,
			/// Forwarded status
			STATUS_FORWARD,
			/// Declared status
			STATUS_DECLARED
		};

	private:
		/// Labels collection type
		typedef std::set<LabelType> LabelCollection;

		/// Set of defined labels
		LabelCollection m_labels;

		/// Statement sequence in block
		StatementSequence* m_statementSequence;

		/// Declaration status
		DeclarationStatus m_declarationStatus;

    public:
        /// \brief
        /// Constructor
        ///
        /// \param id - block identifier
        /// \param outer - outer (higher level) namespace
        NamedBlock(const String& id, Namespace* outer);

        /// \brief
        /// Destructor
        ///
        virtual ~NamedBlock(void);


		/// \brief
		/// Add label
		///
		/// \param[in] label - numerical value of label to add
		///
		/// \return
		/// \c True if value was successfully added or \c false otherwise.
		bool addLabel(unsigned int label);

		/// \brief
		/// Check if label exists
		///
		/// \param[in] label - numerical value of label to check for
		///
		/// \return
		/// \c True is returned if label exists or false otherwise.
		bool existLabel(unsigned int label) const;

		/// \brief
		/// Set statement sequence
		///
		/// \param[in] sequence - statement sequence to set
		void setStatementSequence(StatementSequence* sequence);

		/// \brief
		/// Get statement sequence
		///
		/// \return
		/// Returns block statement sequence or NULL if there is no sequence.
		StatementSequence* getStatementSequence() const;

		/// \brief
		/// Get declaration status
		///
		/// \return
		/// Returns current declaration status.
		DeclarationStatus getDeclarationStatus() const;

		/// \brief
		/// Set declaration status
		///
		/// \param[in] status - new declaration status
		void setDeclarationStatus(DeclarationStatus status);

    public:
        // SES output support
		virtual bool writeContentsSES(SESStream& stream, int indent) const;
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_NAMEDBLOCK_HPP */
