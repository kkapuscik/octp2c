//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics type base class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_TYPE_HPP
#define OCT_SEMANTICS_TYPE_HPP

//--------------------------------------------------------------------

#include "object.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Type base class
	class Type : public Object
	{
		DEFINE_RTTI(Type,Object)

	public:
		/// \brief
		/// Constructor
		///
		/// Creates named type.
		///
		/// \param[in] id - type name (identifier)
		Type(const String& id);

		/// \brief
		/// Destructor
		///
		virtual ~Type(void);
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_TYPE_HPP */
