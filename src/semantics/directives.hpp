//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics directives definitions
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_DIRECTIVES_HPP
#define OCT_SEMANTICS_DIRECTIVES_HPP

//--------------------------------------------------------------------

/// Directives enumeration
enum Directive
{
	/// Unknown directive
	DIRECTIVE_UNKNOWN,
	/// FORWARD directive
	DIRECTIVE_FORWARD
};

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_DIRECTIVES_HPP */
