//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics record type class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_RECORDTYPE_HPP
#define OCT_SEMANTICS_RECORDTYPE_HPP

//--------------------------------------------------------------------

#include "type.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Record type class
	///
	class RecordType : public Type
	{
		DEFINE_RTTI(RecordType,Type)

	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] id - type name (identifier)
		RecordType(const String& id);
	};
}

//--------------------------------------------------------------------

#endif
