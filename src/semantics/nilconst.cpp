//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics NIL constans class (implementation)
//--------------------------------------------------------------------

#include "nilconst.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	NilConst::NilConst()
		: Const(NULL)
	{
		/// \todo Set type?
	}

	NilConst::~NilConst()
	{
	}

}
