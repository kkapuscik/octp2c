#include "procedurestatement.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	ProcedureStatement::ProcedureStatement(Procedure* procedure)
		: Statement()
		, m_procedure(procedure)
	{
		assert(procedure);
	}

	ProcedureStatement::~ProcedureStatement()
	{
		for(int i=getParameterCount()-1; i>=0; --i)
			delete getParameter(i);
	}

	//--------------------------------------------------------------------

	void ProcedureStatement::addParameter(ActualParameter* parameter)
	{
		m_parameters.push_back(parameter);
	}

	//--------------------------------------------------------------------

	Procedure* ProcedureStatement::getProcedure() const
	{
		return m_procedure;
	}

	int ProcedureStatement::getParameterCount() const
	{
		return (int)m_parameters.size();
	}

	const ActualParameter* ProcedureStatement::getParameter(int pos) const
	{
		assert(pos >=0 && pos < getParameterCount());

		return m_parameters[pos];
	}

}
