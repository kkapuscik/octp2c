//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics constans objects base class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_CONST_HPP
#define OCT_SEMANTICS_CONST_HPP

//--------------------------------------------------------------------

#include "typed.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Base constans object
	///
	class Const : public Typed
	{
		DEFINE_RTTI(Const,Typed)

	protected:
		/// \brief
		/// Constructor
		///
		/// \param[in] type - semantic object type
		/// \param[in] id - object identificator (default: empty)
		///
		/// \remarks
		/// The 'type' object given is managed by constructed object,
		/// so it MUST NOT be fried or modified outside after the
		/// construction is finished.
		Const(const Type* type, const String& id = String());

	public:
		/// \brief
		/// Destructor
		///
		virtual ~Const();

	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_CONST_HPP */
