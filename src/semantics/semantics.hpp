//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics namespace info
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_HPP
#define OCT_SEMANTICS_HPP

//--------------------------------------------------------------------

/// \brief
/// Semantics elements namespace
namespace Semantics
{
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_HPP */
