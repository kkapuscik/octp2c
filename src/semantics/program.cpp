//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics program class (implementation)
//--------------------------------------------------------------------

#include "program.hpp"

namespace Semantics
{

//--------------------------------------------------------------------

Program::Program(const String& id, Global* outer)
    : NamedBlock(id, outer)
{
    assert(outer);
}

Program::~Program(void)
{
}

//--------------------------------------------------------------------

bool Program::writeSES(SESStream& stream, int indent) const
{
    if(!isOfClass(clid_Program))
        assert(0);

    // write begin tag
    writeIndentSES(stream, indent);
    stream << "<PROGRAM " << getId() << ">" << endl;

    // write contents
    if(!writeContentsSES(stream, indent+1))
        return false;

    // write end tag
    writeIndentSES(stream, indent);
    stream << "</PROGRAM>" << endl;

    return true;
}

//--------------------------------------------------------------------

}
