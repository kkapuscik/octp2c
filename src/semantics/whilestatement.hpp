//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics WHILE loop statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_WHILESTATEMENT_HPP
#define OCT_SEMANTICS_WHILESTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

namespace Semantics
{
	class Expression;
};

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// WHILE loop statement class
	///
	class WhileStatement : public Statement
	{
		DEFINE_RTTI(WhileStatement,Statement)

	private:
		/// Loop condition expression
		const Expression* m_conditionEpression;
		/// Loop body statement
		const Statement* m_loopStatement;

	public:
		/// \brief
		/// Constructor
		///
		/// Creates initialized statement object
		///
		/// \param[in] condition - loop condition object
		/// \param[in] loopstatement - loop body statement
		WhileStatement(const Expression* condition, const Statement* loopstatement);

		/// \brief
		/// Destructor
		///
		virtual ~WhileStatement();

		/// \brief
		/// Get loop condition
		///
		/// \return
		/// Loop condition object is returned.
		const Expression* getCondition() const;

		/// \brief
		/// Get loop body statement
		///
		/// \return
		/// Loop body statement object is returned.
		const Statement* getLoopStatement() const;
	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_WHILESTATEMENT_HPP */

