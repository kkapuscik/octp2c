//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics variable class (implementation)
//--------------------------------------------------------------------

#include "variable.hpp"

namespace Semantics
{

//--------------------------------------------------------------------

Variable::Variable(const Type* type, const String& id)
    : Typed(type, id)
{
}

Variable::~Variable(void)
{
}

//--------------------------------------------------------------------

bool Variable::writeSES(SESStream& stream, int indent) const
{
    if(!isOfClass(clid_Variable))
        assert(0);
    
    // write begin tag
    writeIndentSES(stream, indent);
    stream << "<VARIABLE>" << endl;

    // write contents
    if(!writeContentsSES(stream, indent+1))
        return false;

    // write end tag
    writeIndentSES(stream, indent);
    stream << "</VARIABLE>" << endl;

    return true;
}

//--------------------------------------------------------------------

}
