//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics array type class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_ARRAYTYPE_HPP
#define OCT_SEMANTICS_ARRAYTYPE_HPP

//--------------------------------------------------------------------

#include "type.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Array type class
	///
	/// \remarks
	/// NOT IMPLEMENTED NOW!
	class ArrayType : public Type
	{
		DEFINE_RTTI(ArrayType,Type)
		
	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] id - type name (identifier)
		ArrayType(const String& id);
	};

}

//--------------------------------------------------------------------

#endif
