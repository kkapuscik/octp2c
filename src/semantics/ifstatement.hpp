//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics IF statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_IFSTATEMENT_HPP
#define OCT_SEMANTICS_IFSTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

namespace Semantics
{
	class Expression;
};

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// IF conditional statement class
	///
	class IfStatement : public Statement
	{
		DEFINE_RTTI(IfStatement,Statement)

	private:
		/// IF condition expression
		const Expression* m_conditionEpression;
		/// IF part statement
		const Statement* m_ifStatement;
		/// ELSE part statement
		const Statement* m_elseStatement;

	public:
		/// \brief
		/// Constructor
		///
		/// Create statement with only IF part.
		///
		/// \param[in] condition - condition expression
		/// \param[in] ifstatement - IF part statement
		IfStatement(const Expression* condition, const Statement* ifstatement);

		/// \brief
		/// Constructor
		///
		/// Create statement with IF and ELSE parts.
		///
		/// \param[in] condition - condition expression
		/// \param[in] ifstatement - IF part statement
		/// \param[in] elsestatement - ELSE part statement
		IfStatement(const Expression* condition, const Statement* ifstatement,
			const Statement* elsestatement);

		/// \brief
		/// Destructor
		///
		virtual ~IfStatement();

		/// \brief
		/// Get condition expression
		///
		/// \return
		/// Returns condition expression object.
		const Expression* getCondition() const;

		/// \brief
		/// Get IF part statement
		///
		/// \return
		/// Returns IF part statement object.
		const Statement* getIfStatement() const;

		/// \brief
		/// Get ELSE part statement
		///
		/// \return
		/// Returns ELSE part statement object or NULL if there is no ELSE part.
		const Statement* getElseStatement() const;
	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_IFSTATEMENT_HPP */

