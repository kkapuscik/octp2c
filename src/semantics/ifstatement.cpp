//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics IF statement class (implementation)
//--------------------------------------------------------------------

#include "ifstatement.hpp"

#include "expression.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	IfStatement::IfStatement(const Expression* condition, const Statement* ifstatement)
		: m_conditionEpression(condition)
		, m_ifStatement(ifstatement)
		, m_elseStatement(NULL)
	{
		assert(condition);
		assert(ifstatement);
	}

	IfStatement::IfStatement(const Expression* condition, const Statement* ifstatement,
		const Statement* elsestatement)
		: m_conditionEpression(condition)
		, m_ifStatement(ifstatement)
		, m_elseStatement(elsestatement)
	{
		assert(condition);
		assert(ifstatement);
		assert(elsestatement);
	}
	
	IfStatement::~IfStatement()
	{
		delete m_conditionEpression;
		delete m_ifStatement;
		delete m_elseStatement;
	}

	//--------------------------------------------------------------------

	const Expression* IfStatement::getCondition() const
	{
		return m_conditionEpression;
	}

	const Statement* IfStatement::getIfStatement() const
	{
		return m_ifStatement;
	}

	const Statement* IfStatement::getElseStatement() const
	{
		return m_elseStatement;
	}

	//--------------------------------------------------------------------

}

