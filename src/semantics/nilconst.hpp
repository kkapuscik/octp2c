//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics NIL constans class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_NILCONST_HPP
#define OCT_SEMANTICS_NILCONST_HPP

//--------------------------------------------------------------------

#include "const.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// NIL const class
	///
	/// Object of this class represents the NIL value in semantics
	/// structure.
	class NilConst : public Const
	{
		DEFINE_RTTI(NilConst,Const)

	public:
		/// \brief
		/// Constructor
		///
		NilConst();

		/// \brief
		/// Destructor
		///
		virtual ~NilConst();
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_NILCONST_HPP */
