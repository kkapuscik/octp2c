//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics simple types const class (implementation)
//--------------------------------------------------------------------

#include "simpleconst.hpp"

#include "simpletype.hpp"
#include "main/globals.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	SimpleConst::SimpleConst(const String& value, const String& id)
		: Const(g_semgen.getSimpleType(SimpleType::INVALID), id)
	{
		setValue(value);
	}

	SimpleConst::SimpleConst(double value, const String& id)
		: Const(g_semgen.getSimpleType(SimpleType::INVALID), id)
	{
		setValue(value);
	}

	SimpleConst::SimpleConst(int value, const String& id)
		: Const(g_semgen.getSimpleType(SimpleType::INVALID), id)
	{
		setValue(value);
	}

	SimpleConst::SimpleConst(bool value, const String& id)
		: Const(g_semgen.getSimpleType(SimpleType::INVALID), id)
	{
		setValue(value);
	}

	SimpleConst::SimpleConst(char value, const String& id)
		: Const(g_semgen.getSimpleType(SimpleType::INVALID), id)
	{
		setValue(value);
	}

	SimpleConst::~SimpleConst()
	{
	}

	//--------------------------------------------------------------------

	void SimpleConst::getValue(String& result) const
	{
		assert(((SimpleType*)getType())->getTypeId() == SimpleType::STRING);
		result = m_valueString;
	}

	void SimpleConst::getValue(double& result) const
	{
		assert(((SimpleType*)getType())->getTypeId() == SimpleType::REAL);
		result = m_valueReal;
	}

	void SimpleConst::getValue(int& result) const
	{
		assert(((SimpleType*)getType())->getTypeId() == SimpleType::INTEGER);
		result = m_valueInteger;
	}

	void SimpleConst::getValue(bool& result) const
	{
		assert(((SimpleType*)getType())->getTypeId() == SimpleType::BOOLEAN);
		result = m_valueBoolean;
	}

	void SimpleConst::getValue(char& result) const
	{
		assert(((SimpleType*)getType())->getTypeId() == SimpleType::CHAR);
		result = m_valueChar;
	}

	//--------------------------------------------------------------------

	void SimpleConst::setValue(const String& value)
	{
		setType(g_semgen.getSimpleType(SimpleType::STRING));
		m_valueString = value;
	}

	void SimpleConst::setValue(double value)
	{
		setType(g_semgen.getSimpleType(SimpleType::REAL));
		m_valueReal = value;
	}

	void SimpleConst::setValue(int value)
	{
		setType(g_semgen.getSimpleType(SimpleType::INTEGER));
		m_valueInteger = value;
	}

	void SimpleConst::setValue(bool value)
	{
		setType(g_semgen.getSimpleType(SimpleType::BOOLEAN));
		m_valueBoolean = value;
	}

	void SimpleConst::setValue(char value)
	{
		setType(g_semgen.getSimpleType(SimpleType::CHAR));
		m_valueChar = value;
	}

	//--------------------------------------------------------------------

	bool SimpleConst::writeSES(SESStream& stream, int indent) const
	{
		if(!isOfClass(clid_SimpleConst))
			assert(0);
	    
		// write begin tag
		writeIndentSES(stream, indent);
		stream << "<SIMPLECONST>" << endl;

		// write contents
		if(!writeContentsSES(stream, indent+1))
			return false;

		// write end tag
		writeIndentSES(stream, indent);
		stream << "</SIMPLECONST>" << endl;

		return true;
	}

	bool SimpleConst::writeContentsSES(SESStream& stream, int indent) const
	{
		if(!Const::writeContentsSES(stream, indent))
			return false;

		const SimpleType* type = cast<SimpleType>(getType());
		assert(type);

		// write value
		writeIndentSES(stream, indent);
		stream << "<VALUE>";
		switch(type->getTypeId())
		{
		case SimpleType::STRING:
			stream << m_valueString;
			break;
		case SimpleType::REAL:
			stream << m_valueReal;
			break;
		case SimpleType::INTEGER:
			stream << m_valueInteger;
			break;
		case SimpleType::BOOLEAN:
			stream << m_valueBoolean;
			break;
		case SimpleType::CHAR:
			stream << m_valueChar;
			break;
		default:
			assert(0);
		}
		stream << "</VALUE>" << endl;

		return true;
	}

	//--------------------------------------------------------------------

	const SimpleType* SimpleConst::getSimpleType() const
	{
		const SimpleType* simple = cast<SimpleType>(getType());
		assert(simple);
		return simple;
	}

	//--------------------------------------------------------------------

}
