//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics named block class (implementation)
//--------------------------------------------------------------------

#include "namedblock.hpp"
#include "statementsequence.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	NamedBlock::NamedBlock(const String& id, Namespace* outer)
		: Namespace(id, outer)
		, m_statementSequence(NULL)
		, m_declarationStatus(STATUS_INVALID)
	{
	}

	NamedBlock::~NamedBlock(void)
	{
		delete m_statementSequence;
	}

	//--------------------------------------------------------------------

	bool NamedBlock::addLabel(unsigned int label)
	{
		assert(label >= 0 && label <= 9999);

		if(!existLabel(label))
		{
			m_labels.insert(label);
			return true;
		}
		else
			return false;
	}

	bool NamedBlock::existLabel(unsigned int label) const
	{
		return m_labels.find(label) != m_labels.end();
	}

	//--------------------------------------------------------------------

	bool NamedBlock::writeContentsSES(SESStream& stream, int indent) const
	{
		if(!Namespace::writeContentsSES(stream, indent))
			return false;

		writeIndentSES(stream, indent);
		stream << "<LABELS>" << endl;

		LabelCollection::const_iterator iter;
		for(iter = m_labels.begin(); iter != m_labels.end(); ++iter)
		{
			writeIndentSES(stream, indent+1);
			stream << "<LABEL>";
			if(!(stream << *iter))
				return false;
			stream << "</LABEL>" << endl;
		}

		writeIndentSES(stream, indent);
		stream << "</LABELS>" << endl;
	    
		return true;
	}

	//--------------------------------------------------------------------

	void NamedBlock::setStatementSequence(StatementSequence* sequence)
	{
		assert(m_statementSequence == NULL);

		m_statementSequence = sequence;
	}

	StatementSequence* NamedBlock::getStatementSequence() const
	{
		return m_statementSequence;
	}

	//--------------------------------------------------------------------

	NamedBlock::DeclarationStatus NamedBlock::getDeclarationStatus() const
	{
		return m_declarationStatus;
	}

	void NamedBlock::setDeclarationStatus(DeclarationStatus status)
	{
		m_declarationStatus = status;
	}

	//--------------------------------------------------------------------

}
