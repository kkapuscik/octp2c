//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics procedure class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_PROCEDURE_HPP
#define OCT_SEMANTICS_PROCEDURE_HPP

//--------------------------------------------------------------------

#include "namedblock.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Procedure class
	///
    class Procedure : public NamedBlock
    {
        DEFINE_RTTI(Procedure,NamedBlock)

    public:
        /// \brief
        /// Constructor
        ///
        /// \param id - program identifier
        /// \param outer - outer (higher level) namespace
        ///
        /// \todo Should be 'outer' Program* ?
        Procedure(const String& id, Namespace* outer);

        /// \brief
        /// Destructor
        ///
        virtual ~Procedure(void);
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_PROCEDURE_HPP */
