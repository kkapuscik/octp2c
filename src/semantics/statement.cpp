//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics statement base class (implementation)
//--------------------------------------------------------------------

#include "statement.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	Statement::Statement()
		: Object()
		, m_label(INVALID_LABEL_VALUE)
	{
	}

	Statement::~Statement()
	{
	}

	//--------------------------------------------------------------------

	LabelType Statement::getLabel() const
	{
		return m_label;
	}

	void Statement::setLabel(LabelType label)
	{
		assert(label != INVALID_LABEL_VALUE);
		assert(m_label == INVALID_LABEL_VALUE);

		m_label = label;
	}

	//--------------------------------------------------------------------

}
