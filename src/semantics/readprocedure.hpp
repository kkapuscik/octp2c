//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics read / readln procedure class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_READPROCEDURE_HPP
#define OCT_SEMANTICS_READPROCEDURE_HPP

//--------------------------------------------------------------------

#include "procedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Procedure class
	///
    class ReadProcedure : public Procedure
    {
        DEFINE_RTTI(ReadProcedure, Procedure)

	private:
		/// Read / Readln procedure version flag
		bool m_readline;

    public:
        /// \brief
        /// Constructor
        ///
        /// \param id - program identifier
        /// \param outer - outer (higher level) namespace
        ///
        /// \todo Should be 'outer' Program* ?
        ReadProcedure(bool readline, const String& id, Namespace* outer);

        /// \brief
        /// Destructor
        ///
        virtual ~ReadProcedure(void);

		/// \brief
		/// Get procedure version
		///
		/// Check if procedure is a Readln (true) or Read (false) version.
		///
		/// \return
		/// 'Line' version flag value is returned.
		bool getLineFlag() const;
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_READPROCEDURE_HPP */
