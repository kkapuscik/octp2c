//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics function class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_FUNCTION_HPP
#define OCT_SEMANTICS_FUNCTION_HPP

//--------------------------------------------------------------------

#include "procedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Function class
	///
    class Function : public Procedure
    {
        DEFINE_RTTI(Function,Procedure)
    
    public:
        /// \brief
        /// Constructor
        ///
        /// \param id - program identifier
        /// \param outer - outer (higher level) namespace
        ///
        /// \todo Should be 'outer' Program* ?
        Function(const String& id, Namespace* outer);

        /// \brief
        /// Destructor
        ///
        virtual ~Function(void);
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_FUNCTION_HPP */
