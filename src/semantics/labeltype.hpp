//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics label type definition
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_LABELTYPE_HPP
#define OCT_SEMANTICS_LABELTYPE_HPP

//--------------------------------------------------------------------

/// Label type (label number)
typedef unsigned int LabelType;

/// Invalid label value (number)
const LabelType INVALID_LABEL_VALUE = (LabelType)-1;

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_LABELTYPE_HPP */

