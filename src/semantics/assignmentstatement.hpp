//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics assignment statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_ASSIGNMENTSTATEMENT_HPP
#define OCT_SEMANTICS_ASSIGNMENTSTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

namespace Semantics
{
	class Expression;
	class ObjectAccess;
	class Typed;
};

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Assignment statement class
	///
	class AssignmentStatement : public Statement
	{
		DEFINE_RTTI(AssignmentStatement,Statement)

	private:
		/// Result variable (variable access)
		const ObjectAccess* m_resultVariable;
		/// Assigned value expression
		const Expression* m_valueExpression;

	public:
		/// \brief
		/// Constructor
		///
		/// Creates initialized statement object.
		///
		/// \param[in] resultvariable - result variable access object
		/// \param[in] valueexpression - assigned value expression object
		AssignmentStatement(const ObjectAccess* resultvariable,
			const Expression* valueexpression);

		/// \brief
		/// Destructor
		///
		virtual ~AssignmentStatement();

		/// \brief
		/// Get assignment result variable
		///
		/// \return
		/// Returns result variable access object.
		const ObjectAccess* getResultVariable() const;
		
		/// \brief
		/// Get value expression
		///
		/// \return
		/// Returns value expression object.
		const Expression* getValueExpression() const;

		/// \brief
		/// Method for checking if types are compatible and could be assigned
		///
		/// \param[in] result - assignment result type
		/// \param[in] value - assignment value type
		///
		/// \return
		/// \c True is returned if assignment could be done or \c false otherwise.
		static bool checkTypes(const Typed* result, const Typed* value);
	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_ASSIGNMENTSTATEMENT_HPP */

