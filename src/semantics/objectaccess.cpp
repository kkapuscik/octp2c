//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics object access class (implementation)
//--------------------------------------------------------------------

#include "objectaccess.hpp"

#include "variable.hpp"
#include "function.hpp"
#include "const.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	ObjectAccess::ObjectAccess(Variable* variable)
		: Typed(variable->getType())
		, m_variable(variable)
		, m_accessType(ENTIRE_VARIABLE)
	{
	}

	ObjectAccess::ObjectAccess(Function* function)
		/// \todo - type
		: Typed(NULL)
		, m_function(function)
		, m_accessType(FUNCTION_RETURN)
	{
	}

	ObjectAccess::ObjectAccess(Const* constans)
		: Typed(constans->getType())
		, m_const(constans)
		, m_accessType(CONSTANS)
	{
	}

	ObjectAccess::~ObjectAccess()
	{
	}

	//--------------------------------------------------------------------

	ObjectAccess::Type ObjectAccess::getAccessType() const
	{
		return m_accessType;
	}

	//--------------------------------------------------------------------

	Object* ObjectAccess::getAccessedObject() const
	{
		switch(m_accessType)
		{
		case CONSTANS:
			return m_const;
		case ENTIRE_VARIABLE:
			return m_variable;
		case FUNCTION_RETURN:
			return m_function;
		default:
			assert(0);
			return NULL;
		}
	}

	//--------------------------------------------------------------------

	Const* ObjectAccess::getConstans() const
	{
		assert(m_accessType == CONSTANS);

		return m_const;
	}

	//--------------------------------------------------------------------

}

