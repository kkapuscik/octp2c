//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics read / readln procedure class (implementation)
//--------------------------------------------------------------------

#include "readprocedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	ReadProcedure::ReadProcedure(bool readline, const String& id, Namespace* outer)
		: Procedure(id, outer)
		, m_readline(readline)
	{
	}

	ReadProcedure::~ReadProcedure()
	{
	}

	//--------------------------------------------------------------------

	bool ReadProcedure::getLineFlag() const
	{
		return m_readline;
	}

}

