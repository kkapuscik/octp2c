//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics write / writeln procedure class (implementation)
//--------------------------------------------------------------------

#include "writeprocedure.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	WriteProcedure::WriteProcedure(bool writeline, const String& id, Namespace* outer)
		: Procedure(id, outer)
		, m_writeline(writeline)
	{
	}

	WriteProcedure::~WriteProcedure()
	{
	}

	//--------------------------------------------------------------------

	bool WriteProcedure::getLineFlag() const
	{
		return m_writeline;
	}

}

