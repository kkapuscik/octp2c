//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics empty statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_EMPTYSTATEMENT_HPP
#define OCT_SEMANTICS_EMPTYSTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Empty statement class
	///
	class EmptyStatement : public Statement
	{
		DEFINE_RTTI(EmptyStatement,Statement)

	public:
		/// \brief
		/// Constructor
		///
		EmptyStatement();

		/// \brief
		/// Destructor
		///
		virtual ~EmptyStatement();
	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_EMPTYSTATEMENT_HPP */
