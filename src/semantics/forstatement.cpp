//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics FOR loop statement class (implementation)
//--------------------------------------------------------------------

#include "forstatement.hpp"

#include "expression.hpp"
#include "objectaccess.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	ForStatement::ForStatement(Direction dir, ObjectAccess* indexvariable,
		Expression* initialvalue, Expression* finalvalue,
		Statement* loopstatement)
		: Statement()
		, m_direction(dir)
		, m_indexVariable(indexvariable)
		, m_initialValue(initialvalue)
		, m_finalValue(finalvalue)
		, m_loopStatement(loopstatement)
	{
	}

	ForStatement::~ForStatement()
	{
		delete m_indexVariable;
		delete m_initialValue;
		delete m_finalValue;
		delete m_loopStatement;
	}

	//--------------------------------------------------------------------

	ForStatement::Direction ForStatement::getDirection() const
	{
		return m_direction;
	}

	ObjectAccess* ForStatement::getIndexVariable() const
	{
		return m_indexVariable;
	}

	Expression* ForStatement::getInitialValue() const
	{
		return m_initialValue;
	}

	Expression* ForStatement::getFinalValue() const
	{
		return m_finalValue;
	}

	Statement* ForStatement::getLoopStatement() const
	{
		return m_loopStatement;
	}

}
