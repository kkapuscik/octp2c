//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics REPEAT loop statement class (implementation)
//--------------------------------------------------------------------

#include "repeatstatement.hpp"

#include "expression.hpp"
#include "statementsequence.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	RepeatStatement::RepeatStatement(const Expression* condition,
		const StatementSequence* sequence)
		: Statement()
		, m_conditionEpression(condition)
		, m_loopSequence(sequence)
	{
	}

	RepeatStatement::~RepeatStatement()
	{
		delete m_conditionEpression;
		delete m_loopSequence;
	}

	//--------------------------------------------------------------------

	const Expression* RepeatStatement::getCondition() const
	{
		return m_conditionEpression;
	}

	const StatementSequence* RepeatStatement::getSequence() const
	{
		return m_loopSequence;
	}

}