//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics global namespace class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_GLOBAL_HPP
#define OCT_SEMANTICS_GLOBAL_HPP

//--------------------------------------------------------------------

#include "namespace.hpp"
//#include "program.hpp"

//--------------------------------------------------------------------

namespace Semantics
{
    class Program;

	/// \brief
	/// Global namespace class
	///
	/// Global namespace class is a main class holding all other
	/// semantic classes - it is a root of semantics tree.
    class Global : public Namespace
    {
        DEFINE_RTTI(Global,Namespace)

    private:
        /// Program objects collection
        Namespace m_programs;        

    public:
        /// \brief
        /// Constructor
        ///
        Global(void);

        /// \brief
        /// Destructor
        ///
        ~Global(void);

    public:
        // SES output support
        virtual bool writeSES(SESStream& stream, int indent) const;
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_GLOBAL_HPP */
