//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics simple type class (implementation)
//--------------------------------------------------------------------

#include "simpletype.hpp"

using std::endl;

namespace Semantics
{

//--------------------------------------------------------------------

SimpleType::SimpleType(TypeId tid)
    : Type(SimpleType::getTypeName(tid))
    , m_typeId(tid)
{
}

SimpleType::~SimpleType(void)
{
}

//--------------------------------------------------------------------

SimpleType::TypeId SimpleType::getTypeId(void) const
{
    return m_typeId;
}

//--------------------------------------------------------------------

bool SimpleType::writeSES(SESStream& stream, int indent) const
{
    if(!isOfClass(clid_SimpleType))
        assert(0);
    
    // write begin tag
    writeIndentSES(stream, indent);
    stream << "<SIMPLETYPE>" << endl;

/*
	// write type name
	writeIndentSES(stream, indent+1);
	stream << "<TYPENAME>" << getTypeName() << "</TYPENAME>" << endl;
*/

    // write contents
    if(!writeContentsSES(stream, indent+1))
        return false;

    // write end tag
    writeIndentSES(stream, indent);
    stream << "</SIMPLETYPE>" << endl;

    return true;
}

//--------------------------------------------------------------------

const String& SimpleType::getTypeName(void) const
{
    return SimpleType::getTypeName(m_typeId);
}

const String& SimpleType::getTypeName(TypeId tid)
{
    switch(tid)
    {
    case REAL:
        {
            static String name = "Real";
            return name;
        }
    case INTEGER:
        {
            static String name = "Integer";
            return name;
        }
    case BOOLEAN:
        {
            static String name = "Boolean";
            return name;
        }
        break;
    case CHAR:
        {
            static String name = "Char";
            return name;
        }
    case STRING:
        {
            static String name = "String";
            return name;
        }
	case INVALID:
        {
			static String name = "### INVALID ###";
            return name;
        }
		break;
    default:
        assert(0);
        static String empty;
        return empty;
    }
}

//--------------------------------------------------------------------

}
