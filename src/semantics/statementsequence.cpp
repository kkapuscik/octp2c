//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics statement sequence class (implementation)
//--------------------------------------------------------------------

#include "statementsequence.hpp"

namespace Semantics
{

	//--------------------------------------------------------------------

	StatementSequence::StatementSequence()
		: Statement()
	{
	}

	StatementSequence::~StatementSequence()
	{
		SequenceCollection::const_iterator iter;
		for(iter = m_sequence.begin(); iter != m_sequence.end(); ++iter)
			delete *iter;
	}

	//--------------------------------------------------------------------

	void StatementSequence::addStatement(Statement* statement)
	{
		assert(statement);

		m_sequence.push_back(statement);
	}

	const StatementSequence::SequenceCollection& StatementSequence::getSequence() const
	{
		return m_sequence;
	}

	//--------------------------------------------------------------------

}
