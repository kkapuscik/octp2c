//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics variable class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_VARIABLE_HPP
#define OCT_SEMANTICS_VARIABLE_HPP

//--------------------------------------------------------------------

#include "typed.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Semantics variable class
	///
	class Variable : public Typed
	{
		DEFINE_RTTI(Variable,Typed)

	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] type - semantic object type
		/// \param[in] id - object identificator
		///
		/// \remarks
		/// The 'type' object given is managed by constructed object,
		/// so it MUST NOT be fried or modified outside after the
		/// construction is finished.
		Variable(const Type* type, const String& id);

		/// \brief
		/// Destructor
		///
		virtual ~Variable(void);

		// SES support
	public:
		bool writeSES(SESStream& stream, int indent) const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_VARIABLE_HPP*/
