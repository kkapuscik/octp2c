//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics FOR loop statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_FORSTATEMENT_HPP
#define OCT_SEMANTICS_FORSTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"

namespace Semantics
{
	class ObjectAccess;
	class Expression;
}

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// FOR loop statement class
	///
	class ForStatement : public Statement
	{
		DEFINE_RTTI(ForStatement,Statement)

	public:
		/// For loop direction enumeration type
		enum Direction
		{
			/// Up direction (incrementation)
			DIRECTION_UPTO,
			/// Down direction (decrementation)
			DIRECTION_DOWNTO
		};

	private:
		/// Loop direction
		Direction m_direction;
		/// Index variable
		ObjectAccess* m_indexVariable;
		/// Initial value
		Expression* m_initialValue;
		/// Final value
		Expression* m_finalValue;
		/// Loop statement
		Statement* m_loopStatement;

	public:
		/// \brief
		/// Constructor
		///
		/// Creates initialized statement object
		///
		/// \param[in] dir - loop direction
		/// \param[in] indexvariable - access to variable used for indexing
		/// \param[in] initialvalue - initial index value
		/// \param[in] finalvalue - final index value
		/// \param[in] loopstatement - loop body statement
		ForStatement(Direction dir, ObjectAccess* indexvariable,
			Expression* initialvalue, Expression* finalvalue,
			Statement* loopstatement);

		/// \brief
		/// Destructor
		///
		virtual ~ForStatement();

		/// \brief
		/// Get loop direction
		///
		/// \return
		/// Returns loop direction.
		Direction getDirection() const;

		/// \brief
		/// Get index variable
		///
		/// \return
		/// Returns access object to index variable.
		ObjectAccess* getIndexVariable() const;

		/// \brief
		/// Get initial index value
		///
		/// \return
		/// Returns expression which value is initial index value.
		Expression* getInitialValue() const;

		/// \brief
		/// Get final index value
		///
		/// \return
		/// Returns expression which value is final index value.
		Expression* getFinalValue() const;

		/// \brief
		/// Get loop body statement
		///
		/// \return
		/// Returns loop body statement object.
		Statement* getLoopStatement() const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_FORSTATEMENT_HPP */

