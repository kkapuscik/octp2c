//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics global namespace class (implementation)
//--------------------------------------------------------------------

#include "global.hpp"

using std::endl;

//--------------------------------------------------------------------

namespace Semantics
{

//--------------------------------------------------------------------

Global::Global(void)
{
}

Global::~Global(void)
{
}

//--------------------------------------------------------------------

bool Global::writeSES(SESStream& stream, int indent) const
{
    if(!isOfClass(clid_Global))
        assert(0);

    // write begin tag
    writeIndentSES(stream, indent);
    stream << "<GLOBAL>" << endl;

    // write contents
    if(!writeContentsSES(stream, indent+1))
        return false;

    // write end tag
    writeIndentSES(stream, indent);
    stream << "</GLOBAL>" << endl;

    return true;
}

//--------------------------------------------------------------------

}

//--------------------------------------------------------------------
