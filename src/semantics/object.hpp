//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantic objects base class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_OBJECT_HPP
#define OCT_SEMANTICS_OBJECT_HPP

//--------------------------------------------------------------------

#include "utility/string.hpp"
#include "rtti.hpp"
#include "rtti_types.hpp"

#include <cassert>
#include <iostream>

//--------------------------------------------------------------------

using std::endl;

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Semantic object class
	///
	/// Semantic object class is a base class for all other semantic
	/// elements classes.
	class Object
	{
		// RTTI support - this is placed here instead of
		// DEFINE_RTTI(Object,<empty>) for purposes of
		// documentation generation.
	public:
		/// \brief
		/// Get class name
		///
		/// \return
		/// String describing class name.
		static const String& getClassNameStatic(void)
		{
			static String clname("Object");
			return clname;
		}
	                                                        
		/// \brief
		/// Get class identifier
		///
		/// \return
		/// Class identifier.
		static ClassId getClassIdStatic(void)
		{
			return clid_Object;
		}
	                                                        
		/// \brief
		/// Check if object is of given class
		///
		/// \param[in] clid - class identifier
		///
		/// \return
		/// Method returns true is object is of specified class or false otherwise.
		virtual bool isOfClass(ClassId clid) const				
		{
			return Object::getClassIdStatic() == clid;
		}

		/// \brief
		/// Get class name for current object
		///
		/// \return
		/// Class name string.
		virtual const String& getClassName(void) const
		{                                             
			return Object::getClassNameStatic();         
		}

		/// \brief
		/// Get class identifier for current object
		///
		/// \return
		/// Current object class identifier.
		virtual ClassId getClassId(void) const
		{
			return Object::getClassIdStatic();
		}

	private:
		/// Object identificator
		String m_id;

	public:
		/// \brief
		/// Constructor
		///
		/// \param[in] id - object identificator (default: empty)
		Object(const String& id = String());

		/// \brief
		/// Destructor
		///
		virtual ~Object(void);


		/// \brief
		/// Get object identificator
		///
		/// \return
		/// Object identificator
		const String& getId(void) const;

		/// \brief
		/// Set object identifier
		///
		/// \param[in] id - new object identifier
		///
		/// \remarks
		/// Use this function carefully. The object that are managed by
		/// other objects (ex. namespaces) should not be modified outside
		/// manager in ANY way, so the identifier should not be changed too.
		void setId(const String& id);

	public:
		/// SES (Semantic Structure) Stream type
		typedef std::ostream SESStream;

		/// \brief
		/// Write indent for SES (Semantic Structure) output
		///
		/// \param[in] stream - SES output stream
		/// \param[in] indent - output indentation
		///
		/// \return
		/// Returns true on success and false otherwise.
		bool writeIndentSES(SESStream& stream, int indent) const;

		/// \brief
		/// Write object SES (Semantic Structure) output
		///
		/// \param[in] stream - SES output stream
		/// \param[in] indent - output indentation
		///
		/// \return
		/// Returns true on success and false otherwise.
		virtual bool writeSES(SESStream& stream, int indent) const;

		/// \brief
		/// Write object contents SES (Semantic Structure) output
		///
		/// \param[in] stream - SES output stream
		/// \param[in] indent - output indentation
		///
		/// \return
		/// Returns true on success and false otherwise.
		virtual bool writeContentsSES(SESStream& stream, int indent) const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_OBJECT_HPP */
