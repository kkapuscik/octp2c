//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics typed objects base class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_TYPED_HPP
#define OCT_SEMANTICS_TYPED_HPP

//--------------------------------------------------------------------

#include "object.hpp"
#include "type.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Base typed object
	///
	/// This is a base class for all objects that have a type
	/// (ex. variables, constans).
	class Typed : public Object
	{
		DEFINE_RTTI(Typed,Object)

	private:
		/// Semantic object type
		const Type* m_type;
	    
	protected:
		/// \brief
		/// Constructor
		///
		/// \param[in] type - semantic object type
		/// \param[in] id - object identificator (default: empty)
		///
		/// \remarks
		/// The 'type' object given is not managed by Typed object.
		Typed(const Type* type, const String& id = String());

	public:
		/// \brief
		/// Destructor
		///
		virtual ~Typed(void);
	    
		/// \brief
		/// Get semantic type
		///
		/// \return
		/// Semantic type object.
		const Type* getType(void) const;

		/// \brief
		/// Set semantic type
		///
		/// \param[in] type - new semantic type
		///
		/// \remarks
		/// The 'type' object given is managed by Typed object,
		/// so it MUST NOT be fried or modified outside after the
		/// construction is finished.
		void setType(const Type* type);

		// SES support
		virtual bool writeContentsSES(SESStream& stream, int indent) const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_TYPED_HPP */
