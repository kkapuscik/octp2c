//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics typed objects base class (implementation)
//--------------------------------------------------------------------

#include "typed.hpp"

namespace Semantics
{

//--------------------------------------------------------------------

Typed::Typed(const Type* type, const String& id)
    : Object(id)
    , m_type(type)
{
	setType(type);
}

Typed::~Typed(void)
{
}

//--------------------------------------------------------------------

const Type* Typed::getType(void) const
{
    return m_type;
}

void Typed::setType(const Type* type)
{
	m_type = type;
}

//--------------------------------------------------------------------

bool Typed::writeContentsSES(SESStream& stream, int indent) const
{
    // write begin tag
    writeIndentSES(stream, indent);
    stream << "<TYPE>" << endl;

	// write type name
	m_type->writeSES(stream, indent+1);

    // write begin tag
    writeIndentSES(stream, indent);
    stream << "</TYPE>" << endl;

    return true;
}

//--------------------------------------------------------------------

}
