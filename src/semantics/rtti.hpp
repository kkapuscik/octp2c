//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics objects RTTI support
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_RTTI_HPP
#define OCT_SEMANTICS_RTTI_HPP

//--------------------------------------------------------------------

#include "utility/string.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	class Object;

	//--------------------------------------------------------------------

	/// \brief
	/// Templated casting function for type-safe casting
	///
	/// Function tries to cast object to type specified by template
	/// and returns casted pointer or NULL if casting was not successful.
	template <class T>
	T* cast(Object* object)
	{
		if(object)
			return object->isOfClass(T::getClassIdStatic()) ? (T*)object : NULL;
		else
			return NULL;
	}

	/// \brief
	/// Templated casting function for type-safe casting (const version)
	///
	/// Function tries to cast object to type specified by template
	/// and returns casted pointer or NULL if casting was not successful.
	template <class T>
	const T* cast(const Object* object)
	{
		if(object)
			return object->isOfClass(T::getClassIdStatic()) ? (const T*)object : NULL;
		else
			return NULL;
	}

}

//--------------------------------------------------------------------

#define DEFINE_RTTI(classname,parentname)           \
public:                                             \
static const String& getClassNameStatic(void)       \
{                                                   \
    static String clname(#classname);               \
    return clname;                                  \
}                                                   \
static ClassId getClassIdStatic(void)               \
{                                                   \
    return clid_##classname ;                       \
}                                                   \
virtual bool isOfClass(ClassId clid) const          \
{                                                   \
    if(classname::getClassIdStatic() == clid)       \
        return true;                                \
    else                                            \
        return parentname::isOfClass(clid);         \
}                                                   \
virtual ClassId getClassId(void) const				\
{													\
	return classname::getClassIdStatic();			\
}													\
virtual const String& getClassName(void) const      \
{                                                   \
    return classname::getClassNameStatic();         \
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_RTTI_HPP */
