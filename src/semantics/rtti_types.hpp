//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics RTTI types definitions
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_RTTITYPES_HPP
#define OCT_SEMANTICS_RTTITYPES_HPP

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Semantics class identificators enumeration
	///
	/// Each semantics class has an entry in this enumeration.
	typedef enum ClassId
	{
		clid_Object,

		clid_Namespace,
		clid_Global,
		clid_NamedBlock,
		clid_Procedure,
		clid_Function,
		clid_Program,

		clid_ReadProcedure,
		clid_WriteProcedure,

		clid_Typed,
		clid_Expression,
		clid_Variable,

		clid_ObjectAccess,
		clid_ActualParameter,

		clid_Type,
		clid_ArrayType,
		clid_SimpleType,
		clid_RecordType,
		clid_FileType,

		clid_Const,
		clid_SimpleConst,
		clid_NilConst,

		clid_Statement,
		clid_StatementSequence,
		clid_ForStatement,
		clid_WhileStatement,
		clid_RepeatStatement,
		clid_AssignmentStatement,
		clid_EmptyStatement,
		clid_IfStatement,
		clid_ProcedureStatement
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_RTTITYPES_HPP */
