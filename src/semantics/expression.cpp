//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics expression class (implementation)
//--------------------------------------------------------------------

#include "expression.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	//--------------------------------------------------------------------

	Expression::Expression(Typed* value)
		: Typed(NULL)
		, m_expressionType(EXACT)
		, m_value1(value)
		, m_value2(NULL)
		, m_operator(O_NOOP)
	{
		/// \todo Set type
	}

	Expression::Expression(Operator oper, Typed* value)
		: Typed(NULL)
		, m_expressionType(UNARY)
		, m_value1(value)
		, m_value2(NULL)
		, m_operator(oper)
	{
		/// \todo Set type
	}

	Expression::Expression(Operator oper, Typed* value1, Typed* value2)
		: Typed(NULL)
		, m_expressionType(BINARY)
		, m_value1(value1)
		, m_value2(value2)
		, m_operator(oper)
	{
		/// \todo Set type
	}

	Expression::~Expression()
	{
		delete m_value1;
		delete m_value2;
	}

	//--------------------------------------------------------------------

	Expression::Type Expression::getExpressionType() const
	{
		return m_expressionType;
	}

	Typed* Expression::getFirstOperand() const
	{
		return m_value1;
	}

	Typed* Expression::getSecondOperand() const
	{
		return m_value2;
	}

	Operator Expression::getOperator() const
	{
		return m_operator;
	}

	//--------------------------------------------------------------------

}
