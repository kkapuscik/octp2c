//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics program class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_PROGRAM_HPP
#define OCT_SEMANTICS_PROGRAM_HPP

//--------------------------------------------------------------------

#include "namedblock.hpp"
#include "global.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// PROGRAM element class
	///
    class Program : public NamedBlock
    {
        DEFINE_RTTI(Program,NamedBlock)
    
    public:
        /// \brief
        /// Constructor
        ///
        /// \param id - program identifier
        /// \param outer - outer (higher level) namespace
        Program(const String& id, Global* outer);

        /// \brief
        /// Destructor
        ///
        virtual ~Program(void);
        
    public:
        // SES output support
        /// \todo PARAMETERS SUPPORT (in writeSESContents() )
        virtual bool writeSES(SESStream& stream, int indent) const;
    };

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_PROGRAM_HPP */
