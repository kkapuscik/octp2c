//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics procedure statement class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_PROCEDURESTATEMENT_HPP
#define OCT_SEMANTICS_PROCEDURESTATEMENT_HPP

//--------------------------------------------------------------------

#include "statement.hpp"
#include "actualparameter.hpp"

#include <vector>

namespace Semantics
{
	class Procedure;
};

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Assignment statement class
	///
	class ProcedureStatement : public Statement
	{
		DEFINE_RTTI(ProcedureStatement,Statement)

	public:
		typedef std::vector<ActualParameter*> ParameterCollection;

	private:
		Procedure* m_procedure;
		ParameterCollection m_parameters;

	public:
		/// \brief
		/// Constructor
		///
		ProcedureStatement(Procedure* procedure);

		/// \brief
		/// Destructor
		///
		virtual ~ProcedureStatement();

		void addParameter(ActualParameter* parameter);

		Procedure* getProcedure() const;
		int getParameterCount() const;
		const ActualParameter* getParameter(int pos) const;

	};
}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_PROCEDURESTATEMENT_HPP */

