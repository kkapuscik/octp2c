//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics namespace class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_NAMESPACE_HPP
#define OCT_SEMANTICS_NAMESPACE_HPP

//--------------------------------------------------------------------

#include "object.hpp"
#include "utility/autopointer.hpp"

#include <set>
#include <vector>

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Semantic namespace class
	///
	/// Semantic namespace class is a container class grouping other
	/// semantic objects. Class allows adding new object and finding
	/// them by identifier in recursive or non-recursive mode.
	class Namespace : public Object
	{
		DEFINE_RTTI(Namespace,Object)

	private:
		/// Object comparision by identifier functor
		class IdComparator
		{
		public:
			/// \brief
			/// Call operator used to compare objects
			///
			/// \param[in] so1 - first object to compare
			/// \param[in] so2 - second object to compare
			///
			/// \return
			/// \c True is returned if so1 < so2 or \c false otherwise.
			bool operator () (const Object* so1, const Object* so2) const
			{
				return CompareNC(so1->getId(), so2->getId()) < 0;
			}
		};

		/// Object collection type
		typedef std::set<Object*,IdComparator> ObjectCollection;
	public:
		/// Object collection sorted by insertion time type
		typedef std::vector<const Object*> SortedObjectCollection;

	private:
		/// Outer namespace
		Namespace* m_outer;
		/// Namespace objects
		ObjectCollection m_objects;
		/// Namespace object (sorted by insertion time)
		SortedObjectCollection m_sortedObjects;

	public:
		/// \brief
		/// Constructor
		///
		/// \param id - namespace identifier
		/// \param outer - outer (higher level) namespace
		Namespace(const String& id = String(), Namespace* outer = NULL);

		/// \brief
		/// Destructor
		///
		virtual ~Namespace(void);

		/// \brief
		/// Add object to namespace
		///
		/// Method takes the object given by autopointer and stores
		/// it in its internal collection it is possible (storing
		/// could be not possible due to ex. repeated name, bad
		/// object type - depending on Namespace type).
		/// 
		/// \warning
		/// If object is stored by namespace the autopointer is
		/// invalidated (NULL-ed) so it became unuseful! It object
		/// is not stored the autopointer status is not changed.
		/// 
		/// \param object - autopointer holding pointer to object that have to be stored
		///
		/// \return
		/// Method returns true on success or false on error (storing not possible).
		virtual bool add(AutoPointer<Object>& object);

		/// \brief
		/// Add object to namespace
		///
		/// Method takes the object given and stores it in its internal
		/// collection it is possible (storing could be not possible due
		/// to ex. repeated name, bad object type - depending on Namespace type).
		/// 
		/// \warning
		/// If object is stored by namespace it is managed (and it will be deleted)
		/// by the namespace object. It object is not stored its status is not changed.
		/// 
		/// \param object - pointer to object that have to be stored
		///
		/// \return
		/// Method returns true on success or false on error (storing not possible).
		virtual bool add(Object* object);


		/// \brief
		/// Find object by identifier
		///
		/// \param id - identifier of object to find
		/// \param recursive - flag indicating if search should be recursive
		///						(searching also in outer namespaces)
		///
		/// \return
		/// Pointer to object of NULL if object could not be found.
		virtual Object* find(const String& id, bool recursive);

		/// \brief
		/// Find object by identifier
		///
		/// \param id - identifier of object to find
		/// \param recursive - flag indicating if search should be recursive
		///						(searching also in outer namespaces)
		///
		/// \return
		/// Pointer to object of NULL if object could not be found.
		virtual const Object* find(const String& id, bool recursive) const;
        
        /// \brief
        /// Get outer namespace
        ///
        /// \return
        /// Parent namespace or NULL if object has no parent namespace.
        Namespace* getOuter(void);
        
		/// \brief
		/// Get object collection sorted by insertion time
		///
		/// \return
		/// Collection of objects sorted by insertion time.
		const SortedObjectCollection& getSortedObjects() const;
        
	protected:
		/// \brief
		/// Utility find method
		///
		/// This method is used to find object internally
		/// basing on created temporary object.
		///
		/// \param[in] sem - temporary object used to find
		/// \param[in] recursive - recursive search flag
		///
		/// \return
		/// Returns found object or \c NULL if object cannot be found.
		virtual Object* find(Object* sem, bool recursive) const;


	public:
		// SES support
		virtual bool writeContentsSES(SESStream& stream, int indent) const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_NAMESPACE_HPP */
