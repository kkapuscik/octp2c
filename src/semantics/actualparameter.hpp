//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics actual parameter class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_ACTUALPARAMETER_HPP
#define OCT_SEMANTICS_ACTUALPARAMETER_HPP

//--------------------------------------------------------------------

#include "typed.hpp"

namespace Semantics
{
	class ObjectAccess;
	class Function;
	class Procedure;
	class Expression;
}

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Actual parameter class
	///
	/// Actual parameter class is used to describe actual parameters
	/// specified for procedure or function call.
	class ActualParameter : public Typed
	{
		DEFINE_RTTI(ActualParameter,Typed)

	public:
		/// Parameter type
		enum Type
		{
			/// Access to variable
			VARIABLE_ACCESS,
			/// Procedure 
			PROCEDURE,
			/// Function
			FUNCTION,
			/// Expression (single)
			EXPRESSION
		};

	private:
		/// Access type
		Type m_parameterType;

		/// Accessed variable
		ObjectAccess* m_variableAccess;
		/// Procedure
		Procedure* m_procedure;
		/// Function
		Function* m_function;
		/// Expression
		Expression* m_expression;

	public:
		/// \brief
		/// Constructor
		///
		/// This constructor creates 'variable access' parameter object.
		///
		/// \param[in] variableaccess - accessed variable descriptor
		ActualParameter(ObjectAccess* variableaccess);

		/// \brief
		/// Constructor
		///
		/// This constructor creates 'procedure' parameter object.
		///
		/// \param[in] procedure - procedure object
		ActualParameter(Procedure* procedure);

		/// \brief
		/// Constructor
		///
		/// This constructor creates 'function' parameter object.
		///
		/// \param[in] function - function object
		ActualParameter(Function* function);

		/// \brief
		/// Constructor
		///
		/// This constructor creates 'single expression' parameter object.
		///
		/// \param[in] expression - expression object
		ActualParameter(Expression* expr);

		/// \brief
		/// Destructor
		///
		virtual ~ActualParameter();


		Type getParameterType() const;

		Expression* getExpression() const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_ACTUALPARAMETER_HPP */
