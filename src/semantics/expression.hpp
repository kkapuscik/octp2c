//--------------------------------------------------------------------
/// \file
/// \brief
/// Semantics expression class
//--------------------------------------------------------------------

#ifndef OCT_SEMANTICS_EXPRESSION_HPP
#define OCT_SEMANTICS_EXPRESSION_HPP

//--------------------------------------------------------------------

#include "typed.hpp"
#include "operators.hpp"

//--------------------------------------------------------------------

namespace Semantics
{

	/// \brief
	/// Expression class
	///
	/// Expression class represents all expression objects - there are
	/// general types:
	/// EXACT - no operator, single value expression;
	/// UNARY - unary operator, single value expression
	/// BINARY - binary operator, double value expression
	class Expression : public Typed
	{
		DEFINE_RTTI(Expression,Typed)

	public:
		/// Expression type
		enum Type
		{
			/// Exact value of given object
			EXACT,
			/// Unary operator expression
			UNARY,
			/// Binary operator expression
			BINARY
		};

	private:
		/// Expression type
		Type m_expressionType;
		/// First value element
		Typed* m_value1;
		/// Second value element
		Typed* m_value2;
		/// Expression operator
		Operator m_operator;

	public:
		/// \brief
		/// Constructor
		///
		/// Creates no-operator expression (the value is equal to given Typed object).
		Expression(Typed* value);

		/// \brief
		/// Constructor
		///
		/// Creates unary operator expression.
		Expression(Operator oper, Typed* value);

		/// \brief
		/// Constructor
		///
		/// Creates binary operator expression.
		Expression(Operator oper, Typed* value1, Typed* value2);

		/// \brief
		/// Destructor
		///
		virtual ~Expression();

		/// \brief
		/// Get expression type
		///
		/// \return
		/// Returns expression type.
		Type getExpressionType() const;

		/// \brief
		/// Get first operand
		///
		/// \return
		/// Returns first operand object pointer or NULL if operand does not exist.
		Typed* getFirstOperand() const;

		/// \brief
		/// Get second operand
		///
		/// \return
		/// Returns second operand object pointer or NULL if operand does not exist.
		Typed* getSecondOperand() const;

		/// \brief
		/// Get expression operator
		///
		/// \return
		/// Returns expression operator.
		Operator getOperator() const;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_SEMANTICS_EXPRESSION_HPP */
