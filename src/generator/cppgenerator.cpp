//--------------------------------------------------------------------
/// \file
/// \brief
/// C++ language output code generator class (implementation)
//--------------------------------------------------------------------

#include "cppgenerator.hpp"

#include "semantics/global.hpp"
#include "semantics/const.hpp"
#include "semantics/simpleconst.hpp"
#include "semantics/simpletype.hpp"
#include "semantics/program.hpp"
#include "semantics/variable.hpp"
#include "semantics/statementsequence.hpp"
#include "semantics/ifstatement.hpp"
#include "semantics/assignmentstatement.hpp"
#include "semantics/repeatstatement.hpp"
#include "semantics/whilestatement.hpp"
#include "semantics/forstatement.hpp"
#include "semantics/procedurestatement.hpp"
#include "semantics/procedure.hpp"
#include "semantics/writeprocedure.hpp"
#include "semantics/readprocedure.hpp"

using namespace Semantics;

//--------------------------------------------------------------------

namespace Generators
{

	//--------------------------------------------------------------------

	void CppGenerator::resetIndent()
	{
		m_indent = 0;
	}

	void CppGenerator::incIndent()
	{
		++m_indent;
	}

	void CppGenerator::decIndent()
	{
		--m_indent;
	}

	String CppGenerator::newline()
	{
		String tmp("\n");
		for(int i=0; i<m_indent; ++i)
			tmp += "    ";

		return tmp;
	}


	//--------------------------------------------------------------------

	bool CppGenerator::generate(GenStream& stream, const Global* global)
	{
		resetIndent();

		stream << "//-------------------------------------------------" << newline();
		stream << "// File produced by OCTp2c C++ generator" << newline();
		stream << "//-------------------------------------------------" << newline();

		return processGlobal(stream, global);
	}

	//--------------------------------------------------------------------

	bool CppGenerator::processGlobal(GenStream& stream, const Global* global)
	{
		bool error = false;

		const Namespace::SortedObjectCollection& objects = global->getSortedObjects();
		Namespace::SortedObjectCollection::const_iterator iter = objects.begin();
		Namespace::SortedObjectCollection::const_iterator enditer = objects.end();

		for(; iter != enditer; ++iter)
		{
			const Object* obj = *iter;
			if(obj == NULL)
				continue;

			// process program element
			if(obj->isOfClass(clid_Program))
			{
				if(processProgram(stream, cast<Program>(obj)) == false)
					error = true;
			}
			// skip simple type definitions
			else if(obj->isOfClass(clid_SimpleType))
				continue;
			// skip standard functions
			/// \todo This should be probably changed!
			else if(obj->isOfClass(clid_Procedure))
				continue;
			// report error - unknown element
			else
				error = true;
		}

		return !error;
	}

	//--------------------------------------------------------------------

	bool CppGenerator::processProgram(GenStream& stream, const Program* program)
	{
		bool error = false;

		stream << "//" << newline();
		stream << "// Program: " << program->getId() << newline();
		stream << "//" << newline();
		stream << newline();
		stream << newline();
		stream << "#include <cstring>" << newline();
		stream << "#include <iostream>" << newline();
		stream << "using namespace std;" << newline();
		stream << newline();

		const Namespace::SortedObjectCollection& objects = program->getSortedObjects();
		Namespace::SortedObjectCollection::const_iterator iter = objects.begin();
		Namespace::SortedObjectCollection::const_iterator enditer = objects.end();

		for(; iter != enditer; ++iter)
		{
			const Object* obj = *iter;
			if(obj == NULL)
				continue;

			if(obj->isOfClass(clid_Const))
			{
				if(processConstDeclaration(stream, cast<Const>(obj)) == false)
					error = true;
			}
			else if(obj->isOfClass(clid_Variable))
			{
				if(processVariable(stream, cast<Variable>(obj)) == false)
					error = true;
			}
			else
				error = true;
		}

		if(!error)
			if(createMainFunction(stream, program->getStatementSequence()) == false)
				error = true;

		return !error;
	}

	//--------------------------------------------------------------------

	const char* CppGenerator::getSimpleTypeName(SimpleType::TypeId tid)
	{
		switch(tid)
		{
		case SimpleType::STRING:
			return "char*";
		case SimpleType::INTEGER:
			return "int";
		case SimpleType::BOOLEAN:
			return "bool";
		case SimpleType::REAL:
			return "double";
		case SimpleType::CHAR:
			return "char";
		default:
			assert(0);
			return NULL;
		}
	}

	bool CppGenerator::processConstDeclaration(GenStream& stream, const Const* constobj)
	{
		if(constobj->isOfClass(clid_SimpleConst))
		{
			const SimpleConst* simple = cast<SimpleConst>(constobj);
			stream << "const "
				<< getSimpleTypeName(simple->getSimpleType()->getTypeId())
				<< " ";

			stream << constobj->getId() << " = ";
			processConst(stream, constobj);
			stream << ";" << newline();

			return true;
		}
		else
			return false;
	}

	bool CppGenerator::processConst(GenStream& stream, const Const* constobj)
	{
		if(constobj->isOfClass(clid_SimpleConst))
		{
			const SimpleConst* simple = cast<SimpleConst>(constobj);

			switch(simple->getSimpleType()->getTypeId())
			{
			case SimpleType::STRING:
				{
					String value;
					simple->getValue(value);
					encodeString(stream, value);
					break;
				}
			case SimpleType::INTEGER:
				{
					int value;
					simple->getValue(value);
					stream << value;
					break;
				}
			case SimpleType::BOOLEAN:
				{
					bool value;
					simple->getValue(value);
					stream << value ? "true" : "false";
					break;
				}
			case SimpleType::REAL:
				{
					double value;
					simple->getValue(value);
					stream << value;
					break;
				}
			case SimpleType::CHAR:
				{
					char value;
					simple->getValue(value);
					stream << value;
					break;
				}
			default:
				return false;
			}
			return true;
		}
		return false;
	}


	bool CppGenerator::processVariable(GenStream& stream, const Variable* variable)
	{
		const Type* type = variable->getType();

		if(type->isOfClass(clid_SimpleType))
		{
			stream << getSimpleTypeName(cast<SimpleType>(type)->getTypeId()) << " "
				<< variable->getId() << ";" << newline();

			return true;
		}
		else
			return false;
	}

	//--------------------------------------------------------------------

	void CppGenerator::encodeString(GenStream& stream, const String& value)
	{
		stream << '"';
		String::size_type i, len = value.length();
		for(i=0; i<len; ++i)
		{
			switch(value[i])
			{
			case '"':	stream << "\\\"";	break;
			case '\n':	stream << "\\n";	break;
			case '\r':	stream << "\\r";	break;
			case '\t':	stream << "\\t";	break;
			case '\0':	stream << "\\0";	break;
			default:	stream << value[i];	break;
			}
		}
		stream << '"';
	}

	//--------------------------------------------------------------------

	bool CppGenerator::createMainFunction(GenStream& stream, const StatementSequence* sequence)
	{
		stream << newline();
		stream << "int main(int argc, char* argv[])" << newline();
		return processStatementSequence(stream, sequence, true);
	}

	bool CppGenerator::processStatementSequence(GenStream& stream, const StatementSequence* sequence, bool mainreturn)
	{
		const StatementSequence::SequenceCollection& statements = sequence->getSequence();
		StatementSequence::SequenceCollection::size_type i, n;

		stream << "{" << newline();
		n = statements.size();
		for(i = 0; i < n; ++i)
			if(processStatement(stream, statements.at(i)) == false)
				return false;
		if(mainreturn)
			stream << "return 0;" << newline();
		stream << "}" << newline();
		
		return true;
	}

	bool CppGenerator::processStatement(GenStream& stream, const Statement* statement)
	{
		if(statement->isOfClass(clid_StatementSequence))
		{
			return processStatementSequence(stream, cast<StatementSequence>(statement));
		}
		if(statement->isOfClass(clid_IfStatement))
		{
			const IfStatement* stat = cast<IfStatement>(statement);

			stream << "if(";
			if(processExpression(stream, stat->getCondition()) == false)
				return false;
			stream << ")" << newline();
			if(processStatement(stream, stat->getIfStatement()) == false)
				return false;

			if(stat->getElseStatement() != NULL)
			{
				if(stat->getElseStatement()->isOfClass(clid_IfStatement))
					stream << "else ";
				else
					stream << "else" << newline();
				if(processStatement(stream, stat->getElseStatement()) == false)
					return false;
			}

			return true;
		}
		else if(statement->isOfClass(clid_AssignmentStatement))
		{
			const AssignmentStatement* stat = cast<AssignmentStatement>(statement);
			
			if(processObjectAccess(stream, stat->getResultVariable()) == false)
				return false;
			stream << " = ";
			if(processExpression(stream, stat->getValueExpression()) == false)
				return false;
			stream << ";" << newline();

			return true;
		}
		else if(statement->isOfClass(clid_RepeatStatement))
		{
			const RepeatStatement* repeat = cast<RepeatStatement>(statement);

			stream << "do" << endl;
			if(processStatementSequence(stream, repeat->getSequence()) == false)
				return false;

			stream << " while(!(";
			if(processExpression(stream, repeat->getCondition()) == false)
				return false;
			stream << "));" << endl;

			return true;
		}
		else if(statement->isOfClass(clid_ForStatement))
		{
			const ForStatement* forstat = cast<ForStatement>(statement);

			if(forstat->getDirection() == ForStatement::DIRECTION_UPTO)
			{
/// \todo Fix to be standard compatible
/*
begin
temp1 := e1;
temp2 := e2;
if temp1 <= temp2 then
begin
v := temp1;
body;
while v <> temp2 do
begin
v := succ(v);
body
end
end
end
*/
				stream << "for(";
				if(processTyped(stream, forstat->getIndexVariable()) == false)
					return false;
				stream << " = ";
				if(processExpression(stream, forstat->getInitialValue()) == false)
					return false;
				stream << "; ";
				if(processTyped(stream, forstat->getIndexVariable()) == false)
					return false;
				stream << " <= ";
				if(processExpression(stream, forstat->getFinalValue()) == false)
					return false;
				stream << "; ++";
				if(processTyped(stream, forstat->getIndexVariable()) == false)
					return false;
				stream << ")" << endl;
				if(processStatement(stream, forstat->getLoopStatement()) == false)
					return false;

				return true;
			}
			else
			{
/// \todo Fix to be standard compatible
/*
begin
temp1 := e1;
temp2 := e2;
if temp1 >= temp2 then
begin
v := temp1;
body;
while v <> temp2 do
begin
v := pred(v);
body
end
end
end
*/
				stream << "for(";
				if(processTyped(stream, forstat->getIndexVariable()) == false)
					return false;
				stream << " = ";
				if(processExpression(stream, forstat->getInitialValue()) == false)
					return false;
				stream << "; ";
				if(processTyped(stream, forstat->getIndexVariable()) == false)
					return false;
				stream << " >= ";
				if(processExpression(stream, forstat->getFinalValue()) == false)
					return false;
				stream << "; --";
				if(processTyped(stream, forstat->getIndexVariable()) == false)
					return false;
				stream << ")" << endl;
				if(processStatement(stream, forstat->getLoopStatement()) == false)
					return false;

				return true;
			}
		}
		else if(statement->isOfClass(clid_WhileStatement))
		{
			const WhileStatement* whilestat = cast<WhileStatement>(statement);

			stream << "while(";
			if(processExpression(stream, whilestat->getCondition()) == false)
				return false;
			stream << ")" << endl;

			if(processStatement(stream, whilestat->getLoopStatement()) == false)
				return false;

			return true;
		}
		else if(statement->isOfClass(clid_EmptyStatement))
		{
			stream << ";" << newline();
			return true;
		}
		else if(statement->isOfClass(clid_ProcedureStatement))
		{
			return processProcedureStatement(stream, cast<ProcedureStatement>(statement));
		}
		return false;
	}

	//--------------------------------------------------------------------
	
	bool CppGenerator::processProcedureStatement(GenStream& stream, const ProcedureStatement* statement)
	{
		const Procedure* proc = statement->getProcedure();

		if(proc->isOfClass(clid_WriteProcedure))
		{
			const WriteProcedure* wrproc = cast<WriteProcedure>(proc);

			if(statement->getParameterCount() > 0)
			{
				const ActualParameter* param;
				for(int i=0; i<statement->getParameterCount(); ++i)
				{
					stream << "cout << ";

					param = statement->getParameter(i);
					if(processActualParameter(stream, param) == false)
						return false;

					stream << ";" << endl;
				}
			}
			if(wrproc->getLineFlag())
				stream << "cout << endl;\n" << endl;

			return true;
		}
		else if(proc->isOfClass(clid_ReadProcedure))
		{
			const ReadProcedure* rdproc = cast<ReadProcedure>(proc);

			if(statement->getParameterCount() > 0)
			{
				const ActualParameter* param;
				for(int i=0; i<statement->getParameterCount(); ++i)
				{
					stream << "cin >> ";

					param = statement->getParameter(i);
					if(processActualParameter(stream, param) == false)
						return false;

					stream << ";" << endl;
				}
			}
			if(rdproc->getLineFlag())
				stream << "cin.ignore(numeric_limits<int>::max, '\n');\n" << endl;

			return true;
		}
		else
		{
			return false;
		}

		return false;
	}

	bool CppGenerator::processActualParameter(GenStream& stream, const ActualParameter* param)
	{
		switch(param->getParameterType())
		{
		case ActualParameter::EXPRESSION:
			return processExpression(stream, param->getExpression());
		}
		return false;
	}

	//--------------------------------------------------------------------

	bool CppGenerator::processExpression(GenStream& stream, const Expression* expr)
	{
		switch(expr->getExpressionType())
		{
		case Expression::EXACT:
			if(processTyped(stream, expr->getFirstOperand()) == false)
				return false;
			return true;
		case Expression::UNARY:
			stream << "(";
			if(processOperator(stream, expr->getOperator()) == false)
				return false;
			if(processTyped(stream, expr->getFirstOperand()) == false)
				return false;
			stream << ")";
			return true;
		case Expression::BINARY:
			stream << "(";
			if(processTyped(stream, expr->getFirstOperand()) == false)
				return false;
			stream << " ";
			if(processOperator(stream, expr->getOperator()) == false)
				return false;
			stream << " ";
			if(processTyped(stream, expr->getSecondOperand()) == false)
				return false;
			stream << ")";
			return true;
		default:
			assert(0);
			return false;
		}
	}

	bool CppGenerator::processObjectAccess(GenStream& stream, const ObjectAccess* access)
	{
		switch(access->getAccessType())
		{
		case ObjectAccess::CONSTANS:
		case ObjectAccess::ENTIRE_VARIABLE:
			stream << access->getAccessedObject()->getId();
			return true;
		default:
			return false;
		}
	}

	//--------------------------------------------------------------------

	bool CppGenerator::processTyped(GenStream& stream, const Typed* typed)
	{
		if(typed->isOfClass(clid_Expression))
			return processExpression(stream, cast<Expression>(typed));
		else if(typed->isOfClass(clid_ObjectAccess))
			return processObjectAccess(stream, cast<ObjectAccess>(typed));
		else if(typed->isOfClass(clid_Const))
			return processConst(stream, cast<Const>(typed));
		else
			return false;
	}

	//--------------------------------------------------------------------

	bool CppGenerator::processOperator(GenStream& stream, Operator oper)
	{
		switch(oper)
		{
		case O_NOOP:						return true;
		case O_LT:		stream << "<";		return true;
		case O_LE:		stream << "<=";		return true;
		case O_GE:		stream << ">=";		return true;
		case O_GT:		stream << ">";		return true;
		case O_NE:		stream << "!=";		return true;
		case O_EQ:		stream << "==";		return true;
		case O_MINUS:	stream << "-";		return true;
		case O_PLUS:	stream << "+";		return true;
		case O_MUL:		stream << "*";		return true;
		case O_DIV:		stream << "/";		return true;
		case O_REALDIV:	stream << "/";		return true;
		case O_MOD:		stream << "%";		return true;
		case O_IN:		assert(0);			return false;
		case O_OR:		stream << "|";		return true;
		case O_AND:		stream << "&";		return true;
		case O_NOT:		stream << "!";		return true;
		default:		assert(0);			return false;
		}
	}

}
