//--------------------------------------------------------------------
/// \file
/// \brief
/// C++ language output code generator class
//--------------------------------------------------------------------

#ifndef OCT_GENERATORS_CPP_GENERATOR_HPP
#define OCT_GENERATORS_CPP_GENERATOR_HPP

//--------------------------------------------------------------------

#include "generator.hpp"
#include "utility/string.hpp"
#include "semantics/simpletype.hpp"
#include "semantics/operators.hpp"

namespace Semantics
{
	class Program;
	class Const;
	class Variable;
	class Expression;
	class ObjectAccess;
	class Typed;
	class Statement;
	class StatementSequence;
	class ProcedureStatement;
	class ActualParameter;
}

//--------------------------------------------------------------------

namespace Generators
{

	/// \brief
	/// C++ code generator class
	///
	class CppGenerator : public Generator
	{
	private:
		/// Current indentation
		int m_indent;

	public:
		// Overloaded from Generator
		virtual bool generate(GenStream& stream, const Semantics::Global* global);

	private:
		bool processGlobal(GenStream& stream, const Semantics::Global* global);
		bool processProgram(GenStream& stream, const Semantics::Program* program);
		bool processConstDeclaration(GenStream& stream, const Semantics::Const* constobj);
		bool processVariable(GenStream& stream, const Semantics::Variable* variable);

		const char* getSimpleTypeName(Semantics::SimpleType::TypeId tid);

		void encodeString(GenStream& stream, const String& value);

		bool processExpression(GenStream& stream, const Semantics::Expression* expr);
		bool processObjectAccess(GenStream& stream, const Semantics::ObjectAccess* access);
		bool processTyped(GenStream& stream, const Semantics::Typed* typed);

		bool createMainFunction(GenStream& stream, const Semantics::StatementSequence* sequence);

		bool processStatementSequence(GenStream& stream, const Semantics::StatementSequence* sequence, bool mainreturn = false);
		bool processStatement(GenStream& stream, const Semantics::Statement* statement);
		bool processProcedureStatement(GenStream& stream, const Semantics::ProcedureStatement* statement);
		bool processActualParameter(GenStream& stream, const Semantics::ActualParameter* param);

		bool processOperator(GenStream& stream, Operator oper);
	
		bool processConst(GenStream& stream, const Semantics::Const* constobj);

		void resetIndent();
		void incIndent();
		void decIndent();
		String newline();
	};

}

//--------------------------------------------------------------------

#endif /* OCT_GENERATORS_CPP_GENERATOR_HPP */
