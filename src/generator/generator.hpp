//--------------------------------------------------------------------
/// \file
/// \brief
/// Output code generator base class
//--------------------------------------------------------------------

#ifndef OCT_GENERATORS_GENERATOR_HPP
#define OCT_GENERATORS_GENERATOR_HPP

//--------------------------------------------------------------------

#include <iostream>

namespace Semantics
{
	class Global;
}

//--------------------------------------------------------------------

/// \brief
/// Generator elements namespace
///
namespace Generators
{

	/// \brief
	/// Generator base class
	///
	class Generator
	{
	public:
		/// Generator output stream type
		typedef std::ostream GenStream;

	public:
		/// \brief
		/// Generate output
		///
		/// \param[in] stream - generator output stream
		/// \param[in] global - semantics Global object
		///
		/// \return
		/// \c True is returned on success or \c false otherwise.
		virtual bool generate(GenStream& stream, const Semantics::Global* global) = 0;
	};

}

//--------------------------------------------------------------------

#endif /* OCT_GENERATORS_GENERATOR_HPP */
