//--------------------------------------------------------------------
/// \file
/// \brief
/// Main P2C implementation file
//--------------------------------------------------------------------

#include <cstdlib>
#include <iostream>
using namespace std;

#include "globals.hpp"
#include "generator/cppgenerator.hpp"

//--------------------------------------------------------------------

// Forward declarations
extern int yyparse();
extern int yydebug;

//--------------------------------------------------------------------

/// \brief
/// Main program function
///
/// \param[in] argc - arguments count
/// \param[in] argv - argument values table
///
/// \return
/// Integer value describing error (0 = no error)
int main(int argc, char* argv[])
{
	// turn debug mode on if any parameter specified for the program
    yydebug = (argc > 1) ? 1 : 0;
    
	// initialize semantic generator
    if(g_semgen.init())
    {
		// parse the input
        if(yyparse() == 0)
        {
			// report parsing success
            cerr << "PARSING OK" << endl;
            cerr << endl;

            const Semantics::Global* global = g_semgen.getGlobal();
            if(global)
            {
				// provide SES (Semantic Structure) output 
				cerr << "SES OUTPUT:" << endl;
                if(global->writeSES(cerr, 0) == false)
                    cout << "SES OUTPUT ERROR" << endl;

				// provide C++ output
				Generators::Generator* gen = new Generators::CppGenerator();
				if(gen->generate(cout, global) == false)
					cerr << endl << endl << "GENERATOR OUTPUT ERROR" << endl;
				delete gen;
            }
            else
			{
				// report semantic generator error
                cerr << "ERROR: Cannot get GLOBAL object." << endl;
			}
        }
        else
		{
			// report parsing error
            cerr << "PARSING ERROR" << endl;
		}

		// Free semantic generator data
        g_semgen.free();
    }
    
    return 0;
}
