#
# OCTp2c makefile
#

#--- C FLAGS ---
CFLAGS			=  -g -D _DEBUG=1
#--- C++ FLAGS ---
CXXFLAGS		=
#--- LEX FLAGS ---
LEX				=	flex
#LFLAGS			=	-d
LFLAGS          =
#--- YACC ---
YACC			=	bison
YFLAGS			=

#----------- NO DEFAULT SUFFIXES ---------------
.SUFFIXES:

#----------- DEFINITIONS ---------------
LEX_INPUTS		=	src/lexer/lexer.l
LEX_SOURCES		=	$(LEX_INPUTS:%.l=%.lex.cpp)

YACC_INPUTS		=	src/parser/parser.y
YACC_SOURCES	=	$(YACC_INPUTS:%.y=%.yacc.cpp)

CPP_SOURCES		=	$(wildcard src/log/*.cpp)			\
					$(wildcard src/main/*.cpp)			\
					$(wildcard src/semantics/*.cpp)		\
					$(wildcard src/semgen/*.cpp)		\
					$(wildcard src/utility/*.cpp)

ALL_SOURCES		=	$(YACC_SOURCES) $(LEX_SOURCES) $(CPP_SOURCES)

OBJECTS			=	$(YACC_SOURCES:%.yacc.cpp=%.o)		\
					$(LEX_SOURCES:%.lex.cpp=%.o)		\
					$(CPP_SOURCES:%.cpp=%.o)

DEPENDENCIES	=	$(OBJECTS:%.o=%.d)

INCLUDE_DIR		=	src/

P2C_EXE_NAME	=	bin/p2c

#----------- RULES ---------------

all : $(OBJECTS) 
	echo $(OBJECTS)
	-mkdir bin
	g++ -o $(P2C_EXE_NAME) $^

clean :
	-rm -f $(OBJECTS) $(DEPENDENCIES) $(LEX_SOURCES) $(YACC_SOURCES)

#----------- AUTOMATIC RULES ---------------

include $(DEPENDENCIES)

%.o : %.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c -I$(INCLUDE_DIR) -o $@ $<

%.o : %.yacc.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c -I$(INCLUDE_DIR) -o $@ $<

%.o : %.lex.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c -I$(INCLUDE_DIR) -o $@ $<

%.lex.cpp : %.l
	$(LEX) $(LFLAGS) -o$@ $^

%.yacc.cpp %.yacc.hpp : %.y
	$(YACC) $(YFLAGS) -d -o $@ $^

%.d : %.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -MM -I$(INCLUDE_DIR) $< > $@.$$$$; \
	sed 's,\(.*\)\.o[ :]*,$(@:.d=.o) $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

%.d : %.yacc.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -MM -I$(INCLUDE_DIR) $< > $@.$$$$; \
	sed 's,\(.*\)\.o[ :]*,$(@:.d=.o) $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

%.d : %.lex.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -MM -I$(INCLUDE_DIR) $< > $@.$$$$; \
	sed 's,\(.*\)\.o[ :]*,$(@:.d=.o) $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


#----------- INTERMEDIATE FILES ---------------

.SECONDARY : $(LEX_SOURCES) $(YACC_SOURCES)
