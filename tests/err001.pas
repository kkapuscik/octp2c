program err001;

var
    v1, v2 : Integer;

procedure v1;
begin
end;

procedure v2;
    procedure v1;
    begin
    end;
begin
end;

begin
end.

