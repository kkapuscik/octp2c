program featurestest;

{ constant declarations }
const
    const001    =   11;
    const002    =   -430;
    const003    =   154.02;
    const004    =   123e12;
    const005    =   -141.2e2;
    const006    =   'Sample text';
    const007    =   const001;
    const008    =   -const002;
    const009    =   -const004;
    const010    =   const005;

{ variable declarations }
var
    var001  :   Integer;
    var002  :   Real;
    var003  :   String;
    var004  :   Char;
    var005  :   Boolean;

begin
    { for statement }
    for var001 := 11 to 22 do
        ;
    for var002 := 22 downto 11 do
        ;

    { while statement }
    while var005 do
        ;
        
    { repeat statement }
    repeat
        ;
        ;
    until var005;
    
    { if statement }
    if var005 then ;

    if var005 then begin end
    else ;

    if var005 then begin end
    else if var005 then ;

    if var005 then begin end
    else if var005 then begin end
    else ;
    
    if var005 then
    begin
        if var005 then begin end
        else begin end
    end
    else
    begin
        if var005 then begin end
        else begin end
    end;

    if var005 then
    begin
        if var005 then begin end
        else begin end
    end
    else
    begin
        if var005 then
        begin
        end;

        if var005 then 
            begin end
        else 
            begin end;
    end;

    { assignment statements }
    var001 := 11;
    var001 := var001 + var001;
    var001 := var001 * var001 + var001 / var001 div var001;
    var001 := -var001;
    var001 := -var001 + var001 - var001 * var001;

end.

