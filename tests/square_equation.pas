program squareequation;

const
    DeltaSqrt = 1.15;

var
    a, b, c : Real;
    Delta : Real;
    x1, x2 : Real;
    SolCount : Integer;

begin
    Write('Enter value for A = ');
    Read(a);
    Write('Enter value for B = ');
    Read(b);
    Write('Enter value for C = ');
    Read(c);

    Delta := b * b - 4 * a * c;

    if a <> 0 then
    begin
        if Delta > 0 then
        begin
            x1 := (-b - DeltaSqrt) / (2 * a);
            x2 := (-b + DeltaSqrt) / (2 * a);
            SolCount := 2;
        end
        else if Delta = 0 then
        begin
            x1 := (-b) / (2 * a);
            SolCount := 1;
        end
        else { Delta < 0 }
            SolCount := 0;
    end
    else { a = 0 }
    begin
        if b <> 0 then
        begin
            x1 := (-c) / b;
            SolCount := 1;
        end
        else { b = 0 }
        begin
            if c = 0 then
                SolCount := -1
            else
                SolCount := 0;
        end
    end;

    if SolCount <> -1 then
    begin
        Writeln('There are ', SolCount, ' solution(s).');
        if(SolCount >= 1) then
            Writeln('X1 = ', x1);
        if(SolCount >= 2) then
            Writeln('X2 = ', x2);
    end
    else
        Writeln('There are infinite number of solutions.');

    Writeln('Thank you for using this supermegahit program!');
    Writeln('See you in the next program from OCT!');

end.

